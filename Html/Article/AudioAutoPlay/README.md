---
title: audio标签自动播放问题
date: 2021-12-09
tags:
 - Html
categories:
 - Html
---

## 一、前言

`chrome`、`safari`、`firefox`都在某些版本后限制了`audio`自动播放（chrome是66+），必须要用户与文档产生交互，否则会报错：

```shell
play() failed because the user didn't interact with the document first
```

## 二、chrome 自动播放策略变化

原文如下：

```shell
in order to improve the user experience, minimize incentives to install ad blockers, and reduce data consumption on expensive and/or constrained networks
```

意思就是减少广告杂音对用户的影响以及节省流量，总之就是用户不主动触发页面交互就不可以自动播放。

## 三、解决办法

1. 在chrome 浏览器中输入：`chrome://flags`，搜索 `Autoplay policy`，默认为 `Default`，修改为 `No user gesture is required`；但这个只能在本地对一个客户端做设置，并非最终之计。

2. 手动调用play，在回调中判断是否播放成功，如果播放失败，显式的提示用户点击播放，比如`$message.warning('您的浏览器限制播放，请点击播放')`，或者显示一个播放按钮，引导客户去点击此按钮。

```javascript
var promise = document.querySelector('audio').play();

if (promise !== undefined) {
  promise.then(_ => {
    // Autoplay started!
  }).catch(error => {
    // Autoplay was prevented.
    // Show a "Play" button so that user can start playback.
  });
}
```

## 四、AudioContext

我看网上有人说可以使用AudioContext去实现自动播放，这个是不行的，官网原文如下：

```shell
The Web Audio API will be included in the Chrome autoplay policy with M71 (December 2018).
```

意味着`AudioContext`也在2018/12被禁止自动播放

之所以有人这么说，是因为`Chrome autoplay policy`是2018/4月开始在chrome上实现的，但那时`AudioContext`还没实现这个协议，只有audio标签实现了。

## 五、iframe

官网另有一种说法:
```html
<!-- Autoplay is allowed. -->
<iframe src="https://cross-origin.com/myvideo.html" allow="autoplay">

<!-- Autoplay and Fullscreen are allowed. -->
<iframe src="https://cross-origin.com/myvideo.html" allow="autoplay; fullscreen">
```

经过本人实测，对 `audio` 依然无效，`iframe` 中的 `audio`，无论是设置 `autoplay` 还是调用 `play` 方法都不行，现象跟直接在顶层页面的相应设置和操作是一样的。

## 六、video

`video` 元素设置静音 `muted` 情况下，可以自动播放，但根据chrome官方，后续实现不一定保证这一点。并且这句话的意思是 `vedio` 元素也最多可以视频画面自动播放，无法自动播放声音。

## 七、window.open

经测试发现chrome有个后门，通过 `window.open` 打开的同域名的网页可以自动播放，但这个网页刷新后依然无法自动播放；也就是从另一个页面触发了用户交互事件跳转到播放页面可以算作用户进行了交互，但是刷新页面后效果便会消失。