---
title: HTML(5) 代码规范
date: 2021-12-01
tags:
 - Html
categories:
 - Html
---

针对于 HTML5 ，我们应该形成比较好的代码规范，以下提供了几种规范的建议。

## 一、使用正确的文档类型

文档类型声明位于HTML文档的第一行：

```html
<!DOCTYPE html>
```

如果你想跟其他标签一样使用小写，可以使用以下代码：

```html
<!doctype html>
```

## 二、使用小写元素名

HTML5 元素名可以使用大写和小写字母。

推荐使用小写字母：

- 混合了大小写的风格是非常糟糕的。
- 开发人员通常使用小写 (类似 XHTML)。
- 小写风格看起来更加清爽。
- 小写字母容易编写。

不推荐:

```html
<SECTION>
  <p>这是一个段落。</p>
</SECTION>
```

非常糟糕:

```html
<Section>
  <p>这是一个段落。</p>
</SECTION>
```

推荐:

```html
<section>
  <p>这是一个段落。</p>
</section>
```

## 三、关闭所有 HTML 元素

在 HTML5 中, 你不一定要关闭所有元素 (例如 <p> 元素)，但我们建议每个元素都要添加关闭标签。

不推荐:

```html
<section>
  <p>这是一个段落。
  <p>这是一个段落。
</section>
```

推荐:

```html
<section>
  <p>这是一个段落。</p>
  <p>这是一个段落。</p>
</section>
```

## 四、关闭空的 HTML 元素

在 HTML5 中, 空的 HTML 元素也不一定要关闭，但我们建议添加关闭标签：

不推荐:

```html
<meta charset="utf-8">
```

推荐:

```html
<meta charset="utf-8" />
```

## 五、使用小写属性名

HTML5 属性名允许使用大写和小写字母。

我们推荐使用小写字母属性名:

- 同时使用大小写是非常不好的习惯。
- 开发人员通常使用小写 (类似 XHTML)。
- 小写风格看起来更加清爽。
- 小写字母容易编写。

不推荐：

```html
<div CLASS="menu">
```

推荐：

```html
<div class="menu">
```

## 六、属性值

HTML5 属性值可以不用引号。

属性值我们推荐使用引号:

- 如果属性值含有空格需要使用引号。
- 混合风格不推荐的，建议统一风格。
- 属性值使用引号易于阅读。

以下实例属性值包含空格，没有使用引号，所以不能起作用:

```html
<table class=table striped>
```

以下使用了双引号，是正确的：

```html
<table class="table striped">
```

## 七、图片属性

图片通常使用 alt 属性。 在图片不能显示时，它能替代图片显示。

```html
<img src="html5.gif" alt="HTML5">
```

定义好图片的尺寸，在加载时可以预留指定空间，减少闪烁。

```html
<img src="html5.gif" alt="HTML5" style="width:128px;height:128px">
```

## 八、空格和等号

等号前后可以使用空格。

```html
<link rel = "stylesheet" href = "styles.css">
```

但我们推荐少用空格:

```html
<link rel="stylesheet" href="styles.css">
```

## 九、避免一行代码过长

使用 HTML 编辑器，左右滚动代码是不方便的。

每行代码尽量少于 `80` 个字符。

## 十、空行和缩进

不要无缘无故添加空行。

为每个逻辑功能块添加空行，这样更易于阅读。

缩进使用 `两个空格`，不建议使用 `TAB`。

## 十一、省略 `<html>` 、`<body>` 和 `<head>` ?

在标准 HTML5 中， `<html>` 、`<body>` 和 `<body>` 标签是可以省略的，但 `不推荐省略`。

## 十二、元数据

HTML5 中 `<title>` 元素是必须的，标题名描述了页面的主题:

```html
<title>标题</title>
```

标题和语言可以让搜索引擎很快了解你页面的主题:

```html
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta charset="UTF-8">
  <title>菜鸟教程</title>
</head>
```

## 十三、HTML 注释

注释可以写在 `<!-- 和 -->` 中:

```html
<!-- 这是注释 -->
```

比较长的注释可以在 `<!-- 和 -->` 中分行写：

```html
<!--
  这是一个较长注释。 这是 一个较长注释。这是一个较长注释。
  这是 一个较长注释 这是一个较长注释。 这是 一个较长注释。
-->
```

长注释第一个字符缩进`两个空格`，更易于阅读。

## 十四、使用小写文件名

大多 Web 服务器 (Apache, Unix) 对大小写敏感： `london.jpg` 不能通过 `London.jpg` 访问。

其他 Web 服务器 (Microsoft, IIS) 对大小写不敏感： `london.jpg` 可以通过 `London.jpg` 或 `london.jpg` 访问。

你必须保持统一的风格，我们建议统一使用小写的文件名。


## 十五、.htm 和 .html 的区别

.htm 和 .html 的扩展名文件本质上是没有区别的。浏览器和 Web 服务器都会把它们当作 HTML 文件来处理。

区别在于：

.htm 应用在早期 DOS 系统，系统现在或者只能有三个字符。

在 Unix 系统中后缀没有特别限制，一般用 .html。
