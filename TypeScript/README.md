---
title: TypeScript
date: 2022-05-26
tags:
 - TypeScript
---

## 安装

```shell
npm i typescript -g
```

检测是否安装成功：

```shell
tsc -v
```

## 类型声明

建议使用 `let` 和 `const` 进行类型声明，不建议使用 `var`。与JS不同的是声明时要进行类型声明让ts有意义。

```typescript
// 1. 声明类型再声明初始化值
let a:number;
a = 10;
// 报错： a = "hello";


// 2. 声明类型的同时声明初始化值
let b:number = 100;
b = 101;
// 报错：b = "hello";

// 3. 声明类型的同时声明初始化值
let c = 100;
c = 99;
//  报错：c = "hello";
```

函数进行声明：

```typescript
// 在此处规定函数的返回值为字符串类型
function fn(a:number, b:number):string {
    return `result: ${a + b}`;
}

console.log(fn(1, 2));
```

> 编译ts文件：tsc 文件名  会编译成js文件，再运行node ***.js即可
> 实时监控编译ts指令：tsc 文件名 -w

## ts数据类型

### string

```typescript
let a:string;
a = "hello"
```

### number

```typescript
let a:number;
a = 100;
```

### boolean

```typescript
let a:boolean;
a = true;
a = false;
```

### 字面类型

定义一个初始化值或类型的集合。

```typescript
// 字面类型除了可以锁定数据类型还能锁定初始化的值
let a:100;
// 报错 a = 99;
// 报错 a = "100"
```

为什么不至于用const呢？

```typescript
let a:99|100|101;

a = 99;
a = 100;
```

当然也可以定义类型的集合：

```typescript
let a:number|string;
a = 100;
a = "100";
// 报错 a = true;
```

### any

我们不希望类型检查器对这些值进行检查而是直接让它们通过编译阶段的检查。 那么我们可以使用any类型来标记这些变量：

```typescript
let a:any;
a = 1; // OK
a = "1"; // OK
a = true; // OK

// 当你只知道一部分数据的类型时，any类型也是有用的。 比如，你有一个数组，它包含了不同的类型的数据：
let arr:any[];
arr = [1, "1", true]; // OK
```

> 尽量少的使用 `any`， 否则你可能在用 `AnyScript` 写代码

### unknown

unknown也可以像any一样赋值为任何类型的值，但不能将unknown类型的值赋值给其他变量。

```typescript
let a:unknown;
a = 1; // OK
a = "1"; // OK
a = true; // OK

let b:string;
// 报错 b = a;
```

```typescript
let a:any;
a = 1; // OK
a = "1"; // OK
a = true; // OK

let b:string;
b = a; // OK
```

### 数组

有两种方式可以定义数组。 第一种，可以在元素类型后面接上`[]`，表示由此类型元素组成的一个数组：

```typescript
let arr1:number[] = [1, 2, 3];
let arr2:string[] = ["1", "2"];
let arr3:(number | string)[] = [1, 2, "3"];
```

第二种方式是使用数组泛型，`Array<元素类型>`：

```typescript
let arr1:Array<number> = [1, 2, 3];
let arr2:Array<string> = ["1", "2"];
let arr3:Array<number | string> = [1, 2, "3"];
```

### 对象

对象定义方式与JS相同。

### 元组 Tuple

元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同。 比如，你可以定义一对值分别为`string`和`number`类型的元组。

```javascript
let arr:[number, string]
arr = [1, "1"]
// 报错 arr = ["1", 1]
```

### 枚举类型

`enum`类型是对JavaScript标准数据类型的一个补充。

```typescript
enum userType {
    admin,
    user
}

console.log(userType.admin); // 0
console.log(userType.user); // 1
```

默认情况下，从`0`开始为元素编号。 你也可以手动的指定成员的数值。 例如，我们将上面的例子改成从`1`开始编号：

```typescript
enum userType {
    user = 1,
    admin,
    superAdmin
}

console.log(userType.user); // 1
console.log(userType.admin); // 2
console.log(userType.superAdmin); // 3
```

一般情况下我们都会给枚举进行真正的赋值:

```typescript
enum userType {
    user = "User",
    admin = "Admin",
    superAdmin = "Super"
}

console.log(userType.user); // User
console.log(userType.admin); // Admin
console.log(userType.superAdmin); // Super
```

### void

void代表返回为空，可以是没有返回值，或者返回特定的 `null` / `undefined`。

```typescript
function fn():void {
    console.log("hello");
}
```

```typescript
function fn():void {
    // return undefined;
    return null;
}
```

### never

never类型表示的是那些永不存在的值的类型。never 类型用于返回 `error` 或死循环。

```typescript
function getError(): never {
    throw new Error("error");
}
function infiniteFunc(): never {
    while (true) {}
}
```

### Null 和 Undefined

TypeScript里，`undefined` 和 `null` 两者各自有自己的类型分别叫做 `undefined` 和 `null`。 和 `void` 相似，它们的本身的类型用处不是很大：

```typescript
let u: undefined = undefined
let n: null = null
```

### 类型别名（自定义类型）

使用 `type` 关键字。

```typescript
// 联合类型
type Name = string | number;
let a:Name;

a = "123"; // OK
a = 123; // OK
```

```typescript
// 原始值
type Name = 11|22|33|44;
let a:Name;

a = 11; // OK
a = 22; // OK
```

## ts类

### 初始类

```typescript
class Person {
    // 属性
    name:string = "Ron";
    age:number = 18;

    // 方法
    join(b:number):number {
        return this.age + b;
    }
}

const p = new Person();

console.log(p.age); // 18
// 改变类中的属性
p.age = 23;
console.log(p.name); // Ron
console.log(p.age); // 23
console.log(p.join(2)); // 25
```

### 构造函数

```typescript
class Person {
    name:string;
    age:number;
    // 构造方法
    constructor(name:string, age: number) {
        this.name = name;
        this.age = age;
    }
    sayHello():string{
        return "hello" + this.name;
    }
}

const p1 = new Person("Ron", 20);
const p2 = new Person("Jack", 18);

console.log(p1); // { name: 'Ron', age: 20 }
console.log(p2); // { name: 'Jack', age: 18 }
console.log(p2.sayHello()); // helloJack
```

### 类的继承

```typescript
class Animal {
    name:string;
    // 构造方法
    constructor(name:string) {
        this.name = name;
    }
    move(distance: number){
        console.log(`${this.name}移动了${distance}米`);
    }
}

class Dog extends Animal {
    constructor(name: string) {
        // 当子类中包含构造函数时，必须 要先调用super(),它会执行超类的构造函数，并且一定要在构造函数访问this之前。
        super(name);
    }
    // 重写父类的方法
    move(distance: number = 10) {
        console.log("奔跑...")
        super.move(distance);
    }
}


const dog = new Dog("小花");
dog.move(10); // 奔跑...  小花移动了10米
```

### 类修饰符

公共修饰符`public`：在当前类（this.name），子类(super.name)，类外部(p.name)都可以访问。如果不加修饰符，默认是public。

受保护的修饰符`protected`，在当前类和子类可以访问，无法在类外部访问。

私有修饰符`private`，只能在当前类中访问，无法在子类和类外部访问。

只读修饰符`readonly`，都可以访问，但无法重新赋值修改。

静态修饰符`static`，只能通过类来访问（Person.name）。

> 只有`public`和`protected`修饰的属性才能通过“super”关键字访问。

### 抽象类和抽象方法

不能创建一个抽象类的实例；
抽象类中的抽象方法必须在派生类中实现；
定义一个抽象基类中没有声明的方法，但是它没办法在实例中使用；

```typescript
abstract class Department {

    constructor(public name: string) {}

    printName(): void {
        console.log('Department name: ' + this.name);
    }

    abstract printMeeting(): void; // 必须在派生类中实现
}

class AccountingDepartment extends Department {

    constructor() {
        super('Accounting and Auditing'); // 在派生类的构造函数中必须调用 super()
    }

    printMeeting(): void {
        console.log('The Accounting Department meets each Monday at 10am.');
    }

    // 定义一个抽象基类中没有声明的方法，但是它没办法在实例中使用
    generateReports(): void {
        console.log('Generating accounting reports...');
    }
}

let department: Department; // 允许创建一个对抽象类型的引用
department = new Department(); // 错误: 不能创建一个抽象类的实例
department = new AccountingDepartment(); // 允许对一个抽象子类进行实例化和赋值
department.printName();
department.printMeeting();
department.generateReports(); // 错误: 方法在声明的抽象类中不存在
```

## interface接口

接口便是规定

### 基本使用

```typescript
interface IUser {
    name: string;
    age: number;
    sex?: number;
    readonly isSingle: boolean;
}

const user: IUser = {
    name: "Ron",
    age: 10,
    isSingle: true
}

console.log(user); // { name: 'Ron', age: 10, isSingle: true }
```

### 函数使用

```typescript
interface AddFn {
    (number1: number, number2: number):number;
}

const add:AddFn = (n1, n2) => {
    return n1 + n2;
}

console.log(add(1,2)); // 3
```

### 类使用

需要使用`implements`关键字。

```typescript
interface MyInter {
    name: string; // 必须
    age?: number; // 可选
    readonly sex: number; // 只读
    move(): string;
}

class MyClass implements MyInter {
    name:string;
    age:number;
    sex:number;
    constructor(name:string, age:number, sex:number) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
    move(): string {
        return `${this.name}在移动`;
    }
}

const m = new MyClass("Ron", 18, 1);
console.log(m); // MyClass { name: 'Ron', age: 18, sex: 1 }
console.log(m.move()); // Ron在移动
```

## 泛型

### 处理函数参数

```typescript
function print<T>(arg:T):T {
    console.log(arg)
    return arg
}

print<string>('hello')  // 定义 T 为 string
print('hello')  // TS 类型推断，自动推导类型为 string
```

### 泛型约束类

定义一个栈，有入栈和出栈两个方法，如果想入栈和出栈的元素类型统一，就可以这么写：

```typescript
class Stack<T> {
    private data: T[] = []
    push(item:T) {
        return this.data.push(item)
    }
    pop():T | undefined {
        return this.data.pop()
    }
}
```

### 泛型约束接口

```typescript
interface IKeyValue<T, U> {
    key: T
    value: U
}

const k1:IKeyValue<number, string> = { key: 18, value: 'Ron'}
const k2:IKeyValue<string, number> = { key: 'Ron', value: 18}
```

### 泛型定义数组

```typescript
const arr: Array<number> = [1,2,3];
```
