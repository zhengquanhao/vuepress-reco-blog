# Pagination

二次封装分页器

## Usage

```vue
<template>
<Pagination
    :page-sizes="[10, 20, 50]"
    :page="state.currentPage"
    :limit="state.pageSize"
    :total="state.total"
    @pagination="onPagination"
/>
</template>

<script setup lang="ts">
import { ref, reactive } from 'vue';
import Pagination, {
    TPaginationParam,
} from '@/components/Pagination/index.vue';

interface IPageState {
    currentPage: number;
    pageSize: number;
    total: number;
}

const state = reactive<IPageState>({
    currentPage: 1,
    pageSize: 10,
    total: 100,
});

function onPagination({ page, limit }: TPaginationParam) {
    console.log(`page: ${page}, limit: ${limit}`);
    state.pageSize = limit;
    state.currentPage = page;
}
</script>
```

## Attributes

## Event
