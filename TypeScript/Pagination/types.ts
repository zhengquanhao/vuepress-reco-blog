export type TPaginationParam = {
    page: number;
    limit: number;
};
