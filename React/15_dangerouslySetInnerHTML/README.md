---
title: dangerouslySetInnerHTML
date: 2022-06-08
tags:
 - React
categories:
 - React
---

dangerouslySetInnerHTML可以帮助我们解析html标签，但存在风险，请在特定的绝对信任的场景下使用。

处于安全的原因，React当中所有表达式的内容会被转义，如果直接输入，标签会被当成文本。这时候就需要使用`dangerouslySetHTML`属性，它允许我们动态设置`innerHTML`。

```javascript
import { Component } from "react";

class App extends Component {
    state = {
        myTemplate: `<div>
            <p>React.js是一个构建UI的库</p>
        </div>`
    }
    render() {
        return (
            <div dangerouslySetInnerHTML={
                // 里面接收一个对象，有一个固定属性__html为要渲染的元素
                {
                    __html: this.state.myTemplate
                }
            }></div>
        )
    }
}

export default App;
```
