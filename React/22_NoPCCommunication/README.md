---
title: 非父子组件传值
date: 2022-06-10
tags:
 - React
categories:
 - React
---

## 状态提升（中间人模式）

把父组件当做中间人，子组件A的值先传给父组件，再从父组件将值传给组件B。

```javascript
import { Component } from "react";

class Child1 extends Component {
    render () {
        let str = "子组件的值";
        return (
            <div>
                子组件A
                <button onClick={() => {this.props.clickEvent(str)}}>点击</button>
            </div>
        )
    }
}

class Child2 extends Component {
    render () {
        return (
            <div>
                子组件B
                {this.props.msg}
            </div>
        )
    }
}

class App extends Component {
    state = {
        msg: ""
    }
    render() {
        return (
            <div>
                <Child1 clickEvent={this.clickEvent} />
                <Child2 msg={ this.state.msg }/>
            </div>
        )
    }
    clickEvent = (val) => {
        this.setState({
            msg: val
        });
    }
}

export default App;
```

## 订阅发布者模式

订阅发布者模式就像我们生活中常用的微信公众号一样，订阅者关注公众号，发布者在发布后订阅者可以看到信息。

先订阅再发布，订阅者提早的订阅才能在发布者发布的时候获取到发布信息；如果先发布在订阅那么将什么的得不到。

```javascript
// 调度中心
var bus = {
    list: [],
    // 订阅
    subscribe(callback) {
        this.list.push(callback);
    },
    // 发布
    publish(text) {
        this.list.forEach(callback => {
            callback && callback(text);
        })
    }
}

// 订阅者
bus.subscribe((val) => {
    // 订阅者要做的事可以当成是一个回调
    console.log("111", val);
});

bus.subscribe((val) => {
    console.log("222", val);
});

// 发布者
setTimeout(() => {
    bus.publish("发布信息1");
}, 100)

setTimeout(() => {
    bus.publish("发布信息2");
}, 200)
```

上述案例，A便为发布者，B组件就是订阅者；且订阅发布者模式不必须是兄弟组件关系，任意组件皆可通信。

```javascript
import { Component } from "react";

// 调度中心
var bus = {
    list: [],
    // 订阅
    subscribe(callback) {
        this.list.push(callback);
    },
    // 发布
    publish(text) {
        this.list.forEach(callback => {
            callback && callback(text);
        })
    }
}

class Child1 extends Component {
    render () {
        let str = "子组件的值";
        return (
            <div>
                子组件A
                <button onClick={() => {bus.publish(str)}}>点击</button>
            </div>
        )
    }
}

class Child2 extends Component {
    constructor() {
        super();
        this.state = {
            msg: ""
        }
        // 在构造方法中就进行订阅
        bus.subscribe((val) => {
            console.log(val);
            this.setState({
                msg: val
            })
        })
    }
    render() {
        return (
            <div>
                子组件B
                {this.state.msg}
            </div>
        )
    }
}

class App extends Component {
    state = {
        msg: ""
    }
    render() {
        return (
            <div>
                <Child1 />
                <Child2 />
            </div>
        )
    }
}

export default App;
```

## context方案

可以理解成生产者消费者模式，举例国家有三大运营商便是生产者，买了电话卡的人就是消费者，只要有信号 买了卡的人就可以通过运营商相互通信。

```javascript
import { Component, createContext } from "react";

const GlobalContext = createContext(); // 创建context对象

class App extends Component {
    state = {
        msg: ""
    }
    render() {
        return (
            // 供应商提供服务，提供的服务写在value对象里
            <GlobalContext.Provider value={{
                msg: this.state.msg,
                changeMsg: (val) => {
                    this.setState({
                        msg: val
                    })
                }
            }}>
                <Child1 />
                <Child2 />
            </GlobalContext.Provider>
        )
    }
}

class Child1 extends Component {
    render () {
        let str = "子组件的值";
        return (
            // 消费者1，里面是一个函数，返回结构
            <GlobalContext.Consumer>
                {
                    (value) => {
                        return(
                            <div>
                                子组件A
                                <button onClick={() => {value.changeMsg(str)}}>点击</button>
                            </div>
                        )
                    }
                }
            </GlobalContext.Consumer>
        )
    }
}

class Child2 extends Component {
    render() {
        return (
            // 消费者2
            <GlobalContext.Consumer>
                {
                    (value) => {
                        console.log(value);
                        return(
                            <div>
                                子组件B
                                {value.msg}
                            </div>
                        )
                    }
                }
            </GlobalContext.Consumer>
        )
    }
}

export default App;
```
