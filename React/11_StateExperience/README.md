---
title: 状态初识
date: 2022-06-08
tags:
 - React
categories:
 - React
---

## 状态（state）

状态就是组件描述某种显示情况的数据，由组件自己设置和更改，也就是说由组件自己维护，使用状态的目的就是为了在不同的状态下使组件的显示不同(自己管理)

## 定义状态

使用`state`关键字。如果使用其他自定义变量，数据改变界面不会更新。

在`vue`中，`data`属性是利用`Object.defineProperty`处理过的，更改`​data`的数据的时候会触发数据的`getter`和`setter`，但是`React`中没有做这样的处理，如果直接更改的话，`react`是无法得知的，所以，需要使用特殊的更改状态的方法`setState`。

### 第一种方式

```javascript
import React, { Component } from "react";

class App extends Component {
    state = {
        flag: true
    }
    render() {
        return(
            <button onClick={ this.handleClick }>{ this.state.flag ? "收藏" : "取消收藏" }</button>
        )
    }
    handleClick = () => {
        // 不能直接改变state的值，react不允许this.state.flag = !this.state.flag; 需要使用setState方法
        this.setState({
            flag: !this.state.flag
        });
    }
}


export default App;
```

### 第二种方式（推荐）

```javascript
import React, { Component } from "react";

class App extends Component {
    constructor() {
        super();
        this.state = {
            flag: true
        }
    }
    render() {
        return(
            <button onClick={ this.handleClick }>{ this.state.flag ? "收藏" : "取消收藏" }</button>
        )
    }
    handleClick = () => {
        // 不能直接改变state的值，react不允许this.state.flag = !this.state.flag; 需要使用setState方法
        this.setState({
            flag: !this.state.flag
        });
    }
}


export default App;
```
