---
title: 组件样式
date: 2022-06-08
tags:
 - React
categories:
 - React
---

## 行内样式

- 想给虚拟DOM添加行内样式，需要使用表达式传入`样式对象`的方式来实现。
- 属性名需要驼峰写法

```javascript
import { Component } from "react";

class App extends Component {
    render() {
        return(
            <div>
                {/* 注意这里的两个括号，第一个表示使用jsx语法，第二个表示插入对象 */}
                <p style={{color: "red", fontSize: "14px"}}>1111</p>
            </div>
        )
    }
}

export default App;
```

```javascript
import { Component } from "react";

class App extends Component {
    render() {
        const obj = {
            color: "red",
            fontSize: "14px"
        }
        return(
            <div>
                <p style={ obj }>1111</p>
            </div>
        )
    }
}

export default App;
```

行内样式需要写入一个样式对象，而这个样式对象的位置可以放在很多地方，例如`render`函数里、组件原型上、外链js文件中。

## class类样式

React推荐我们使用行内样式，因为React觉得每一个组件都是一个独立的整体

其实我们大多数情况下还是大量的在为元素添加类名，使用类样式；但需要注意的是`class`需要写成`className`（因为毕竟是在写类js代码，会收到js规则的现在，而class是关键字）

需要新建`.css`文件并引入。

```javascript
import { Component } from "react";
import "./index.css"

class App extends Component {
    render() {
        return(
            <div>
                <p className="text">1111</p>
            </div>
        )
    }
}

export default App;
```

## 扩展

同`class`一样，`for`在jsx里也属于关键字，所以我们在写html用到`for`时，使用`htmlFor`替代。

```javascript
import { Component } from "react";
import "./index.css"

class App extends Component {
    render() {
        return(
            <div>
                {/* 正常写法：<label for="userName">用户名</label> */}
                <label htmlFor="userName">用户名</label>
                <input type="text" id="userName" />
            </div>
        )
    }
}

export default App;
```