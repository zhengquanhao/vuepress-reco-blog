---
title: React事件绑定
date: 2022-06-08
tags:
 - React
categories:
 - React
---

## 事件绑定的四种方式

采用`on+事件名`的方式来绑定一个事件。

```javascript
import { Component } from "react";

class App extends Component {
    render() {
        return(
            <div>
                {/* 方式一 */}
                <button onClick={() => {
                    console.log("click1");
                }}>点击1</button>
                {/* 方式二 */}
                <button onClick={ this.handleClick2 }>点击2</button>
                {/* 方式三 */}
                <button onClick={ this.handleClick3 }>点击3</button>
                {/* 方式四 */}
                <button onClick={ () => {
                    this.handleClick4();
                } }>点击4</button>
            </div>
        )
    }
    handleClick2() {
        console.log("click2");
    }
    handleClick3 = () => {
        console.log("click3");
    }
    handleClick4() {
        console.log("click4");
    }
}


export default App;
```

注意：方式二三在调用上使用`this`，因为是调用类中的方法；后面不能加`()`，因为加入括号表示立即调用，且在上面案例中函数是没有返回值的立即执行后返回`undefined`，等价于下面代码从而点击事件失效。

```html
<button onClick={ undefined }>点击2</button>
```

## 四种方式的使用

在this的指向上，方式二会出问题，其他三种this因为箭头函数不改变this指向从而指向正常。

```javascript
import { Component } from "react";

class App extends Component {
    a = 100;
    render() {
        return(
            <div>
                {/* 方式一 */}
                <button onClick={() => {
                    console.log(this.a); // 100
                }}>点击1</button>
                {/* 方式二 */}
                <button onClick={ this.handleClick2 }>点击2</button>
                {/* 方式三 */}
                <button onClick={ this.handleClick3 }>点击3</button>
                {/* 方式四 */}
                <button onClick={ () => {
                    this.handleClick4();
                } }>点击4</button>
            </div>
        )
    }
    handleClick2() {
        // 谁调用函数this就指向谁
        console.log(this.a); // 报错
    }
    handleClick3 = () => {
        console.log(this.a); // 100
    }
    handleClick4() {
        console.log(this.a); // 100
    }
}


export default App;
```

方案二的this指向问题解决：

```html
<button onClick={ this.handleClick2.bind(this) }>点击2</button>
```

- 方式一：在处理逻辑比较少时使用，多的话影响代码可读性
- 方式二：因为this指向有问题，不推荐使用
- 方式三：推荐使用，但无法传递参数
- 方式四：实在是太香了

方式四因为就一句话，可以直接简写：

```javascript
<button onClick={ () => this.handleClick4() }>点击4</button>
```

## 事件对象

```javascript
import { Component } from "react";

class App extends Component {
    render() {
        return(
            <div>
                {/* 方式一 */}
                <button onClick={(e) => {
                    console.log(e);
                    console.log(e.target); // <button>点击1</button>
                }}>点击1</button>
                {/* 方式二 */}
                <button onClick={ this.handleClick2 }>点击2</button>
                {/* 方式三 */}
                <button onClick={ this.handleClick3 }>点击3</button>
                {/* 方式四 */}
                <button onClick={ (e) => this.handleClick4(e) }>点击4</button>
            </div>
        )
    }
    handleClick2(e) {
        console.log(e);
        console.log(e.target);
    }
    handleClick3 = (e) => {
        console.log(e);
        console.log(e.target);
    }
    handleClick4(e) {
        console.log(e);
        console.log(e.target);
    }
}


export default App;
```

## 和原生的事件绑定的区别

React事件绑定和原生的事件是有区别的，原生的事件全是小写onclick, React里的事件是驼峰onClick

React并没有真正绑定事件在具体元素身上，而是利用了`js冒泡的机制`通过`代理的方式`绑定在`root`根节点上

![click](./images/click1.png)

![click](./images/click2.png)
