---
title: 编写第一个React应用程序
date: 2022-05-26
tags:
 - React
categories:
 - React
---

src/index.js:

```javascript
// 从react的包中引入React，只要你写React组件就必须引入react，因为react里有一种语法叫JSX，要写JSX就需要用到react
import React from "react";
// ReactDOM可以帮我们把React组件渲染到页面上。
import ReactDOM from "react-dom";

// ReactDOM里面有一个render方法，功能就是把组件渲染并且构造DOM树，然后插入到页面上某个特定元素上
ReactDOM.render(
    // JSX语法； JSX == JS + XML
    <h1>Hello React</h1>,
    // 渲染到哪里
    document.getElementById("root")
);
```

> 有些同学可能会问在这个程序中react引入了但没有使用为啥还要引入？因为使用JSX就必须引入react，之前在的版本不引人将无法正常渲染，在这里我们为了严禁也将react引入。

在react18版本中不在使用render方法，而是使用[createRoot方法](https://zh-hans.reactjs.org/docs/hello-world.html)。