---
title: 状态再体验
date: 2022-06-09
tags:
 - React
categories:
 - React
---

## setState的同步异步

当`setState` 处在一个同步的逻辑值 异步更新状态和真实dom（原理中setState有一个标志位，当出在同步逻辑时标志位为true，支持合并处理以setState最后一次为主）

当`setState` 处在一个异步的逻辑值 同步更新状态和真实dom（标志位为false，不支持合并处理）

```javascript
// 当`setState` 处在一个同步的逻辑值
import { Component } from "react";

class App extends Component {
    state = {
        count: 1
    }
    render() {
        return (
            <div>
                {this.state.count}
                <button onClick={() => this.handleClick()}>click</button>
            </div>
        )
    }
    handleClick = () => {
        // 即使书写多次setState，数据也只更新一次，每次只加1
        this.setState({
            count: this.state.count + 1
        })
        this.setState({
            count: this.state.count + 1,
        })
        this.setState({
            count: this.state.count + 1
        })
    }
}

export default App;
```

```javascript
// 当`setState` 处在一个异步的逻辑值
import { Component } from "react";

class App extends Component {
    state = {
        count: 1
    }
    render() {
        return (
            <div>
                {this.state.count}
                <button onClick={() => this.handleClick()}>click</button>
            </div>
        )
    }
    handleClick = () => {
        setTimeout(() => {
            // 处在异步逻辑中，预期一致每次加3
            this.setState({
                count: this.state.count + 1
            })
            this.setState({
                count: this.state.count + 1,
            })
            this.setState({
                count: this.state.count + 1
            })
        }, 0)
    }
}

export default App;
```

## setState的第二个参数

setState具备第二个参数，第二个参数是可选的回调函数，状态和DOM更新完成后会触发

```javascript
import { Component } from "react";
import BScroll from 'better-scroll'

class App extends Component {
    state = {
        list: []
    }
    render() {
        return (
            <div>
                <button onClick={() => this.handleClick()}>click</button>
                <div className="wrapper" style={{height: '200px', overflow: 'hidden', background: 'pink'}}>
                    <ul className="content">
                        {this.state.list.map(item => <li key={item}>{item}</li>)}
                    </ul>
                </div>
            </div>
        )
    }
    handleClick = () => {
        let newList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        this.setState({
            list: newList
        }, () => {
            new BScroll('.wrapper')
        })
        // 或在异步中处理
        // setTimeout(() => {
        //     this.setState({
        //         list: newList
        //     })
        //     new BScroll('.wrapper'
        // }, 0)
    }
}

export default App;
```
