---
title: Ref的应用
date: 2022-06-08
tags:
 - React
categories:
 - React
---

## 给html标签设置ref="userName"

通过这个获取`this.$refs.username`，ref获取到应用的真实DOM

## 给组件设置ref="userName"

通过这个获取`this.$refs.username`，ref获取到组件对象

## 新的写法

上述写法已废弃，严格模式写无法使用；新的写法如下：

```javascript
myRef = React.createRef();

<div ref={this.myRef}>hello</div>

访问this.myRef.current
```

## 案例

```javascript
import React, { Component } from "react";

class App extends Component {
    myRef = React.createRef();
    render() {
        return(
            <div>
                <input type="text" ref={ this.myRef } />
                <button onClick={ this.handleClick }>点击</button>
            </div>
        )
    }
    handleClick = () => {
        console.log(this.myRef); // {current: input}
        console.log(this.myRef.current); // <input type="text">
        console.log(this.myRef.current.value); // 文本框输入的值
    }
}


export default App;
```
