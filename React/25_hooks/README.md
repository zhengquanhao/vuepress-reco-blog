---
title: hooks
date: 2022-06-10
tags:
 - React
categories:
 - React
---

## 使用hooks理由

1. 高阶组件为了复用，导致代码层级复杂
2. 类组件生命周期复杂
3. 最开始代码写成function无状态组件，后来开发又发现需要状态再改成类组件，成本太高

## useState(保存状态组件)

```javascript
const [state, setState] = useState(initialState)
```

```javascript
import { useState } from "react";

export default function App() {
    const [name, setName] = useState("");
    const [age, setAge] = useState();

    return(
        <div>
            {name}--{age}
            <button onClick={() => {
                setName("Ron");
                setAge(18)
            }}>点击</button>
        </div>
    )
}
```

## useEffect（处理副作用）和useLayoutEffect(同步执行副作用)

函数式组件没有生命周期，只有类组件有，所以不要把类组件的生命周期概念搬到函数式组件中对号入座。

比如发送ajax这种组件一加载就应该做的事为什么不能在直接写在函数式组件的function里面？因为setList后会让函数重新执行，这样就会不断的发送请求

```javascript
useEffect(() => {
    // Effect
    return () => {
        // cleanup
    }
}, [依赖的状态；空数组表示不依赖])
```

```javascript
import { useEffect, useState } from "react";

export default function App() {
    const [list, setList] = useState([]);

    useEffect(() => {
        // Effect
        axios.get("/test.json").then(res => {
            setList(res.data)
        })
    }, []) // 传[]只会发送一次 []不会影响effect
    return(
        <div>
            {list}
        </div>
    )
}
```

不要对Dependencies撒谎，如果你明明使用了某个变量，却没有申明在依赖中，你等于向React撒了谎，后果就是当依赖的变量改变时，useEffect也`不会再次执行`，eslint会报警告

不依赖任何状态可以传[]，用了什么依赖就要传什么状态。

```javascript
// 比如此案例，点击按钮后仍然要让首字母大写
import { useEffect, useState } from "react";

export default function App() {
    const [name, setName] = useState("ron");

    useEffect(() => {
        setName(name.substring(0, 1).toUpperCase() + name.substring(1))
    }, [name]) // 第一次执行一次，之后name（依赖）更新也会执行
    return(
        <div>
            {name}
            <button onClick={() => {
                setName('xiaoming')
            }}>click</button>
        </div>
    )
}
```

在函数式组件页面卸载怎么做？页面卸载的逻辑可以在useEffect的return函数中进行处理

```javascript
import { useEffect, useState } from "react";

export default function App() {
    const [show, setShow] = useState(true);
    return(
        <div>
            <button onClick={() => {
                setShow(false)
            }}>click</button>
            {show && <Child />}
        </div>
    )
}

function Child () {
    useEffect(() => {
        let timer = setInterval(() => {
            console.log(1);
        }, 1000)

        return () => {
            clearInterval(timer);
        }
    }, [])
    return (
        <div>Child</div>
    )
}
```

useEffect注意点:

- useEffect可以使用多次
- useEffect和useLayoutEffect的区别：
    - 简单来说调用时机不同，useLayoutEffect和原来componentDidMount&componentDidUpdate一致，在react完成DOM更新后马上同步调用的代码，会阻塞渲染，而useEffect是会在整个页面渲染完才会调用
    - 官方建议使用useEffect
    - 需要操作DOM时用useLayoutEffect，不然用useEffect

## useCallback

防止因为组件重新渲染，导致方法被重新创建，起到缓存作用，只有第二个参数变化了，才重新声明一次

useState => 记忆状态，可以记住状态变量
useCallback => 把函数记忆下来

```javascript
var handleClick = useCallback(() => {
    console.log(name)
}, [name])
<button onClick={() => handleClick()}>hello</button>
// 只有name改变后，这个函数才会重新声明一次
// 如果传入空数组，那么就是第一次创建后就被缓存，如果name后期改变了，拿到的还是老name
// 如果不传第二个参数，每次都会重新声明一次，拿到的就是最新的name
```

## useMemo 记忆组件（等价于vue的计算属性）

useCallback的功能完全可以由useMemo所取代，如果你想通过使用useMemo返回一个记忆函数也是完全可以的

```javascript
useCallback(fn, inputs) is equivalent to useMemo(()=> fn, inputs)
```

唯一的区别是：**useCallback不会执行第一个参数函数，而是将它返回给你，而useMemo会执行第一个函数并将函数执行结果返回。**所以在前面的例子中，可以返回handleClick来达到存储函数的目的。

所以useCallback常用记忆事件函数，生成记忆后的事件函数并传递给子组件使用。而useMemo更适合经过函数计算得到一个确定的值，比如记忆组件。

```javascript
function Parent({ a, b }) {
  // Only re-rendered if `a` changes:
  const child1 = useMemo(() => () => <Child1 a={a} />, [a]);
  // Only re-rendered if `b` changes:
  const child2 = useMemo(() => () => <Child2 b={b} />, [b]);
  return (
    <>
      {child1}
      {child2}
    </>
  )
}
```

## useRef 保存引用值

useRef 跟 createRef 类似，都可以用来生成对 DOM 对象的引用，看个简单的例子：

```javascript
import React, { useState, useRef } from "react";
function App() {
  let [name, setName] = useState("Nate");
  let nameRef = useRef();
  const submitButton = () => {
    setName(nameRef.current.value);
  };
  return (
    <div className="App">
      <p>{name}</p>

      <div>
        <input ref={nameRef} type="text" />
        <button type="button" onClick={submitButton}>
          Submit
        </button>
      </div>
    </div>
  );
}
```

如何保存一个变量？

1. 使用状态 state
2. 使用useRef

想要在组件中记录一些值，并且这些值在稍后可以更改，可以使用useRef。

```javascript
function App() {
  const count = useRef(0);

  const handleClick = number => {
    count.current = count.current + number;
  };

  return (
    <div>
      <p>You clicked {count.current} times</p>
      <button onClick={() => handleClick(1)}>增加 count</button>
      <button onClick={() => handleClick(-1)}>减少 count</button>
    </div>
  );
}
```

## useContext减少组件层级

优化组件通信过程。

生产消费者模式简化：

```javascript
const colorContext = React.createContext("gray");
function Bar() {
  const color = useContext(colorContext);
  return <div>{color}</div>;
}
function Foo() {
  return <Bar />;
}
function App() {
  return (
    <colorContext.Provider value={"red"}>
      <Foo />
    </colorContext.Provider>
  );
}
```

## useReducer

useReducer 这个 Hooks 在使用上几乎跟 Redux/React-Redux 一模一样，唯一缺少的就是无法使用 redux 提供的中间件。经常和useContext配合使用。

```javascript
import React, { useReducer } from "react";
const initialState = {
  count: 0
};
function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return { count: state.count + action.payload };
    case "decrement":
      return { count: state.count - action.payload };
    default:
      throw new Error();
  }
}
function App() {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <>
      Count: {state.count}
      <button onClick={() => dispatch({ type: "increment", payload: 5 })}>
        +
      </button>
      <button onClick={() => dispatch({ type: "decrement", payload: 5 })}>
        -
      </button>
    </>
  );
}
```
