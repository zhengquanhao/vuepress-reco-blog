---
title: Css Module
date: 2022-06-14
tags:
 - React
categories:
 - React
---

目的：为了防止多文件css类目相同而互相影响。

1. 文件名修改
film.css => film.module.css

2. 引用和使用上修改

```css
/* film.module.css */
.select {
    color: red;
}
```

```javascript
import style from "./css/file.module.css";

<NavLink to="/home" activeClass={style.select}>link</NavLink>
```

3. 加上:global表示影响全局

`:global(.select)`
