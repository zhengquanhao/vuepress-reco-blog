---
title: 案例：表单域组件
date: 2022-06-09
tags:
 - React
categories:
 - React
---

## 父子通信版

```javascript
import { Component } from "react";

class Felid extends Component {
    render() {
        return(<div>
            {this.props.label}
            <input type={this.props.type} onChange={(e)=> {
                this.props.changeEvent(e.target.value);
            }} value={this.props.value} />
        </div>)
    }
}

class App extends Component {
    state = {
        userName: "",
        password: ""
    }
    render() {
        return (
            <div>
                <Felid label="用户名" type="text" value={this.state.userName} changeEvent={(value)=>{
                    this.setState({
                        userName: value
                    })
                }} />
                <Felid label="密码" type="text" value={this.state.password} changeEvent={(value) => {
                    this.setState({
                        password: value
                    })
                }}/>
                <button onClick={() => this.handleClick()}>登录</button>
                <button onClick={() => this.handleClear()}>清除</button>
            </div>
        )
    }
    handleClick = () => {
        console.log(this.state.userName);
        console.log(this.state.password);
    }

    handleClear = () => {
        this.setState({
            userName: "",
            password: ""
        })
    }
}

export default App;
```

## ref版

```javascript
import { Component, createRef } from "react";

class Felid extends Component {
    state = {
        value: ""
    }
    render() {
        return(<div>
            {this.props.label}
            <input type={this.props.type} value={this.state.value} onChange={(e) => {
                this.setState({
                    value: e.target.value
                })
            }} />
        </div>)
    }
    clear() {
        this.setState({
            value: ""
        })
    }
}

class App extends Component {
    userName = createRef()
    password = createRef()
    render() {
        return (
            <div>
                <Felid label="用户名" type="text" ref={this.userName} />
                <Felid label="密码" type="text" ref={this.password} />
                <button onClick={() => this.handleClick()}>登录</button>
                <button onClick={() => this.handleClear()}>清除</button>
            </div>
        )
    }
    handleClick = () => {
        console.log(this.userName.current.state.value);
        console.log(this.password.current.state.value);
    }

    handleClear = () => {
        this.userName.current.clear();
        this.password.current.clear();
    }
}

export default App;
```
