---
title: 父子组件通信
date: 2022-06-09
tags:
 - React
categories:
 - React
---

**父传子传属性，子传父原理为在子组件中调用父组件传来的函数。**

案例：

子组件发送请求让父组件展示文本。

```javascript
import { Component } from "react";

class Child extends Component {
    render () {
        return (
            <div>
                子组件
                <button onClick={() => {this.props.clickEvent()}}>点击</button>
            </div>
        )
    }
}

class App extends Component {
    state = {
        isShow: false
    }
    render() {
        return (
            <div>
                <Child clickEvent={() => this.showText()} />
                {this.state.isShow && '展示'}
            </div>
        )
    }
    showText() {
        console.log("通过子组件触发在父组件中执行");
        this.setState({
            isShow: !this.state.isShow
        });
    }
}

export default App;
```
