---
title: React反向代理
date: 2022-06-14
tags:
 - React
categories:
 - React
---

安装：

```shell
npm install http-proxy-middleware --save
```

Next, create `src/setupProxy.js` and place the following contents in it:

```javascript
const { createProxyMiddleware } = require('http-proxy-middleware');


module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://localhost:5000',
      changeOrigin: true,
    })
  );
};
```

[官方文档](https://create-react-app.dev/docs/proxying-api-requests-in-development/)
