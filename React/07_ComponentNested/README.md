---
title: 组件的嵌套
date: 2022-06-08
tags:
 - React
categories:
 - React
---

```javascript
import { Component } from "react";

class Child extends Component {
    render() {
        return(
            <div>Child</div>
        )
    }
}

class TopBar extends Component {
    render() {
        return (
            <div>
                TopBar
                <Child />
            </div>
        )
    }
}

class Swiper extends Component {
    render() {
        return (
            <div>Swiper</div>
        )
    }
}

class App extends Component {
    render() {
        return(
            <div>
                <TopBar />
                <Swiper />
            </div>
        )
    }
}

export default App;
```
