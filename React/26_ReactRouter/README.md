---
title: React路由
date: 2022-06-13
tags:
 - React
categories:
 - React
---

## 安装和引入

安装：(最新@6版本)

```shell
npm install react-router-dom@5
```

使用：

```javascript
import {Component} from "react";
import {HashRouter, Route} from "react-router-dom"

export default class App extends Component {
    render() {
        return (
            <div>
                <HashRouter>
                    <Route path="/home" component={Home}></Route>
                    <Route path="/about" component={About}></Route>
                </HashRouter>
            </div>
        )
    }
}

// 最好把组件放到/views/***下
class Home extends Component {
    render() {
        return(
            <div>Home</div>
        )
    }
}

class About extends Component {
    render() {
        return(
            <div>About</div>
        )
    }
}
```

## 重定向


```javascript
import {Component} from "react";
import {HashRouter, Route, Redirect} from "react-router-dom"

export default class App extends Component {
    render() {
        return (
            <div>
                <HashRouter>
                    <Route path="/home" component={Home}></Route>
                    <Route path="/about" component={About}></Route>
                    <Redirect from="/" to="/home"></Redirect>
                </HashRouter>
            </div>
        )
    }
}
```

但上述有问题，因为/是模糊匹配，在about页面刷新页面后会到home页面，所以需要用Switch包裹，表示遇到匹配命中的直接跳出。

使用exact关键字变成精确匹配。

```javascript
import {Component} from "react";
import {HashRouter, Route, Redirect, Switch} from "react-router-dom"

export default class App extends Component {
    render() {
        return (
            <div>
                <HashRouter>
                    <Switch>
                        <Route path="/home" component={Home}></Route>
                        <Route path="/about" component={About}></Route>
                        <Redirect from="/" to="/home" exact></Redirect>
                    </Switch>
                </HashRouter>
            </div>
        )
    }
}
```

## 404页面

不写path属性，只写component，遇到不匹配返回404

```javascript
import {Component} from "react";
import {HashRouter, Route, Redirect, Switch} from "react-router-dom"

export default class App extends Component {
    render() {
        return (
            <div>
                <HashRouter>
                    <Switch>
                        <Route path="/home" component={Home}></Route>
                        <Route path="/about" component={About}></Route>
                        <Redirect from="/" to="/home" exact></Redirect>
                        <Route component={NotFound}></Route>
                    </Switch>
                </HashRouter>
            </div>
        )
    }
}
```

## 嵌套路由

在一级路由组件下写二级路由

```javascript
import {Component} from "react";
import {HashRouter, Route, Redirect, Switch} from "react-router-dom"

export default class App extends Component {
    render() {
        return (
            <div>
                <HashRouter>
                    <Switch>
                        <Route path="/home" component={Home}></Route>
                        <Route path="/about" component={About}></Route>
                        <Redirect from="/" to="/home" exact></Redirect>
                        <Route component={NotFound}></Route>
                    </Switch>
                </HashRouter>
            </div>
        )
    }
}

// 最好把组件放到/views/***下
class Home extends Component {
    render() {
        return(
            <div>
                Home
                <Switch>
                    <Route path="/home/page1" component={Page1}></Route>
                    <Route path="/home/page2" component={Page2}></Route>
                    <Redirect from="/home" to="/home/page1"></Redirect>
                </Switch>
            </div>
        )
    }
}

class About extends Component {
    render() {
        return(
            <div>About</div>
        )
    }
}

class NotFound extends Component {
    render() {
        return(
            <div>NotFound</div>
        )
    }
}

class Page1 extends Component {
    render() {
        return(
            <div>Page1</div>
        )
    }
}

class Page2 extends Component {
    render() {
        return(
            <div>Page2</div>
        )
    }
}
```

## 声明式和编程式导航

声明式：

```javascript
import { NavLink} from 'react-router-dom'
<NavLink activeClassName="selected" to='/aside'>二级菜单</NavLink>
```

编程式：

用了类似vue3中useRouter( )的导航方式，使用了react中的新增的 useHistory（） 钩子（hook）来做函数式组件的编程式导航，请看示例代码： 注意： 该钩子函数只能用于函数组件

```javascript
import { useHistory } from 'react-router';//导入useHistory钩子
import { Button } from 'antd';
export default function Header(){
  let history = useHistory() //将useHistory()钩子赋值给history方便使用
  let programmingLink = function(){
    history.push('/home')//点击按钮后使用push方法跳转到对应的路由位置
  }

  return(
   <div className='header'>
      <div className='btns'>
        <Button type="primary" onClick={programmingLink} ghost>Dashed</Button>
      </div>
   </div>
 )
}
```

在类式组件中进行编程式跳转【Route】

```javascript
import { Component } from 'react'
import { Route } from 'react-router'
export default class Footer extends Component {
  render () {
    return (
      <>
        <Route render={({ history }) => (<button onClick={() => { history.push('/changLink') }}>点击更换路由</button>)} />
      </>
    )
  }
}
```

## 动态路由

```javascript
// /detail/111 动态路由

export default function Detail (props) {
    console.log(props.match.params.myId);
}

<Route path="/detail/:myId" component={Detail}>
```

## 路由传参

```javascript
// 1、query
this.props.history.push({pathname: "/user", query: {id: '111'}})
this.props.location.query.id
// 2、state
this.props.history.push({pathname: "/user", state: {id: '111'}})
this.props.location.state.id
```

## 路由拦截（路由守卫）

有些页面是需要登录验证的，没有登录前一些页面跳转到登录页

```javascript
<Route path="/center" component={Center} />
等价于
<Route path="/center" render={() => <Center />} />
```

但是render函数内可以做路由拦截逻辑：

```javascript
<Route path="/center" render={() => {
    return isAuth ? <Center /> : <Redirect to="/login" />
}} />
```

## 路由模式

- history => `<BrowserRouter>`
- hash => `<HashRouter>`

## withRouter

通过render函数定义的路由，在子组件中无法直接拿到props，需要进行传递

```javascript
<Route path="/center" render={(props) => {
    return isAuth ? <Center {...props}/> : <Redirect to="/login" />
}} />
// 子组件
props.history.push("/home")
```

```javascript
import {withRouter} from "react-router-dom";

function Child () {

}

const WithChild = withRouter(Child); // 可以理解成Child的props（父组件传递的参数）没有router，那Child就给自己又多找了个爹叫withRouter，新爹给提供了router方法

// <Child> => 无法使用router API；WithChild进行增强可以在props没有router信息的情况下使用router
<WithChild>
```
