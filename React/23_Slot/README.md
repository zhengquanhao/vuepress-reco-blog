---
title: 插槽
date: 2022-06-10
tags:
 - React
categories:
 - React
---

React中的插槽没有像Vue一样封装成了slot，而是把结构挂载属性上`children`上。

插槽的作用：

1. 为了组件能更好的复用
2. 一定程度上减少了父子组件之间的通信

## 基本使用

```javascript
import { Component } from "react";

class App extends Component {
    render() {
        return (
            <div>
                <Child1>
                    <div>
                        <p>123</p>
                        <span>456</span>
                    </div>
                </Child1>
            </div>
        )
    }
}

class Child1 extends Component {
    render() {
        return(
            <div>
                Child1
                {this.props.children}
            </div>
        )
    }
}

export default App;
```

## 多项取值

```javascript
import { Component } from "react";

class App extends Component {
    render() {
        return (
            <div>
                <Child1>
                    {/* children[0] */}
                    <div>
                        <p>123</p>
                        <span>456</span>
                    </div>
                    {/* children[1] */}
                    <Child2 />
                    {/* children[2] */}
                    <a href="www.aaa.com">链接</a>
                </Child1>
            </div>
        )
    }
}

class Child1 extends Component {
    render() {
        return(
            <div>
                Child1
                {this.props.children[0]}
                {this.props.children[1]}
                {this.props.children[2]}
            </div>
        )
    }
}

class Child2 extends Component {
    render() {
        return(
            <div>
                Child2
            </div>
        )
    }
}

export default App;
```
