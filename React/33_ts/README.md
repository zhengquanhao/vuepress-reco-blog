---
title: ts
date: 2022-06-16
tags:
 - React
categories:
 - React
---

## ts介绍

1. TypeScript 的定位是静态类型语言，在写代码阶段就能检查错误，而非运行阶段
2. 类型系统是最好的文档，增加了代码的可读性和可维护性。
3. 有一定的学习成本，需要理解接口(Interfaces)、泛型(Generics)、类(Classes)等
4. ts最后被编译成js

## 创建react-ts项目

```shell
create-react-app my-app --template typescript
```

## 定义普通函数

```javascript
interface SearchFunc {
  (source: string, subString: string): boolean;
}
//对于函数类型的类型检查来说，函数的参数名不需要与接口里定义的名字相匹配。
let mySearch: SearchFunc;
mySearch = function(src: string, sub: string): boolean {
    let result = src.search(sub);
    return result > -1;
}
```

传参：

```javascript
function Test(list:String[],text?:String,...args:String[]):void{
    console.log(list,text,args)
}
Test(["1111","2222"])
//list:["1111","2222"] text: undefined args: []
Test(["0","1"],"a","b","c")
//list:["0","1"] text: "a" args: ["b","c"]
```

类型断言as

```javascript
function Test( myText:string|number ){
    console.log((myText as string).length) //对
    console.log((myText as any).length) //对
    console.log((myText as string[]).length) //错，原声明没有这个类型，无法断言
}
```

## 定义普通类

```javascript
interface MyInter {
    name: string, // 必选属性
    readonly country:string, // 只读属性
    getName(): void // 定义方法
}

class MyObj implements MyInter{
    name="ron"
    country="China"
    private age = 100 //私有属性， 不能在接口定义
    getName() {
        //...
    }
    private getAge() {
        //...
    } //私有方法， 不能在接口定义
}
```

## 定义类组件

```javascript
export default class App extends Component<约定属性, 约定状态> {}

interface IState = {
    name: string
} // 这样setState的时候便会被检测
export default class App extends Component<any, IState> {}

// 子组件
interface IProps {
    name: string
}

class Child extends Component<IProps, any> {
    render () {
        return <div>{this.props.name}</div>
    }
}
```

```javascript
interface PropInter {
name: string | number; firstName?: string;//可选属性 lastName?: string;//可选属性
// [propName: string]: any 任意属性
}
interface StateInter {
   count: number
}
//根组件 ，第一个参数可以传any
class HelloClass extends React.Component<PropInter, StateInter> {
   state: State = {
      count: 0,
}; //setState时候也才会检查
static defaultProps = { // 属性默认值
      name: "default name"
      firstName: "",
      lastName: "",
}; }
```

## 定义函数式组件

```javascript
// 根组件
const App:React.FC = (props)=>{
console.log(props)
const [name, setname] = useState<string>("kerwin") return <div>
app
</div> }
//子组件接受属性 -1
interface iprops {
    count:number
}
const Child:React.FC<iprops> = (props)=>{
    return <div>
        child-{props.count}
    </div>
}
//子组件接受属性 -2
const Child = (props:iprops)=>{
    return <div> child-{props.count}</div>
}
```

useRef

```javascript
const mytext = useRef<HTMLInputElement>(null)

<input type="text" ref={mytext}/>
    useEffect(() => {
    console.log(mytext.current && mytext.current.value)
}, [])
```

useContext

```javascript
interface IContext{
    call:string
}
const GlobalContext = React.createContext<IContext>({
    call:"" //定义初始值,按照接口规则
})

<GlobalContext.Provider value={{
    call:"电话"
}}>
</GlobalContext.Provider>
const {call} = useContext(GlobalContext)
```

useReducer

```javascript
interface IPrevState{
    count:number
}
interface IAction{
    type:string,
    payload:any
}
function reducer (prevState:IPrevState,action:IAction){
    .....
    return prevState
}
const [state,dispatch]= useReducer(reducer,{
        count:1
})
 dispatch({
    type:"Action1",
    payload:[]
})
```

## 父子通信

```javascript
//父组件调用
<Child key={index} item={item} index={index} cb={(index)=>{
    var newlist= [...list]
    newlist.splice(index,1)
    setList(newlist)
}}/>
//子组件
interface ItemType{
    item:string,
    index:number, //定义接口
    cb:(param:number)=>void //定义接口
}
const Child = (props:ItemType)=>{
    let {index,item,cb} = props
    return <div >{item}
        <button onClick={()=>cb(index)}>del-{index}</button>
    </div>
}
```

## 路由

声明

1. 可以在当前文件加上declare const $: any;
2. 安装 npm i @types/jquery @types是npm的一个分支，用来存放*.d.ts文件

```shell
npm i --save react-router-dom
npm i --save @types/react-router-dom //编译器需要通过这个声明文件，进行类型检查工作
```

编程式导航

```javascript
// 使用编程式导航，需要引入接口配置
import { RouteComponentProps } from "react-router-dom";

interface IProps {自己定义的接口}

type HomeProps = IProps & RouteComponentProps; //两个接口属性都支持

interface IState {}

class Home extends React.Component<HomeProps, IState> {
    private handleSubmit = async () => {
    //code for API calls
        this.props.history.push("/home"); };
    public render(): any {
        return <div>Hello</div>;
    }
}
```

动态路由

```javascript
interface IParams{
    id:string
}
// RouteComponentProps是一个泛型接口
class Detail extends Component< RouteComponentProps<IParams> >{
    componentDidMount() {
        console.log(this.props.match.params.id)
    }
    render(){
        return <div>
            detail
        </div>
    }
}
```

## redux

```javascript
import {createStore} from 'redux'
interface ActionInter{
    type:String,
    payload:any
}
const reducer = (prevState={},action:ActionInter)=>{
    return action.payload
}
const store = createStore(reducer,//enhancer)
export default store;
```
