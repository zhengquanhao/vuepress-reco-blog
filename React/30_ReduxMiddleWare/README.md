---
title: Redux中间件
date: 2022-06-14
tags:
 - React
categories:
 - React
---

## 常见中间件

有很多优秀的redux中间件，如：

- redux-thunk：用于异步操作
- redux-logger：用于日志记录

上述的中间件都需要通过`applyMiddlewares`进行注册，作用是将所有的中间件组成一个数组，依次执行

然后作为第二个参数传入到`createStore`中

```javascript
const store = createStore(
  reducer,
  applyMiddleware(thunk, logger)
);
```

## redux-thunk

`redux-thunk`是官网推荐的异步处理中间件(异步情况下返回一个对象存在问题，因为异步结果未返回时对象就已经return回去了)

默认情况下的`dispatch(action)`，`action`需要是一个JavaScript的对象

`redux-thunk`中间件会判断你当前传进来的数据类型，如果是一个函数，将会给函数传入参数值`（dispatch，getState）`

- dispatch函数用于我们之后再次派发action
- getState函数考虑到我们之后的一些操作需要依赖原来的状态，用于让我们可以获取之前的一些状态

所以`dispatch`可以写成下述函数的形式：

```javascript
const getHomeMultidataAction = () => {
  return (dispatch) => {
    axios.get("http://xxx.xx.xx.xx/test").then(res => {
      const data = res.data;
      dispatch({
          type: "change-list",
          payload: data.recommend.list});
    })
  }
}
```

## redux-logger

如果想要实现一个日志功能，则可以使用现成的`redux-logger`

```javascript
import { applyMiddleware, createStore } from 'redux';
import createLogger from 'redux-logger';
const logger = createLogger();

const store = createStore(
  reducer,
  applyMiddleware(logger)
);
```

这样我们就能简单通过中间件函数实现日志记录的信息

## redux开发者工具

https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=zh_CN

## 使用react-redux

可以先结合context来手动连接react和redux。

react-redux提供两个核心的api：

- Provider: 提供store
- connect: 用于连接容器组件和展示组件
    - Provider: 根据单一store原则 ，一般只会出现在整个应用程序的最顶层。
    - connect: 语法格式为

```javascript
connect(mapStateToProps?, mapDispatchToProps?, mergeProps?, options?)(component)
```

一般来说只会用到前面两个，它的作用是：

- 把`store.getState()`的状态转化为展示组件的props
- 把`actionCreators`转化为展示组件props上的方法

只要上层中有Provider组件并且提供了store, 那么，子孙级别的任何组件，要想使用store里的状态，都可以通过connect方法进行连接。如果只是想连接actionCreators，可以第一个参数传递为null

react-redux原理：

```javascript
function ronConnect(cb, obj) {
    let value = cb();
    return(MyComponent) => {
        return (props) => {
            return <div>
                <MyComponent {...value} {...obj} {...props}/>
            </div>
        }
    }
}

export default ronConnect(() => {
    return {
        a: 1,
        b: 2
    }
}, {
    aa() {},
    bb() {}
})(Child)
```

## redux持久化

是否存储在locationStorage？

有些场景可以存在locationStorage，比如切换城市功能的所选城市。

使用(`redux-persist`)[https://www.npmjs.com/package/redux-persist]实现redux持久化
