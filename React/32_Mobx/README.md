---
title: Mobx
date: 2022-06-15
tags:
 - React
categories:
 - React
---

## Mobx介绍

1. Mobx是一个功能强大，上手非常容易的状态管理工具。
2. Mobx背后的哲学很简单：任何源自应用状态的东西都应该自动地获得。
3. Mobx利用getter,setter来收集组件的数据依赖关系，从而在数据发生变化的时候精准知道哪些组件需要重绘，在外界的规模变化的时候，往往会有跟多细粒度更新（Vue类似）；不像redux更新状态时把所有的reducer过一遍。

**Mobx与redux区别**

- Mobx写法更偏向于面向对象编程
- 对一份数据直接进行修改操作，不需要始终返回一个新数据
- 并非单一store，可以多store
- Redux默认以JavaScript原生对象形式存储数据，而Mobx使用可观察对象

**优缺点**

优点：

- 学习成本低
- 面向对象编程，对TS友好

缺点：

- 过于自由，Mobx提供的约定及模板代码很少，代码编写很自由，如果不做一些约定比较容易导致团队代码风格不统一。
- 相关的中间件很少，逻辑层业务整合是问题。

## Mobx使用

autorun是认人的，方法里没有用到，及时改变了也不触发函数。

```javascript
import React, {Component} from "react";
import {observable, autorun} from "mobx";

// 对于普通类型数据的监听
var observableNumber = observable.box(10);

autorun(() => {
    // 页面刚加载会执行，之后每次改变也会执行
    console.log(observableNumber.get());
})

setTimeout(() => {
    observableNumber.set(20)
}, 1000)

// 对于对象数据监听方式一：使用map
var myObj = observable.map({
    name: "Ron",
    age: 100
})

atuorun(() => {
    console.log(myObj.get("name"))
})

myObj.set("age", 18) // 执行这句话不会再次打印name，因为改的是age

// 对于对象数据监听方式二，直接使用对象
var myObj = observable({
    name: "Ron",
    age: 100
})

atuorun(() => {
    console.log(myObj.name)
})

setTimeout(() => {
    myObj.name = "xiaoming"
}, 1000)
```

**action，runInAction和严格模式**

```javascript
import {observable, action, configure,runInAction} from 'mobx';
configure({enforceActions:'always'})
//严格模式， 必须写action,
//如果是never，可以不写action,
//最好设置always, 防止任意地方修改值， 降低不确定性。
class Store {
  @observable number = 0;
  @observable name = "kerwin";
  @action add = () => {
    this.number++;
  }//action只能影响正在运行的函数，而无法影响当前函数调用的异步操作
  @action load = async () => {
    const data = await getData();
    runInAction(() => {
      this.name = data.name;
    });
    }// runInAction 解决异步问题
  }
  const newStore = new Store();
  newStore.add();
  //如果在组件监听
  componentDidMount() {
    autorun(()=>{
      console.log(newStore.number);
    })
}
```

## mobx-react使用

和redux-react类似，不需要自己进行监听，帮着我们自动监听。
