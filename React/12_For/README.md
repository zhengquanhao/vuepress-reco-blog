---
title: 循环渲染
date: 2022-06-08
tags:
 - React
categories:
 - React
---

利用JS的map方法。

## 渲染列表

```javascript
import React, { Component } from "react";

class App extends Component {
    state = {
        list: ["Ron", "Jack", "Marry"]
    }
    render() {
        return(
            <ul>
                {this.state.list.map(item => <li key={item}>{item}</li>)}
            </ul>
        )
    }
}


export default App;
```

或者将渲染逻辑提取：

```javascript
import React, { Component } from "react";

class App extends Component {
    state = {
        list: ["Ron", "Jack", "Marry"]
    }
    render() {
        const newList = this.state.list.map(item => <li key={item}>{item}</li>)
        return(
            <ul>
                { newList }
            </ul>
        )
    }
}


export default App;
```

## 渲染对象列表

```javascript
import React, { Component } from "react";

class App extends Component {
    state = {
        list: [
            {
                id: 1,
                name: "Ron",
                age: 18
            },
            {
                id: 2,
                name: "Jack",
                age: 20
            },
            {
                id: 3,
                name: "Marry",
                age: 21
            }
        ]
    }
    render() {
        const newList = this.state.list.map(item => {
            return (
                <li key={item.id}>
                    <p>姓名：{item.name}</p>
                    <p>年龄：{item.age}</p>
                </li>
            )
        })
        return(
            <ul>
                { newList }
            </ul>
        )
    }
}


export default App;
```
