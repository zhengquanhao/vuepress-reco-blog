---
title: Flux与Redux介绍
date: 2022-06-14
tags:
 - React
categories:
 - React
---

## Flux与Redux介绍

Flux是一种架构思想，专门解决软件的结构问题。它跟MVC架构是同一类东西，但是更加简单和清晰。Flux存在多种实现（Redux是其中一种）

Facebook Flux是用来构建客户端Web应用的应用架构。它利用`单向数据流`的方式来组合React中的视图组件。它更像一个模式而不是一个正式的框架，开发者不需要太多的新代码就可以快速上手Flux。

![flux](./images/flux.png)

Redux最主要是用作状态的管理。简而言之，Redux用一个单独的常量状态数（state对象）保存一整个应用的状态，这个对象不能直接被改变。当一些数据发生变化了，一个新的对象就会被创建（使用actions和reducers）这样就可以进行数据追踪，实现时光旅游。

## Redux三大原则

- state以单一对象存储store对象中
- state只读
- 使用纯函数reducer执行state更新

## Redux工作流

![flow](./images/flow.png)

## Redux案例

![anli](./images/anli.png)

## 划分reducer

因为一个应用中只能有一个大的state，这样的话reducer中的代码将会特别特别的多，那么就可以使用combineReducers方法将已经分开的reducer合并到一起

```javascript
import {combineReducers} from "redux"
import CityReducer from "./reducers/CityReducer"
import TabbarReducer from "./reducers/TabbarReducer"

const reducer = combineReducers({
    CityReducer,
    TabbarReducer
})

const store = createStore(reducer)

export default store;

// 使用需要加上CityReducer
state.CityReducer
```
