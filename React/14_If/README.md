---
title: 条件渲染
date: 2022-06-08
tags:
 - React
categories:
 - React
---

利用三元运算符/&&运算符

```javascript
{/* 直接去除元素 */}
{this.state.list.length === 0 ? <div>列表为空</div> : null}
{this.state.list.length === 0 && <div>列表为空</div>}
{/* 元素通过display控制 */}
<div style={{display: this.state.list.length === 0 ? 'block' : 'none'}}>列表为空</div>
```

完整代码：

```javascript
import React, { Component } from "react";

class App extends Component {
    myRef = React.createRef();
    state = {
        list: ["Ron", "Jack", "Marry"]
    }
    render() {
        return(
            <div>
                <input type="text" ref={ this.myRef } />
                <button onClick={ this.add }>添加</button>
                <ul>
                    {this.state.list.map((item, i) => {
                        return (
                            <li key={item}>
                                {item}
                                <button onClick={() => this.remove(i)}>Remove</button>
                            </li>
                        )
                    })}
                </ul>
                {/* 直接去除元素 */}
                {this.state.list.length === 0 ? <div>列表为空</div> : null}
                {this.state.list.length === 0 && <div>列表为空</div>}
                {/* 元素通过display控制 */}
                <div style={{display: this.state.list.length === 0 ? 'block' : 'none'}}>列表为空</div>
            </div>
        )
    }
    add = () => {
        let newList = [...this.state.list];
        newList.push(this.myRef.current.value);

        this.setState({
            list: newList
        })
    }
    remove = (index) => {
        let newList = [...this.state.list];
        newList.splice(index, 1);
        this.setState({
            list: newList
        })
    }
}


export default App;
```
