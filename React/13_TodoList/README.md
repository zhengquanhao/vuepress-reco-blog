---
title: TodoList案例
date: 2022-06-08
tags:
 - React
categories:
 - React
---

```javascript
import React, { Component } from "react";

class App extends Component {
    myRef = React.createRef();
    state = {
        list: ["Ron", "Jack", "Marry"]
    }
    render() {
        return(
            <div>
                <input type="text" ref={ this.myRef } />
                <button onClick={ this.add }>添加</button>
                <ul>
                    {this.state.list.map((item, i) => {
                        return (
                            <li key={item}>
                                {item}
                                <button onClick={() => this.remove(i)}>Remove</button>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
    add = () => {
        // 不要直接修改状态，会造成不可预期的问题
        // this.state.list.push(this.myRef.current.value)
        // 可以创建一个新变量，但也不要直接赋值，因为这样的深拷贝
        // let newList = this.state.list;
        // 可以使用扩展运算符
        let newList = [...this.state.list];
        newList.push(this.myRef.current.value);

        this.setState({
            list: newList
        })
    }
    remove = (index) => {
        let newList = [...this.state.list];
        newList.splice(index, 1);
        this.setState({
            list: newList
        })
    }
}


export default App;
```

> 获取文本框的值可以不使用ref，也可以使用 `e.target.value` 获取。
