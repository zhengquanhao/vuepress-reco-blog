---
title: 表单的受控与非受控
date: 2022-06-09
tags:
 - React
categories:
 - React
---

React中的组件分为受控组件和非受控组件

受控组件的两个要点：

- 组件的`value`属性与`React`中的`状态`绑定
- 组件内声明了`onChange`事件处理`value`的变化

非受控组件更像是传统的HTML表单元素，数据存储在 `DOM` 中，而不是组件内部，获取数据的方式是通过 `ref` 引用

## 受控

```javascript
import { Component } from "react";

class App extends Component {
    state = {
        value: "hello"
    }
    render() {
        return (
            <div>
                <input type="text" ref={ this.myText } value={this.state.value} onChange={(e) => {
                    this.setState({
                        value: e.target.value
                    });
                }}/>
                <button onClick={() => this.handleClick()}>点击</button>
            </div>
        )
    }
    handleClick = () => {
        console.log(this.state.value);
    }
}

export default App;
```

受控组件的好处：交给state控制，这样当setState把state改变时会重新走render方法。

> 当使用checkbox时不在使用value绑定而是使用checked绑定

```html
<input type="checkbox" checked={this.state.checked} onChange={() => {}} />
```

## 非受控

```javascript
import { Component, createRef } from "react";

class App extends Component {
    myText = createRef()
    render() {
        return (
            <div>
                {/* 想给input一个默认值不能写value，这会导致文本框无法输入，因为这里的input已经不是简单的原生DOM，可以把他看成是React组件所以不识别value */}
                {/* <input type="text" ref={ this.myText } value="hello" /> */}
                {/* 需要使用defaultValue */}
                <input type="text" ref={ this.myText } defaultValue="hello" />
                <button onClick={() => this.handleClick()}>点击</button>
            </div>
        )
    }
    handleClick = () => {
        console.log(this.myText.current.value);
    }
}

export default App;
```

> 非受控的缺点：无法给子组件传递变化的值，当我们把`this.myText.current.value`传递给子组件时预期是不符的