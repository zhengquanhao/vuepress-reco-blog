---
title: immutable.js
date: 2022-06-15
tags:
 - React
categories:
 - React
---

## immutable介绍

之前的案例中，为了不影响原状态，我们一般会复制一个新状态来修改新状态的值，但之前不完全是深拷贝，且使用deepClone深拷贝又太消耗性能，这样便需要使用immutable

每次修改一个immutable对象时都会创建一个新的不可改变的对象，在新对象上操作并不会影响原对象的数据。

**immutable性能优化方式：**

immutable实现的原理是持久化数据结构，也就是旧数据创建新数据的时候，要保证旧数据同时可用且不变。同时为了避免deepClone把所有节点都复制一遍带来的性能损耗，immutable使用了结构共享，即如果对象树中一个节点发生变化，只修改这个节点和守它影响的父节点，其他节点则进行共享。

[演示图](https://upload-images.jianshu.io/upload_images/2165169-cebb05bca02f1772)

## 使用Immutable.js

```javascript
// 拷贝对象
import { Map } from 'immutable'

const map1 = Map({
  a: 1,
  b: 2,
  c: 3
})

const map2 = map1.set('b', 50)
console.log(map1.get('b') + ' vs. ' + map2.get('b'))
```

```javascript
import {Map} from "immutable"

var obj = {
    name: "ron",
    age: 18
}

var oldImmuObj = Map(obj)
var newImmuObj = oldImmuObj.set("name", "xiaoming")

// 获取immutable
console.log(oldImmuObj.get("name"), newImmuObj.get("name"))
```

如果多级里面也需要用map嵌套：

```javascript
state = {
    info: Map({
        name:"ron",
        filter: Map({
            text: "",
            show: true
        })
    })
}
```

如果拷贝数组

```javascript
import {List} from "immutable"

var arr = List([1,2,3])

var arr2 = arr.push(4)

console.log(arr, arr2)
```

把map转化成正常的map对象

```javascript
console.log(oldImmuObj.toJS())
```

> 个人感觉不好用。。。

## immutable进阶

如果数据是后端传来的如何处理，没法加Map/List呀，所以immutable为我们提供了fromJS方法，效果是一样的。

```javascript
state = {
    info: fromJS({
        name: "ron",
        location: {
            province: "辽宁",
            city: "大连"
        },
        favor: ["读书", "看报", "写代码"]
    })
}
```

深层次的设置值和嵌套也提供了`setIn`,`updateIn`等方法。

```javascript
this.setState({
    info: this.state.info.setIn(["name"], "xiaoming").setIn(["location", "city"], "沈阳")
})

this.setState({
    info: this.state.info.setIn(["favor", 0], "111")
})

this.setState({
    info: this.state.info.updateIn(["favor"], (list) => list.splice(index, 1))
})
```
