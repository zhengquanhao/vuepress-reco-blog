---
title: 函数式组件
date: 2022-06-07
tags:
 - React
categories:
 - React
---

## 基本使用

把需要渲染的JSX直接`return`返回。

```javascript
// components/App.js
function App() {
    return (
        <div>hello react</div>
    )
}

export default App;
```

```javascript
// main.js
import React from "react";
import ReactDOM from "react-dom";

import App from "./components/App";

ReactDOM.render(
    <App name="react" />,
    document.getElementById("root")
);
```

## 箭头函数

```javascript
const App = () => {
    return (
        <div>hello react</div>
    )
}

export default App;
```

## 传参

使用`props.xxx`接收参数。

```javascript
const App = (props) => {
    return (
        <div>hello { props.name }</div>
    )
}

export default App;
```

## 函数式组件的弊端

在16.8版本之前，函数式组件是无状态的，这也意味着很多功能无法实现。
在16.8版本之后，引入`react hooks`是上面问题得到解决。

所以当前使用函数式组件场景还是很多的。

## 注意点

同类组件

1. 导入进来的组件名需要大写，`App`而非`app`，小写会认为是浏览器自带标签而报异常。
2. 组件只能有一个根节点。
3. 组件的render函数return 可以加一个`()`表示其jsx是一个整体，否则可能因为换行而返回 `undefined`
