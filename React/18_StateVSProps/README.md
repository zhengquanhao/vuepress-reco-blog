---
title: 状态VS属性
date: 2022-06-09
tags:
 - React
categories:
 - React
---

## 相同点

都是JS对象，对象改变时都会触发render更新。

## 不同点

**状态是组件内部自己定义的数据，属性一般是父组件传递的数据。**

- 属性能从父组件获取，状态不能
- 属性可以由父组件修改，状态不能
- 属性能在内部设置默认值，状态也可以
- 属性不在组件内部修改，状态要改（子组件内部直接改props会报错）
- 属性能设置子组件初始值，状态不可以
- 属性可以修改子组件的值，状态不可以

`state` 的主要作用是用于组件保存、控制、修改自己的可变状态。`state` 在组件内部初始化，可以被组件自身修改，而外部不能访问也不能修改。你可以认为 `state` 是一个局部的、只能被组件自身控制的数据源。`state` 中状态可以通过 `this.setState`方法进行更新，`setState` 会导致组件的重新渲染。

`props` 的主要作用是让使用该组件的父组件可以传入参数来配置该组件。它是外部传进来的配置参数，组件内部无法控制也无法修改。除非外部组件主动传入新的 `props`，否则组件的 `props` 永远保持不变。

如果搞不清 `state` 和 `props` 的使用场景，记住一个简单的规则：**尽量少地用 `state`，多用 `props`**。

没有 `state` 的组件叫`无状态组件`（stateless component），设置了 `state` 的叫做`有状态组件`（stateful component）。因为状态会带来管理的复杂性，我们尽量多地写无状态组件，尽量少地写有状态的组件。这样会降低代码维护的难度，也会在一定程度上增强组件的可复用性。一般是父级有状态的组件控制子级无状态的组件。
