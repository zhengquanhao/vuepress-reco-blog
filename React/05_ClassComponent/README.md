---
title: React类组件
date: 2022-06-07
tags:
 - React
categories:
 - React
---

## 类组件的基本使用

ES6的加入让JavaScript直接支持使用class来定义一个类，react的第一种创建组件的方式就是使用的类的继承，也是最常用的定义组件的方式。

```javascript
// src/components/App.js
import React from "react";

class App extends React.Component {
    render() {
        // 必须有render方法，当实例化组件的时候会自动执行
        return <div>
            hello react
        </div>
    }
}

export default App;
```

```javascript
// main.js
import React from "react";
import ReactDOM from "react-dom";

import App from "./components/App";

ReactDOM.render(
    // react帮我们做了实例化等操作，无需我们自己使用new关键字来实例化
    <App />,
    document.getElementById("root")
);
```

我们只需要写`<App />`，其实react帮我们做了：

```javascript
const app = new App({
    name: 'react'
}).render()

ReactDOM.render(app, document.getElementById('root'))
```

### 注意点

1. 导入进来的组件名需要大写，`App`而非`app`，小写会认为是浏览器自带标签而报异常。
2. 组件只能有一个根节点。
3. 组件的render函数return 可以加一个`()`表示其jsx是一个整体，否则可能因为换行而返回 `undefined`

```javascript
render(){
    return (
        <div>hello</div>
    )
}
```

## 类组件传递参数

使用`this.props`获取组件传递的参数。

```javascript
import React from "react";

class App extends React.Component {
    render() {
        return <div>
            hello {this.props.name}
        </div>
    }
}

export default App;
```

```javascript
import React from "react";
import ReactDOM from "react-dom";

import App from "./components/App";

ReactDOM.render(
    <App name="react" />,
    document.getElementById("root")
);
```
