---
title: 生命周期
date: 2022-06-10
tags:
 - React
categories:
 - React
---

## 生命周期

### 初始化阶段

1. componentWillMount: 初始化数据的作用，将要渲染时触发；render之前最后一次修改状态的机会，能够拿到state, 但拿不到DOM（被废弃）
2. render: 只能访问this.props和this.state，不允许修改状态和DOM输出
3. componentDidMount: 成功渲染完成真实DOM后触发，可以修改DOM

componentDidMount使用场景：

- 数据请求axios
- 订阅函数的调用
- 基于创建完的Dom进行初始化，比如better-scroll

```javascript
import { Component } from "react";

class App extends Component {
    componentWillMount() {
        console.log("componentWillMount"); // ①  （使用时控制台会警告此函数已废弃）
    }
    componentDidMount() {
        console.log("componentDidMount"); // ③
    }
    render() {
        console.log("render"); // ②
        return (
            <div>
                App
            </div>
        )
    }
}

export default App;
```

### 运行中阶段

1. componentWillUpdate(nextProps, nextState): 数据将要更新；不能修改属性和状态（被废弃）
2. render: 数据更新中；只能访问this.props和this.state，不允许修改状态和DOM输出
3. componentDidUpdate: 数据更新完成；可以修改DOM
4. shouldComponentUpdate: 组件是否应该更新；返回false会阻止render调用
5. componentWillReceiveProps: 父组件修改属性触发（被废弃）

componentDidUpdate: 有两个参数(pervProps, prevState)分别获取之前的props和state
shouldComponentUpdate(scu)：性能优化函数，有两个参数(nextProps, nextState)分别获取最新的props和state

```javascript
import { Component } from "react";

class App extends Component {
    componentWillUpdate() {
        console.log("componentWillUpdate"); // ①  （使用时控制台会警告此函数已废弃）
    }
    componentDidUpdate() {
        console.log("componentDidUpdate"); // ③
    }
    state = {
        msg: ""
    }
    render() {
        console.log("render"); // ②
        return (
            <div>
                App
                <button onClick={() => {
                    this.setState({
                        msg: "msg"
                    })
                }}>更新</button>
            </div>
        )
    }
}

export default App;
```

```javascript
// 上述案例我们点击按钮，控制台会一直打印componentWillUpdate、render、componentDidUpdate，即使数据只改变了一次，所以用shouldComponentUpdate优化
import { Component } from "react";

class App extends Component {
    componentWillUpdate() {
        console.log("componentWillUpdate"); // ①  （使用时控制台会警告此函数已废弃）
    }
    componentDidUpdate(pervProps, prevState) {
        console.log("componentDidUpdate--pervProps", pervProps);
        console.log("componentDidUpdate--prevState", prevState);
        console.log("componentDidUpdate"); // ③
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("shouldComponentUpdate--nextProps", nextProps);
        console.log("shouldComponentUpdate--nextState", nextState);
        // this.state => 旧数据； nextState => 新数据
        // return true应该更新; false 阻止更新
        if(JSON.stringify(this.state) !== JSON.stringify(nextState)) {
            return true;
        } else {
            return false;
        }
    }
    state = {
        msg: ""
    }
    render() {
        console.log("render"); // ②
        return (
            <div>
                App
                <button onClick={() => {
                    this.setState({
                        msg: "msg"
                    })
                }}>更新</button>
            </div>
        )
    }
}

export default App;
```

```javascript
// componentWillReceiveProps案例
// componentWillReceiveProps存在问题：父组件触发render更新便会触发该生命周期函数，即使不传属性也会触发
// 作用是最先获得父组件传来的属性，进行相应的处理
import { Component } from "react";

class App extends Component {
    state = {
        msg: ""
    }
    render() {
        console.log("render"); // ②
        return (
            <div>
                App
                <button onClick={() => {
                    this.setState({
                        msg: "msg"
                    })
                }}>更新</button>
                <Child msg={ this.state.msg }/>
            </div>

        )
    }
}

class Child extends Component {
    componentWillReceiveProps() {
        console.log("componentWillReceiveProps");
    }
    render() {
        return(
            <div>Child---{this.props.msg}</div>
        )
    }
}
export default App;
```

### 销毁阶段

1. componentWillUpdate(nextProps, nextState): 在删除组件之前进行清理操作，比如计数器和事件监听器

```javascript
import { Component } from "react";

class App extends Component {
    state = {
        isShow: ""
    }
    render() {
        console.log("render");
        return (
            <div>
                App
                <button onClick={() => {
                    this.setState({
                        isShow: !this.state.isShow
                    })
                }}>更新</button>
                {this.state.isShow && <Child />}
            </div>

        )
    }
}

class Child extends Component {
    componentWillUnmount() {
        console.log("componentWillUnmount");
    }
    render() {
        return(
            <div>Child</div>
        )
    }
}
export default App;
```

## 新生命周期组件

老生命周期的问题：

1. componentWillMount在ssr中这个方法会被多次调用，所以会重复触发多遍，同时在这里如果绑定事件将无法解绑，导致内存泄露，变得不够安全高效呗逐步放弃
2. componentWillReceiveProps外部组件多次频繁更新传入多次不同的props，会导致不必要的异步请求
3. componentWillUpdate更新前记录dom状态，可能会做一些处理，与componentDidUpdate相隔时间如果过长，会导致状态不可信

新生命周期

1. static getDerivedStateFromProps(nextProps, nextState)：第一次的初始化组件以及后续的更新过程中（包含自身状态更新以及父传子）返回一个对象作为新的state，返回null则说明不需要再这里更新state
2. getSnapshotBeforeUpdate(): 取代了componentWillUpdate，触发时间为update发生的时候，在`render之后dom渲染之前`返回一个值，作为componentDidUpdate的第三个参数

> 同含义的新老生命周期不能共存

初始化时的使用：

更新时

> return出来的对象是干嘛的？是用来更新当前组件的state状态

getDerivedStateFromProps 的作用就是为了让 props 能更新到组件内部 state 中。

我们应该谨慎地使用 getDerivedStateFromProps 这个生命周期。使用时要注意下面几点：

- 因为这个生命周期是静态方法，同时要保持它是纯函数，不要产生副作用。
- 在使用此生命周期时，要注意把传入的 prop 值和之前传入的 prop 进行比较（这个 prop 值最好有唯一性，或者使用一个唯一性的 prop 值来专门比较）。
- getDerivedStateFromProps是一个静态函数，也就是这个函数不能通过this访问到class的属性，也并不推荐直接访问属性。而是应该通过参数提供的nextProps以及prevState来进行判断，根据新传入的props来映射到state。

```javascript
// getDerivedStateFromProps在子组件中的应用
import { Component } from "react";

class App extends Component {
    state = {
        type: 1
    }

    render() {
        console.log("render");
        return (
            <div>
                App
                <button onClick={() => {
                    this.setState({
                        type: 2
                    })
                }}>更新{this.state.type}</button>
                <Child type={this.state.type} />
            </div>

        )
    }
}

class Child extends Component {
    state = {
        str: "11"
    }
    static getDerivedStateFromProps(nextProps, nextState) {
        console.log(nextProps)
        const {type} = nextProps;
        console.log("nextState", nextState)
        // 当传入的type发生变化的时候，更新state
        if (type !== nextState.type) {
            return {
                str: type
            };
        }
        // 否则，对于state不进行任何操作
        return null;
    }
    render() {
        return(
            <div>Child {this.state.abc}</div>
        )
    }
}

export default App;
```

```javascript
// getSnapshotBeforeUpdate的应用
import { Component } from "react";

class App extends Component {
    state = {
        msg: ""
    }
    getSnapshotBeforeUpdate() {
        console.log("getSnapshotBeforeUpdate");
        return 100;
    }
    componentDidUpdate(prevProps, prevState, value) {
        console.log("componentDidUpdate");
        console.log(value);
    }
    render() {
        console.log("render");
        return(
            <div>
                App--{this.state.msg}
                <button onClick={() => {this.setState({
                    msg: "react"
                })}}>更新</button>
            </div>
        )
    }
}

export default App;
```

## React中性能优化方案

1. shouldComponentUpdate

控制组件自身或者子组件是否需要更新，尤其在子组件非常多的情况下，需要进行优化。

2. PureComponent

PureComponent会帮你比较新props跟旧的props，新的state和老的state（值相等，或者对象含有相同的属性、且属性值相等），决定shouldComponentUpdate返回true或false，从而决定要不要呼叫render function。

注意：
如果你的state或props一直在变，那么PureComponent并不会比较快，因为shallowEqual也需要花时间。
