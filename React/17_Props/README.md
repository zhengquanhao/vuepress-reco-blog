---
title: 属性Props
date: 2022-06-09
tags:
 - React
categories:
 - React
---

`state状态`只能自己使用，外部无法改变

## 属性的定义和基本使用

`props`是正常是外部传入的，组件内部也可以通过一些方式来初始化的设置，属性不能被组件自己更改，但是你可以通过父组件主动重新渲染的方式来传入新的 `props`

在使用一个组件的时候，可以把参数放在标签的属性当中，所有的属性都会作为组件 props 对象的键值。

> 如果要传变量需要放入`{}`

```javascript
// 父组件
import { Component } from "react";
import NavBar from "./Navbar";

class App extends Component {
    render() {
        return (
            <div>
                首页：
                <NavBar title="首页" isShow={false}></NavBar>
                购物车页：
                <NavBar title="购物车" isShow={true}></NavBar>
                个人信息页：
                <NavBar title="我的"></NavBar>
            </div>
        )
    }
}

export default App;
```

```javascript
// 子组件
import { Component } from "react";

class NavBar extends Component {
    render() {
        let { title, isShow } = this.props;
        return(
            <div>
                {isShow && <i>返回</i>}
                <div>{title}</div>
            </div>
        )
    }
}

export default NavBar;
```

## 属性验证

我们需要对属性的类型进行校验；我们需要引入`prop-types`并使用类属性`propTypes`

```javascript
import { Component } from "react";
import propTypes from "prop-types";

class NavBar extends Component {
    render() {
        let { title, isShow } = this.props;
        return(
            <div>
                {isShow && <i>返回</i>}
                <div>{title}</div>
            </div>
        )
    }
}

// 类属性
NavBar.propTypes = {
    title: propTypes.string,
    isShow: propTypes.bool
}

export default NavBar;
```

类属性也可以直接写在类里，前面加上`static`关键字

```javascript
import { Component } from "react";
import propTypes from "prop-types";

class NavBar extends Component {
     // 属性校验
    static propTypes = {
        title: propTypes.string,
        isShow: propTypes.bool
    }
    render() {
        let { title, isShow } = this.props;
        return(
            <div>
                {isShow && <i>返回</i>}
                <div>{title}</div>
            </div>
        )
    }
}

export default NavBar;
```

## 默认属性

我们可以给props设置设置默认属性，方式和属性验证相同，需要给类添加`defaultProps`类属性。

```javascript
import { Component } from "react";
import propTypes from "prop-types";

class NavBar extends Component {
    // 默认属性
    static defaultProps = {
        title: "",
        isShow: true
    }
    render() {
        let { title, isShow } = this.props;
        return(
            <div>
                {isShow && <i>返回</i>}
                <div>{title}</div>
            </div>
        )
    }
}

export default NavBar;
```

## 父组件传值简写

父组件传值如果属性名和属性值一致的话可简写

```javascript
import { Component } from "react";
import NavBar from "./Navbar";

class App extends Component {
    render() {
        let obj = {
            title: "我的",
            isShow: false
        }
        return (
            <div>
                个人信息页：
                <NavBar {...obj}></NavBar>
            </div>
        )
    }
}

export default App;
```

## 函数式组件属性

```javascript
import propTypes from "prop-types";

// 通过形参props获取父组件的传值
function NavBar(props) {
    console.log("props", props); // {title: '首页', isShow: false}
    let {isShow, title} = props; // 直接结构出来
    return (<div>
        {isShow && <i>返回</i>}
        <div>{title}</div>
    </div>)
}

// 属性验证和默认属性无法像类组件在类里写static，只能在外层定义类属性

// 属性验证
NavBar.propsType = {
    title: propTypes.string,
    isShow: propTypes.bool
}
// 默认属性
NavBar.defaultProps = {
    title: "",
    isShow: true
}

export default NavBar;
```
