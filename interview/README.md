# 面试题整理

## HTTP相关

1. [GET和POST请求方法的区别](/Interview/HTTP/01_GetPost/)
2. [POST和PUT请求方法的区别](/Interview/HTTP/02_PostPut/)
3. [常见的http请求头和响应头](/Interview/HTTP/03_ReqResHeader/)
4. [常见的http请求方法](/Interview/HTTP/04_Methods/)
5. [http和https的区别](/Interview/HTTP/05_HttpVsHttps/)
6. [HTTP1.0 和 HTTP1.1 的一些区别](/Interview/HTTP/06_Http1.0VsHttp1.1/)
7. [HTTP2 特性 / HTTP2 对比HTTP1.*的提升](/Interview/HTTP/07_Http2Characteristic/)
8. [从输入一个 `URL` 地址到浏览器完成渲染的整个过程](/Interview/HTTP/HTTP/08_BrowserInputURL/)
9. [Get方法Url长度限制的原因？](/Interview/HTTP/HTTP/09_GetUrlLength/)
10. [为什么说HTTPS比HTTP安全? HTTPS是如何保证安全的？](/Interview/HTTP/HTTP/10_Https/)
11. [什么是CDN？原理是什么？](/Interview/HTTP/HTTP/11_CDN/)
12. [DNS协议 是什么？说说DNS 完整的查询过程?](/Interview/HTTP/HTTP/12_DNS/)

## 手写题目

[代码手写题目汇总](/Interview/HandWrite/)