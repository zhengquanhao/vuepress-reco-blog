---
title: 解决跨域的方式
date: 2021-01-11
tags:
 - Interview
---

## 一、使用jsonp解决跨域

### 原理

1. `<script>` 标签的src属性不受同源策略的限制。

2. 跨域请求的接口返回的是一个函数调用，也就是调用我们应用中定义好的一个方法。

```javascript
let script = document.createElement("script");
script.src = "https://www.baidu.com/sugrec?prod=pc&wd=hello&cb=show";
document.body.appendChild(script);

// src中的cb=`show`和回调函数名保持一致
function show(data) {
    // 跨域请求接口的回调函数
    console.log("data", data);
}
```

### 缺点

不安全，容易造成xss攻击；只能是GET请求。

## 二、通过cors解决跨域

### 原理

1. 在服务器端设置允许跨域的请求头，从而实现跨域。

2. 服务器设置后前端通过正常的 `ajax` 请求即可。

```java
// 允许哪些源可以访问
response.setHeader("Access-Control-Allow-Origin", "*");
```

## 三、通过websocket 与服务器通信

原理： `websocket` 可以实现与服务器间的双向通信

![websocket](./images/websocket.png)

## 四、nginx代理跨域

同源策略是浏览器的安全策略，不是HTTP协议的一部分。服务器端调用HTTP接口只是使用HTTP协议，不会执行JS脚本，不需要同源策略，也就不存在跨越问题。

```shell
#proxy服务器
server {
    listen       81;
    server_name  www.domain1.com;

    location / {
        proxy_pass   http://www.domain2.com:8080;  #反向代理
        proxy_cookie_domain www.domain2.com www.domain1.com; #修改cookie里域名
        index  index.html index.htm;

        # 当用webpack-dev-server等中间件代理接口访问nignx时，此时无浏览器参与，故没有同源限制，下面的跨域配置可不启用
        add_header Access-Control-Allow-Origin http://www.domain1.com;  #当前端只跨域不带cookie时，可为*
        add_header Access-Control-Allow-Credentials true;
    }
}
```

## 五、vue proxy

```javascript
proxy: {
    '/api': {
    target: '替换代理接口域名',
    ws: true,
    secure: false,
    changeOrigin: true,
    pathRewrite: {
        '^/api': '/'
    }
    },
}
```

## 六、通过postMessage()实现不同源的两个页面间的通信

### 原理

1. `postMessage()` 能在两个页面间通信。

2. 利用iframe。

![post-message](./images/post-message.png)

## 七、通过window.name 实现跨域

原理：name在不同页面加载后仍旧存在，配合iframe

![window-name](./images/window-name.png)

## 八、通过路径的hash来实现页面间的跨域（通信）

原理：同源间的hash可以相互复制

![hash](./images/hash.png)

## 九、通过window.domain

限制：只能用于二级域名相同的页面

![window-domain](./images/window-domain.png)