---
title: GET和POST请求方法的区别
date: 2022-05-29
tags:
 - Interview
---

1. get一般用来请求数据，post一般发送数据到后台时使用。
2. get请求传参到后台，参数会在url中可见，隐私安全性较差。post请求传递的参数放在request body中，比get安全。
3. get请求刷新或回退浏览器没有影响，post请求回退时会重新提交数据请求。
4. get请求会被浏览器主动cache，而post不会，除非手动设置。
5. get请求会保留在浏览器历史记录中，post不会保存。
6. get请求可以被收藏为书签，post不能收藏为书签。
7. get请求只能进行url编码（application/x-www-form-urlencoded）post支持多种编码方式（application/x-www-form-urlencoded，multipart/form-data，...）
8. get请求通常的方式是通过url地址栏请求，post通常通过表单发送数据请求。

深入理解：

1. post请求和get请求都是http的请求方式，底层都是tcp/ip协议，get请求产生一个tcp数据包，post产生两个数据包（但firefox只发送一次）
2. get请求把header和data一起发送出去，服务器响应200；post请求时先发送header,等服务器响应100再发送data，再响应200。
