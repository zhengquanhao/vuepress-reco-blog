---
title: HTTP1.0 和 HTTP1.1 的一些区别
date: 2022-05-31
tags:
 - Interview
---

## 一、断点续传

HTTP1.0 中，存在一些浪费带宽的现象，例如客户端只是需要某个对象的一部分，而服务器却将整个对象送过来了，并且不支持断点续传功能。

HTTP 1.1默认支持断点续传。

## 二、长连接

HTTP1.0 需要使用 `keep-alive` 参数来告知服务器端要建立一个长连接，而 HTTP1.1 默认支持长连接，一定程度上弥补了 HTTP1.0 每次请求都要创建连接的缺点。

HTTP 是基于 TCP/IP 协议的，创建一个 TCP 连接是需要经过三次握手的,有一定的开销，如果每次通讯都要重新建立连接的话，对性能有影响。因此最好能维持一个长连接，可以用个长连接来发多个请求。

## 三、引入新的缓存控制策略

在 HTTP1.0 中主要使用 header 里的 `If-Modified-Since`（比较资源最后的更新时间是否一致）,`Expires`（资源的过期时间（取决于客户端本地时间）） 来做为缓存判断的标准。

HTTP1.1 则引入了更多的缓存控制策略：

- Entity tag：资源的匹配信息
- If-Unmodified-Since：比较资源最后的更新时间是否不一致
- If-Match：比较 ETag 是否一致
- If-None-Match：比较 ETag 是否不一致

## 四、增加Host 头处理

在 HTTP1.0 中认为每台服务器都绑定一个唯一的 IP 地址，因此，请求消息中的 URL 并没有传递主机名（hostname）。但随着虚拟主机技术的发展，在一台物理服务器上可以存在多个虚拟主机，并且它们共享一个 IP 地址。HTTP1.1 的请求消息和响应消息都应支持 Host 头域，且请求消息中如果没有 Host 头域会报告一个错误（400 Bad Request）。

## 五、新增请求方式

PUT：请求服务器存储一个资源
DELETE：请求服务器删除标识的资源
OPTIONS：请求查询服务器的性能，或者查询与资源相关的选项和需求
CONNECT：保留请求以供将来使用
TRACE：请求服务器回送收到的请求信息，主要用于测试或诊断

## 六、新增状态码

在 HTTP1.1 中新增了 24 个错误状态响应码，如 409（Conflict）表示请求的资源与资源的当前状态发生冲突；410（Gone）表示服务器上的某个资源被永久性的删除。
