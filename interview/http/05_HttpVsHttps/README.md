---
title: http和https的区别
date: 2022-06-05
tags:
 - Interview
---

1. http是明文传输，https加密传输
2. 连接方式不同，使用的端口也不一样，http是80，https是443
3. http是无状态的连接（无法根据之前的状态进行本次的请求处理）https是由ssl+http构成，是可加密的，并且要进行身份认证
4. https是需要申请证书的，证书需要费用
