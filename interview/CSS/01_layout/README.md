---
title: 实际开发中常用的几种CSS布局
date: 2022-01-10
tags:
 - Interview
---

## 一、两列布局-左固定右自适应

实现两列布局左边宽度(.left)固定，右边宽度(.right)自适应。

```html
<div class="container">
    <div class="left"></div>
    <div class="right"></div>
</div>
```

### 1.1 flex 方式

```css
.container {
    display: flex;
    height: 100px;
}
.container .left {
    width: 100px;
    background-color: pink;
}
.container .right {
    flex: 1; /* flex 方式 */
    background-color: skyblue;
}
```

### 2.2 calc 方式

```css
.container {
    display: flex;
    height: 100px;
}
.container .left {
    width: 100px;
    background-color: pink;
}
.container .right {
    width: calc(100% - 100px); /* calc 方式 */
    background-color: skyblue;
}
```

## 二、多列布局

将6个div块展示成为2行3列且宽度自适应。

```html
<div class="container">
    <div class="one item">One</div>
    <div class="two item">Two</div>
    <div class="three item">Three</div>
    <div class="four item">Four</div>
    <div class="five item">Five</div>
    <div class="six item">Six</div>
</div>
```

```css
.item {
    height: 100px;
    text-align: center;
    font-size: 200%;
    color: #fff;
}

.one {
    background: #19CAAD;
}

.two {
    background: #8CC7B5;
}

.three {
    background: #D1BA74;
}

.four {
    background: #BEE7E9;
}

.five {
    background: #E6CEAC;
}

.six {
    background: #ECAD9E;
}
```

### 2.1 column-count + gap

```css
.container {
    column-count: 3;
    gap: 10px; /*列与列之间的间距*/
}
```

展示效果：

![column-count + gap](./images/column-count+gap.png)

### 2.2 display-grid + gap

```css
.container {
    display: grid;
    gap: 10px 10px;
    grid-template: repeat(2, 1fr) / repeat(3, 1fr); /* 2行3列，1fr => 1份 */
}
```

展示效果：

![display-grid+gap](./images/display-grid+gap.png)

#### repeat()函数作用

先看下面代码：

```css
.container {
    grid-template-columns: 40px auto 60px;
}
```

表示我们的布局分为三列，每列的宽度分别是40px，auto，以及60px。

现在，假设我们的布局是12列的，请问，CSS代码会是怎样的？

如果按照上面写法，是不是应该写成下面这样？

```css
.container {
    grid-template-columns: 40px auto 60px 40px auto 60px 40px auto 60px 40px auto 60px;
}
```

代码量多，看得眼花，还不好理解，维护起来也不方便。

此时，repeat()的价值就体现了，什么，12列？小菜一碟。请看表演：

```css
.container {
    grid-template-columns: repeat(4, 40px auto 60px);
}
```

非常好理解，重复4次，每次的重复单元是40px auto 60px这3个尺寸。

### 2.3 flex

```css
.container {
    display: flex;
    flex-wrap: wrap;
}

.item {
    width: 33.33%;
}
```

展示效果：

![multiple-columns-flex](./images/multiple-columns-flex.png)

## 三、水平居中

将 `.box` 在 `.container` 内水平居中。

```html
<div class="container">
    <div class="box"></div>
</div>
```

```css
.box {
    background-color: pink;
    width: 100px;
    height: 100px;
}
```

### 3.1 设置 margin

```css
.box {
    margin: 0 auto;
}
```

### 3.2 position + margin

使用定位加margin计算。

```css
.container {
    position: relative;
}

.box {
    position: absolute;
    left: 50%;
    margin-left: -50px;
    /* 或 transform: translateX(-50%); */
}
```

### 3.3 flex + justify-content

```css
.container {
    display: flex;
    justify-content: center;
}
```

## 四、垂直居中

### 4.1 position + margin

使用定位加margin计算。

```css
.container {
    position: relative;
}

.box {
    position: absolute;
    top: 50%;
    margin-top: -50px;
    /* 或 transform: translateY(-50%); */
}
```

### 4.2 flex + align-items

```css
.container {
    display: flex;
    align-items: center;
}
```
