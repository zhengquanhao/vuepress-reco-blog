---
title: 全站置灰
date: 2022-01-10
tags:
 - Interview
---

借助 `filter（滤镜）` 给网站添加滤镜：

```css
body{
    filter: grayscale(1);
}
```
