---
title: css 的渲染层合成是什么 浏览器如何创建新的渲染层
date: 2022-01-12
tags:
 - Interview
---

## 渲染层合成

在 DOM 树中每个节点都会对应一个渲染对象（RenderObject），当它们的渲染对象处于相同的坐标空间（z 轴空间）时，就会形成一个 RenderLayers，也就是渲染层。渲染层将保证页面元素以正确的顺序堆叠，这时候就会出现层合成（composite），从而正确处理透明元素和重叠元素的显示。对于有位置重叠的元素的页面，这个过程尤其重要，因为一旦图层的合并顺序出错，将会导致元素显示异常。

## 浏览器如何创建新的渲染层

能够导致浏览器为其创建新的渲染层的，包括以下几类常见的情况：

- 根元素 document

- 有明确的定位属性（relative、fixed、sticky、absolute）

- opacity < 1

- 有 CSS filter 属性

- 有 CSS mask 属性

- 有 CSS mix-blend-mode 属性且值不为 normal

- 有 CSS transform 属性且值不为 none

- backface-visibility 属性为 hidden

- 有 CSS reflection 属性

- 有 CSS column-count 属性且值不为 auto或者有 CSS column-width 属性且值不为 auto

- 当前有对于 opacity、transform、filter、backdrop-filter 应用动画

- overflow 不为 visible
