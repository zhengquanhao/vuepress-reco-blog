---
title: CSS实现形状
date: 2022-01-10
tags:
 - Interview
---

## 实现一个正三角形

```html
<div class="container">
    <div class="triangle"></div>
</div>
```

```css
.container {
    width: 20px;
    height: 20px;
}
.triangle {
    border-width: 10px;
    border-style: solid;
    border-color: transparent transparent pink transparent;
}
```

## 实现爱心

利用border和transform旋转

```html
<div class="container">
    <div class="left"></div>
    <div class="right"></div>
</div>
```

```css
.container {
    width: 200px;
    height: 200px;
}

.left {
    display: inline-block;
    width: 30px;
    height: 20px;
    background-color: #f56c84;
    border-start-start-radius: 10px;
    border-end-start-radius: 10px;
    transform: rotate(45deg);
    margin-right: -27px;
}

.right {
    display: inline-block;
    width: 30px;
    height: 20px;
    background-color: #f56c84;
    border-end-end-radius: 10px;
    border-start-end-radius: 10px;
    transform: rotate(-45deg);
}
```
