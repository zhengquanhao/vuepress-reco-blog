---
title: 多维数组拍平
date: 2021-12-22
tags:
 - Interview
---

## 思路

将多维数组拍平的三种方式。

- 利用 `ES2019` 新特性 `flat`函数实现。
- 使用 `reduce` 函数递归实现。
- 使用数组基本方法实现。

## 实现

具体代码:

```javascript
let arr = [
    1,
    [ 2, 3, 4 ],
    [ 5, [ 6, [ 7, [ 8 ] ] ] ]
];
/**
 *
 * @param {*} array 深层嵌套的数据
 * @returns array 新数组
*/

const flat1 = (array) => {
    return array.flat(Infinity);
}

const flat2 = (array) => {
    return array.reduce((preRes, cur) => {
        return preRes.concat(Array.isArray(cur) ? flat2(cur) : cur);
    }, [])
}

const flat3 = (array) => {
    const result = [];
    const stack = [...array];
    while (stack.length !== 0) {
        const val = stack.pop(); // 利用 pop 方法会改变元素组的特性（删除原数组最后一个元素并返回）
        if (Array.isArray(val)) {
            stack.push(...val);
        } else {
            result.unshift(val);
        }
    }
    return result;
}

console.log(flat1(arr)); // [1, 2, 3, 4, 5, 6, 7, 8]
console.log(flat2(arr)); // [1, 2, 3, 4, 5, 6, 7, 8]
console.log(flat3(arr)); // [1, 2, 3, 4, 5, 6, 7, 8]
```