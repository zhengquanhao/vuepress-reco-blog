---
title: javascript 柯里化curry
date: 2022-01-14
tags:
 - Interview
---

## 柯里化定义

柯里化（Currying）是把接受多个参数的函数变换成接受一个单一参数(最初函数的第一个参数)的函数，并且返回接受余下的参数且返回结果的新函数的技术。大多数的blog都是这种说法，说实话我是懵逼的。
我的理解是，**curry是一个收集参数的方法，收集够了去执行函数。**

## 实现柯里化

实现前我们先列一下要点:

1. 收集参数（就像面试题多次执行多个参数）是的利用闭包。

2. 每次执行参数有多有少，例如add(1)(2,3)(4)。

3. 我们需要知道什么时候参数够了。

```javascript
const curry = (func, ...args) => { // 将上一次的innerArgs存在args中
    const fnLen = func.length // function length 属性指明函数的形参个数 (3) 。
    // 做一个闭包
    return function (...innerArgs) { // innerArgs为每次的参数（1，2，3） /（1 2，3）/ （1 2 3）/ （1， 2 3） /
        // 每执行一次收集一次参数,为什么用concat是因为有时候后是多个参数(2,3)
        innerArgs = args.concat(innerArgs)

        if (innerArgs.length < fnLen) {
            // 参数没有收集完我们需要继续收集，递归
            return curry(func, ...innerArgs)
        } else {
            // 直到参数收集完成执行func
            func(...innerArgs)
        }
    }
}


// 测试一下
const add = curry((num1, num2, num3) => {
    console.log(num1, num2, num3, num1 + num2 + num3)
})

add(1)(2)(3); // 1 2 3 6
add(1, 2)(3); // 1 2 3 6
add(1, 2, 3); // 1 2 3 6
add(1)(2, 3); // 1 2 3 6
```
