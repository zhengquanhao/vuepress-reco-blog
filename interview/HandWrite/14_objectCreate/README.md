---
title: Object.create 实现原理
date: 2022-01-11
tags:
 - Interview
---

## Object.create方法说明

1. `Object.create()` 方法创建一个新的对象，并以方法的第一个参数作为新对象的 `__proto__` 属性的值（以第一个参数作为新对象的构造函数的原型对象）。

2. `Object.create()` 方法还有第二个可选参数，是一个对象，对象的每个属性都会作为新对象的自身属性，对象的属性值以`descriptor（Object.getOwnPropertyDescriptor(obj, 'key')）`的形式出现，且enumerable默认为false。

```javascript
console.log(Object.create({})); // {}
console.log(Object.create({name: "Ron"})); // {}, 构造函数的原型对象是{name: "Ron"}
console.log(Object.create({}, {age: {value: 18}})); // {age: 18}
console.log(Object.create({name: "Ron"}, {age: {value: 18}})); // {age: 18}, 构造函数的原型对象是{name: "Ron"}
```

## 实现 Object.create

```javascript
Object.myCreate = function (proto, propertyObject = undefined) {
    if (propertyObject === null) {
        throw "TypeError";
    } else {
        // 创建构造函数
        function Fn () {};
        // 赋值原型
        Fn.prototype = proto;
        // 创建实例
        const obj = new Fn();
        // 支持第二个参数
        if (propertyObject) {
            Object.defineProperties(obj, propertyObject);
        }
        // 支持空原型
        if (proto === null) {
            obj.__proto__ = null;  // Fn.prototype === obj.__proto__ => true;
        }
        return obj;
    }
}

const obj1 = Object.myCreate({name: "Ron"});
console.log(obj1); // {}, obj1的构造函数的原型对象是{name: "Ron"}

const obj2 = Object.myCreate({name: "Ron"}, {
age: {
    value: 18,
    enumerable: true
}
})
console.log(obj2); // {age: 18}, obj2的构造函数的原型对象是{name: "Ron"}
```
