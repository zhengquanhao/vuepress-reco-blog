---
title: 防抖与节流的实现
date: 2022-01-13
tags:
 - Interview
---

## 防抖与节流的定义

多次触发事件，但是只在触发停止后一段时间内才触发一次，叫做 `防抖 (debounce)`；

每隔一段时间触发一次叫 `节流 (throttle)`。

举例：

1. 在搜索框中输入，在输入停止200ms后执行搜索叫防抖；

2. 在输入过程中每隔2秒执行一次搜索叫做节流。

## 实现防抖

```javascript
function debounce (fun, delay = 1000) {
    let timer = null;
    return function() {
        let self = this;
        const args = [...arguments];
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            fun.call(self, args);
        }, delay);
    }
}

// window.addEventListener("resize", () => {console.log("resize")});
window.addEventListener("resize", debounce(() => {console.log("resize")}));
```

## 实现节流

```javascript
//节流
function throttle(fun, delay = 1000){
    let pre = new Date().getTime();
    return function() {
        const args = [...arguments];
        const cur = new Date().getTime();
        if(cur - pre >= delay){
            pre = cur;
            fun.apply(this, args);
        }
    }
}

window.onresize = throttle(() => console.log("resize"), 2000);
```

## 总结

防抖和节流的实现大致相同,都是将原来的同步调用改为异步调用，不同点在于防抖很严格,只要有新的事件刷新,就会抛弃之前的异步执行任务,重新生成新的任务，而节流相比防抖要稍微温和一点, 在delay时间内的多次事件触发,都能保证在delay时间后能执行一次回调。
