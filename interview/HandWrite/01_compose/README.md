---
title: 手写compose函数
date: 2021-12-20
tags:
 - Interview
---

## 介绍

compose组合函数作用：把多层函数嵌套调用扁平化。

> 函数调用扁平化： 如果是多层级嵌套调用的函数， 把一个函数调用完，当做另一个函数的实参算低到下一个函数中。

- 常基于reduce， 柯里化函数思想解决函数调用扁平化的问题。
- 解决函数嵌套问题，比如B函数依赖A函数的返回值， C函数依赖B函数的返回值

举例：

```javascript
const fn1 = x => x+10;
const fn2 = x => x*10;
const fn3 = x => x/10;
console.log(fn3(fn2(fn1())));
```

## 必备知识点

`reduce`函数：

```javascript
array.reduce(function(total, currentValue, currentIndex, arr), initialValue)
```

函数参数说明:

- total:必需, 初始值, 或者计算结束后的返回值。
- currentValue: 必需, 当前元素。
- currentIndex: 可选, 当前元素的索引。
- arr: 可选, 当前元素所属的数组对象。
- initialValue: 可选, 传递给函数的初始值。


## 实现 `compose` 函数：

```javascript
const fn1 = x => x + 10;
const fn2 = x => x * 10;
const fn3 = x => x / 10;

function compose(...fns){
    return (...args) => {
        if(fns.length === 0) {
            return args;
        }
        if(fns.length === 1) {
            return fns[0](...args);
        }
        return fns.reduce((pre, cur)=>{
            //第一次执行：pre表示第一个函数执行的实参， cur表示第一个函数
            //第二次执行：pre表示上一次cur的执行返回值， 作为实参传递给下一个函数执行，cur为下一个函数
            console.log("pre", pre);
            console.log("cur", cur);
            return Array.isArray(pre) ? cur(...pre) : cur(pre);
        }, args);
    }
}

//执行过程
console.log(compose()(5)) // =>5;
console.log(compose(fn1)(5)); // =>10+5;
console.log(compose(fn1, fn2)(5))// =>f1(5)=15 fn2(15)=150;
console.log(compose(fn1,fn2,fn3)(5));// =>f1(5)=15 fn2(15)=150 fn3(150)=>15;
```
