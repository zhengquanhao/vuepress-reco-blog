---
title: 利用正则表达式实现trim
date: 2022-01-04
tags:
 - Interview
---

```javascript
String.prototype.trim1 = function () {
    return this.replace(/^\s+|\s+$/g, "");
};

String.prototype.trim2 = function () {
    return this.replace(/^\s+(.*?)\s+$/, "$1"); // 疑问号让.*的搜索模式从贪婪模式变成惰性模式
};

let str = '     hello    ';

console.log(str.trim1() + "world");
console.log(str.trim2() + "world");
```
