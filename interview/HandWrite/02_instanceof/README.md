---
title: 手写instanceof
date: 2021-12-21
tags:
 - Interview
---

## instanceof

### 有了typeof为什么还需要instanceof

`typeof` 可以帮助我们判断数据类型，但是在判断 `object` 类型的时候 `typeof` 只能告诉我们是 `object` 但是不能告诉我们具体属于哪类`object`。

请看下面的代码，`instanceof`的出现可以解决这个问题:

```javascript
const a = new Date();
const b = [1, 2, 2];

console.log(typeof a); // object
console.log(typeof b); // object
```

### instanceof的实现原理

`instanceof` 运算符用于检测构造函数的 `prototype` 属性是否出现在某个实例对象的原型链上。

实现 `instanceof` 的三步走战略，请大家区分好什么是原型什么是原型对象，`proto` 指的是原型，`prototype` 指的是原型对象。

并需要注意：

```javascript
// Object.prototype.__proto__ === null
// Object.prototype是一个空对象
```

#### 第一步：获取左边表达式的原型

```javascript
let leftProto = leftVal.__proto__;
或
let leftProto = Object.getPrototypeOf(leftVal);
```

#### 第二步：获取右边表达式的原型对象

```javascript
let rightProtoType = rightVal.prototype;
```

#### 第三步：循环判断左边表达式的原型链上是否有右边的表达式

```javascript
while (true) {
    if (leftProto === null) {
        return false;
    } else if (leftProto === rightProtoType) {
        return true;
    }
    leftProto = leftProto.__proto__;
}
// 或使用递归
if (leftProto === null) {
    return false;
} else if (leftProto === rightProtoType) {
    return true;
} else {
    return my_instanceof(leftProto, rightVal);
}
```

### 实现instanceof整体代码

```javascript
const my_instanceof = (leftVal, rightVal) => {
    if (leftVal === null ||  !["object", "function"].includes(typeof leftVal)) {
        return false;
    }
    // 取左表达式的__proto__值
    let leftProto = leftVal.__proto__;
    // 取右表达式的 prototype 值
    let rightProtoType = rightVal.prototype;
    while (true) {
        if (leftProto === null) {
            return false;
        } else if (leftProto === rightProtoType) {
            return true;
        }
        // 往左表达式的原型链上一层继续查找
        leftProto = leftProto.__proto__;
    }
}

let Fn = function() {};
let p1 = new Fn();

console.log(my_instanceof(Fn, p1)); // false
console.log(my_instanceof(p1, Fn)); // true
console.log(my_instanceof(p1, Object)); // true
console.log(my_instanceof(Fn, Object)); // true
console.log(my_instanceof(123, Number)); // false
console.log(my_instanceof("123", String)); // false
```
