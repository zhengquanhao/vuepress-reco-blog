---
title: 判断数据类型
date: 2022-01-12
tags:
 - Interview
---

```javascript
const getType = (s) => {
    const r = Object.prototype.toString.call(s);

    return r.replace(/\[object (.*?)\]/, "$1").toLowerCase();
}

console.log(getType()); // undefined
console.log(getType(null)); // null
console.log(getType(1)); // number
console.log(getType("Ron")); // string
console.log(getType(true)); // boolean
console.log(getType(Symbol("Ron"))); // symbol
console.log(getType({})); // object
console.log(getType([])); // array
```
