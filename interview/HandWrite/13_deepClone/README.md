---
title: 手写深拷贝
date: 2022-01-07
tags:
 - Interview
---

## 浅拷贝与深拷贝

### 浅拷贝

```javascript
var m = { a: 10, b: 20 }
var n = m;
n.a = 15; // 这时m.a的值是多少
```

m.a会输出15，因为这是浅拷贝，n和m指向的是同一个堆，对象复制只是复制的对象的引用。

### 深拷贝

```javascript
var m = { a: 10, b: 20 }
var n = {a:m.a,b:m.b};
n.a = 15;
```

深拷贝和上面浅拷贝不同，就是彻底copy一个对象，而不是copy对象的引用。
这次，我们再来输出m.a ，发现m.a的值还是10,并没有改变，m对象和n对象是虽然所有的值都是一样的，但是在堆里面，对应的不是同一个了，这个就是深拷贝。

## 实现深拷贝

```javascript
const getType = o => Object.prototype.toString.call(o).slice(8, -1);

function deepClone (obj) {
    let result;
    if (getType(obj) === "Object") {
        result = {};
        for (const key in obj) {
            if (getType(obj[key]) === "Object" || getType(obj[key]) === "Array") {
                result[key] = this.deepClone(obj[key]);
            } else {
                result[key] = obj[key];
            }
        }
    } else if (getType(obj) === "Array") {
        result = [];
        for (const key of obj) {
            result.push(this.deepClone(key));
        }
    } else {
        return obj;
    }
    return result;
}

var obj1 = {
    a: 1,
    b: { f: { g: 1 } },
    c: [1, 2, 3],
    d: function() {
        alert("hello world");
    }
};

var obj2 = deepClone(obj1);

console.log(obj2);
```
