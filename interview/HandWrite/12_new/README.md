---
title: 手写new
date: 2022-01-07
tags:
 - Interview
---

## new 的作用

我们先来通过两个例子来了解 `new` 的作用。

```javascript
function Person (name, age) {
    this.name = name;
    this.age = age;
}

Person.prototype.sayName = function() {
    console.log(this.name);
}

const p1 = new Person("Ron", 18);

console.log(p1);

p1.sayName();
```

从上面一个例子中我们可以得出这些结论：

- **new 通过构造函数 `Person` 创建出来的实例可以访问到构造函数中的属性。**
- **new 通过构造函数 `Person` 创建出来的实例可以访问到构造函数原型链中的属性，也就是说通过 `new` 操作符，实例与构造函数通过原型链连接了起来。**

但是当下的构造函数 `Person` 并没有显式 `return` 任何值（默认返回 `undefined`），如果我们让它返回值会发生什么事情呢？

```javascript
function Person (name, age) {
    this.name = name;
    this.age = age;
    return 1;
}

const p1 = new Person("Ron", 18);

console.log(p1);
```

虽然上述例子中的构造函数中返回了 1，但是这个返回值并没有任何的用处，得到的结果还是和之前的例子完全一样。

那么通过这个例子，我们又可以得出一个结论：

- **构造函数如果返回原始值（虽然例子中只有返回了 1，但是你可以试试其他的原始值，结果还是一样的），那么这个返回值毫无意义。**

试完了返回原始值，我们再来试试返回对象会发生什么事情吧:

```javascript
function Person (name, age) {
    this.name = name;
    this.age = age;
    console.log(this); // Person {name: 'Ron', age: 18}
    return {age};
}

Person.prototype.sayName = function() {
    console.log(this.name);
}

const p1 = new Person("Ron", 18);

console.log(p1); // {age: 18}

console.log(p1.name); // undefined

p1.sayName(); // Uncaught TypeError: p1.sayName is not a function
```

通过这个例子我们可以发现，虽然构造函数内部的 `this` 还是依旧正常工作的，但是当返回值为对象时，这个返回值就会被正常的返回出去。

那么通过这个例子，我们再次得出了一个结论：

- **构造函数如果返回值为对象，那么这个返回值会被正常使用**

> 这两个例子告诉了我们一点，构造函数尽量不要返回值。因为返回原始值不会生效，返回对象会导致 `new` 操作符没有作用。

通过以上几个例子，相信大家也大致了解了 new 操作符的作用了，接下来我们就来尝试自己实现 `new` 操作符。

## 自己实现 new 操作符

首先我们再来回顾下 new 操作符的几个作用:

- new 操作符会返回一个对象，所以我们需要在内部创建一个对象。
- 这个对象，也就是构造函数中的 this，可以访问到挂载在 this 上的任意属性。
- 这个对象可以访问到构造函数原型上的属性，所以需要将对象与构造函数链接起来。
- 返回原始值需要忽略，返回对象需要正常处理。

```javascript
function _new(Con, ...args) {
    let obj = {};
    Object.setPrototypeOf(obj, Con.prototype); // 设置obj的原型为Con.prototype
    let result = Con.apply(obj, args);
    return result instanceof Object ? result : obj;
}
```

1. 首先函数接受不定量的参数，第一个参数为构造函数，接下来的参数被构造函数使用。
2. 然后内部创建一个空对象 `obj`。
3. 因为 `obj` 对象需要访问到构造函数原型链上的属性，所以我们通过 setPrototypeOf 将两者联系起来。这段代码等同于 `obj.__proto__ = Con.prototype`。
4. 将 `obj` 绑定到构造函数上，并且传入剩余的参数。
5. 判断构造函数返回值是否为对象，如果为对象就使用构造函数返回的值，否则使用 `obj`，这样就实现了忽略构造函数返回的原始值。

接下来整体代码来使用下该函数，看看行为是否和 `new` 操作符一致:

```javascript
/**
* new 的执行过程
* 1. 创建一个对象obj
* 2. 该对象的__proto__指向构造函数Fn的原型prototype
* 3. 执行构造函数Fn的代码，往新创建的对象obj上添加成员属性和方法
* 4. 返回这个新的对象obj
*/

function _new(Con, ...args) {
    if (typeof Con !== "function") {
        throw "func must be a function";
    }
    let obj = {};
    Object.setPrototypeOf(obj, Con.prototype); // 设置obj的原型为Con.prototype
    let result = Con.apply(obj, args);
    return result instanceof Object ? result : obj;
}


let Person = function (name, age) {
    this.name = name;
    this.age = age;
}

Person.prototype.showInfo = function () {
    console.log(this.name, this.age)
}

let p1 = _new(Person, "Ron", 18)

console.log(p1); // Person {name: 'Ron', age: 18}

p1.showInfo(); // Ron 18
```
