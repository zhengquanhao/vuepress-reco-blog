---
title: 手写bind函数
date: 2021-12-29
tags:
 - Interview
---

## bind函数的定义与使用

### 定义

`bind()` 方法创建一个新的函数，在 `bind()` 被调用时，这个新函数的 `this` 被指定为 `bind()` 的第一个参数，而其余参数将作为新函数的参数，供调用时使用。

[MDN对 `Function.prototype.bind()` 的定义](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Function/bind)

### 使用

```javascript
function fun(arg1, arg2) {
  console.log(this.name)
  console.log(arg1 + arg2)
};

const _this = { name: "Ron" };

// 只变更fun中的this指向，返回新function对象
const newFun = fun.bind(_this);

newFun(1, 2);

// 或以下运用方式 效果相同
// const newFun = fun.bind(_this, 1, 2);
// newFun();
```

当 `bind` 函数传入其余参数时，在最终运算时将以首次传入的其余参数值为准。

```javascript
function fun (arg1, arg2) {
    console.log(this.name);
    console.log(arg1 + arg2);
}
const _this = { name: "Ron" };
const newFun = fun.bind(_this, 4, 5);
newFun(1, 2);

// 输出结果：Ron   9
```

## 手写实现

### 简单实现

```javascript
/**
 * 自定义bind实现
 * @param ctx 上下文
 * @returns {Function}
 */

Function.prototype.myBind = function (ctx) {
    ctx = typeof ctx === "object" ? ctx : window;
    return (...args) => {
        this.call(ctx, ...args); // 这里的this指向func
    };
};

function fun (arg1, arg2) {
    console.log(this.name);
    console.log(arg1 + arg2);
};

const _this = { name: "Ron" };

const newBind = fun.myBind(_this);
newBind(1, 2);
```

### 增加其余参数入参

```javascript
/**
 * 自定义bind实现
 * @param ctx 上下文
 * @param defaultArgs 其余参数
 * @returns {Function}
 */
Function.prototype.myBind = function (ctx, ...defaultArgs) {
    ctx = typeof ctx === "object" ? ctx : window;
    return (...args) => {
        args = defaultArgs.length ? defaultArgs : args;
        this.call(ctx, ...args); // 这里的this指向func
    };
};

function fun (arg1, arg2) {
    console.log(this.name);
    console.log(arg1 + arg2);
};

const _this = { name: "Ron" };

const newBind = fun.myBind(_this, 4, 5);
newBind(1, 2);
```
