---
title: 手写apply函数
date: 2021-12-29
tags:
 - Interview
---

## apply函数的定义与使用

### 定义

[MDN对 `Function.prototype.apply()` 的定义](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Function/apply)

### 使用

从定义可以看出 `apply` 相对于 `call` 除了传参类型不同外没有任何区别。

```javascript
// Function.prototype.apply()样例
function fun(arg1, arg2) {
  console.log(this.name)
  console.log(arg1 + arg2)
}
const _this = { name: "Ron" };
// 参数为数组;方法立即执行
fun.apply(_this, [1, 2]);

// 输出结果： Ron    3
```

## 手写实现

```javascript
/**
 * 自定义Apply实现
 * @param ctx   上下文this对象
 * @param args      参数数组
 */

Function.prototype.myApply = function (ctx, args) {
    ctx = (typeof ctx === "object" ? ctx : window);
    let key = Symbol();
    ctx[key] = this;
    const result = ctx[key](...args);
    delete ctx[key];
    return result;
};

function fun (arg1, arg2) {
    console.log(this.name);
    console.log(arg1 + arg2);
}
const _this = { name: "Ron" };

fun.myApply(_this, [1, 2]);
```
