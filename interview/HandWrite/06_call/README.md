---
title: 手写call函数
date: 2021-12-28
tags:
 - Interview
---

## call函数的定义与使用

### 定义

[MDN对 `Function.prototype.call()` 的定义](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Function/call)

### 使用

```javascript
// Function.prototype.call()样例
function fun(arg1, arg2) {
  console.log(this.name);
  console.log(arg1 + arg2);
}
const _this = { name: 'Ron' };
// 接受的是一个参数列表;方法立即执行
fun.call(_this, 1, 2);

// 输出： Ron   3
```

## 手写实现

```javascript
/**
 *
 * @param {*} ctx 函数执行上下文this
 * @param  {...any} args 参数列表
 * @returns 函数执行的结果
 */

Function.prototype.myCall = function(ctx, ...args) {
    ctx = (typeof ctx === 'object' ? ctx : window);
    // 防止覆盖掉原有属性
    const key = Symbol();
    // 这里的this为需要执行的方法 也就是fun函数; 给ctx增加了一个属性: 键为Symbol()，值为fun
    ctx[key] = this;
    // 方法执行 此时改变了this指向为ctx 也就是_this，故执行方法fun时里面的this便会指向_this
    const result = ctx[key](...args);
    delete ctx[key];
    // 这里的 result 是fun函数的返回值
    return result;
}

function fun(arg1, arg2) {
    console.log(this.name);
    console.log(arg1 + arg2);
    // return "hello";
}
const _this = { name: "Ron" };
// 接受的是一个参数列表;方法立即执行
fun.myCall(_this, 1, 2);
```
