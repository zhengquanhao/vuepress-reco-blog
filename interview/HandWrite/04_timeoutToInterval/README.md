---
title: 用setTimeout和clearTimeout简单实现setInterval与clearInterval
date: 2021-12-23
tags:
 - Interview
---

## 模拟setInterval函数

首先我们先用 `setTimeout` 实现一个简单版本的 `setInterval`。

`setInterval` 需要不停循环调用，这让我们想到了递归调用自身：

```javascript
// const simulateInterval = (cb, interval) => {
//     const fn = () => {
//         cb(); // 执行传入的回调函数
//         setTimeout(fn, interval); // 递归调用自己
//     }
//     setTimeout(fn, interval);
// };
const simulateInterval = (cb, interval) => {
    setTimeout(() => {
        cb();
        simulateInterval(cb, interval); // 递归调用
    }, interval);
};

simulateInterval(() => { console.log("test") }, 1000); // 测试代码
```

嗯，没啥问题，实现了我们想要的功能。等一下，怎么停下来？总不能执行了就不管了吧！

## clearInterval的实现

平时如果用到了 `setInterval` 的同学应该都知道 `clearInterval` 的存在（不然你怎么停下 `interval` 呢）。
`clearInterval` 的用法是 `clearInterval(id)`。而这个 `id` 是 `setInterval` 的返回值，通过这个 `id` 值就能够清除指定的定时器。

```javascript
const id = setInterval(() => {
  // ...
}, 1000)
// ...
clearInterval(id)
```

故需要优化 `simulateInterval` 函数增加返回值，且为了供其他函数使用将其设置为全局变量。

```javascript
let timer = null;

const simulateInterval = (cb, interval) => {
    timer = setTimeout(() => {
        cb();
        simulateInterval(cb, interval);
    }, interval);
};
```

实现clearInterval:

```javascript
const clearInterval = (id) => {
    clearTimeout(id);
}

clearInterval(timer); // 停止定时器
```

## 完整代码

```javascript
let timer = null;

const simulateInterval = (cb, interval) => {
    timer = setTimeout(() => {
        cb();
        simulateInterval(cb, interval);
    }, interval);
};

simulateInterval(() => { console.log("test") }, 1000);

const clearInterval = (id) => {
    clearTimeout(id);
}

clearInterval(timer); // 停止定时器
```

## 优化改造

用构造函数的方式进行优化和改造：

```javascript
const TimeIterator = (function(){
    var time = 1000 // 设置默认间隔
    var timer = null
    return function(_time, func) {
        time = _time || 1000
        this.setTime = function(_time){ // 设置时间间隔
            time = _time
        },
        this.startTimer = function(){ // 开启定时器
            var _this = this
            timer = setTimeout(function(){
                func()
                _this.startTimer()
            },time)
        },
        this.stopTimer = function(){ // 停止定时器
            clearTimeout(timer)
            timer = null
        }
    }
})()

const timer = new TimeIterator(500, ()=>{
    console.log("test") // 希望循环的方法
})
timer.startTimer()
timer.setTime(2000)
timer.stopTimer()
```
