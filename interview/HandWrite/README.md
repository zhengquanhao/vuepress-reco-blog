代码手写题目汇总
---

## 基础

1. [手写一个compose函数](/Interview/HandWrite/01_compose/)

2. [模拟instanceof](/Interview/HandWrite/02_instanceof/)

3. [多维数组拍平](/Interview/HandWrite/03_flat/)

4. [利用setTimeout模拟实现setInterval](/Interview/HandWrite/04_timeoutToInterval/)

5. [利用setInterval模拟实现setTimeout](/Interview/HandWrite/05_intervalToTimeout/)

6. [手写call函数](/Interview/HandWrite/06_call/)

7. [手写apply函数](/Interview/HandWrite/07_apply/)

8. [手写bind函数](/Interview/HandWrite/08_bind/)

9. [数组去重](/Interview/HandWrite/09_uniqueArray/)

10. [利用正则表达式实现trim](/Interview/HandWrite/10_trim/)

11. [分割电话号](/Interview/HandWrite/11_splitePhone/)

12. [实现new](/Interview/HandWrite/12_new/)

13. [深拷贝](/Interview/HandWrite/13_deepClone/)

14. [Object.create 实现原理](/Interview/HandWrite/14_objectCreate/)

15. [判断数据类型](/Interview/HandWrite/15_getType/)

16. [防抖和节流](/Interview/HandWrite/16_debounce/)

17. [javascript 柯里化curry](/Interview/HandWrite/17_curry/)
