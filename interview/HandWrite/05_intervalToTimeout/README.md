---
title: 用setInterval和clearInterval模拟实现setTimeout与clearTimeout
date: 2021-12-24
tags:
 - Interview
---

用setInterval和clearInterval模拟实现setTimeout与clearTimeout的思路为，开启Interval定时器一次后直接清除。

```javascript
let timer = null;

const simulateTimeout = (cb, interval) => {
    timer = setInterval(() => {
        clearInterval(timer); // 调用一次直接清除
        cb();
    }, interval);
}

// const clearTimeout = (id) => { // 上方清除过了，该方法几乎用不上
//     clearInterval(id);
// }

simulateTimeout(() => { console.log("test"); }, 1000);
```