---
title: 分割电话号
date: 2022-01-06
tags:
 - Interview
---

## 代码

```javascript
var str = '13212345678';
var newStr = '';
newStr = str.replace(/\s/g, '').replace(/(^\d{3})(?=\d)/g, "$1 ").replace(/(\d{4})(?=\d)/g, "$1 ");
console.log(newStr);
```

## 正则先行断言

`x(?=y)`: 匹配'x'仅仅当'x'后面跟着'y'，这种叫做先行断言。

例如，/Jack(?=Sprat)/会匹配到'Jack'仅当它后面跟着'Sprat'。/Jack(?=Sprat|Frost)/匹配‘Jack’仅当它后面跟着'Sprat'或者是‘Frost’。但是‘Sprat’和‘Frost’都不是匹配结果的一部分。
