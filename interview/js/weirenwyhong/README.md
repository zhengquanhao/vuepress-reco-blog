---
title: JS微任务和宏任务
date: 2022-08-30
tags:
 - Interview
---

这个问题实际想问：JS代码怎么执行以及执行顺序

js是单线程的语言，同一时间只能做一件事，JS的主要用途与用户互动以及操作DOM，这决定了它只能是单线程，否则会带来很复杂的同步问题，比如，假定js同时拥有两个线程，一个线程是在某个DOM节点上添加内容，另一个线程又是删除这个节点，这是浏览器应该以哪个线程为准？

js代码执行流程：所有的同步任务执行完 => 进入事件循环，事件循环包含宏任务、微任务(先执行所有的微任务再执行宏任务)

宏任务：setTimeout，setInterval，Ajax，DOM事件
微任务：Promise，async/await

```javascript
setTimeout(function() {
    console.log("1");
});

new Promise((resolve) => {
    console.log("promise1");
    resolve();
}).then(() => {
    console.log("微1");
}).then(() => {
    console.log("微2");
})

console.log("2");
// result: promise1  2  微1  微2  1
```
