---
title: 事件委托
date: 2021-12-04
tags:
 - Interview
---

## 事件委托

事件委托的原理：不给每个子节点单独设置事件监听器，而是设置在其父节点上，然后利用冒泡原理设置每个子节点。

事件委托的作用：只操作了一次dom，提高程序的性能。

[案例](https://zhengquanhao.gitlab.io/vuepress-reco-blog/Interview/js/dom/01_EventDelegation/example.html)：
```html
<ul>
    <li>第1个li</li>
    <li>第2个li</li>
    <li>第3个li</li>
</ul>
```

```javascript
// 事件委托的核心原理：给父节点添加侦听器， 利用事件冒泡影响每一个子节点
var ul = document.querySelector("ul");
ul.addEventListener("click",function(e){
    e.target.style.backgroundColor = "tomato";
})
```

在 JavaScript 中，添加到页面上的事件处理程序数量将直接关系到页面的整体运行性能，因为需要不断的操作 dom,那么引起浏览器重绘和回流的可能也就越多，页面交互的事件也就变的越长，这也就是为什么要减少 dom 操作的原因。每一个事件处理函数，都是一个对象，多一个事件处理函数，内存中就会被多占用一部分空间。如果要用事件委托，就会将所有的操作放到 js 程序里面，只对它的父级进行操作，与 dom 的操作就只需要交互一次，这样就能大大的减少与 dom 的交互次数，提高性能；
