---
title: AddEventListener 默认是捕获还是冒泡?
date: 2021-12-04
tags:
 - Interview
---

## addEventListener

默认是冒泡

addEventListener第三个参数默认为 false 代表执行事件冒泡行为。

当为 true 时执行事件捕获行为。
