---
title: 说一下 var、let 和 const 的区别
date: 2022-03-18
tags:
 - Interview
---

## 前言

本题难度：⭐

可以从这几个方面来描述 var、let 和 const 的区别：

- 变量提升
- 暂时性死区
- 块级作用域
- 重复声明
- 修改声明的变量

## 变量提升

变量提升是指变量声明或函数声明会被提升到当前作用域的最顶部去，且赋一个初始值 `undefined`。

- `var` 声明的变量存在变量提升
- `let` 和 `const` 声明的变量不存在变量提升

```javascript
console.log(name) // undefined
var name = 'Ron'
```

等价于

```javascript
var name = undefined
console.log(name) // undefined
name = 'Ron'
```

`let` 和 `const` 没有变量提升

```javascript
console.log(name) // Uncaught ReferenceError: name is not defined
let name = 'Ron'
```

```javascript
console.log(name) // Uncaught ReferenceError: name is not defined
const name = 'Ron'
```

## 暂时性死区

- `var` 不存在暂时性死区
- `let` 和 `const` 存在暂时性死区，只有等到声明变量的那一行代码出现，才可以获取和使用该变量

```javascript
console.log(name) // undefined
var name = 'Ron'
```

```javascript
console.log(name) // Uncaught ReferenceError: name is not defined
let name = 'Ron'
```

```javascript
console.log(name) // Uncaught ReferenceError: name is not defined
const name = 'Ron'
```

## 块级作用域

- `var` 没有块级作用域
- `let` 和 `const` 存在块级作用域

```javascript
if (true) {
  var name = 'Ron'
}
console.log(name) // 'Ron'
```

```javascript
if (true) {
  let name = 'Ron'
}
console.log(name) // Uncaught ReferenceError: name is not defined
```

```javascript
if (true) {
  const name = 'Ron'
}
console.log(name) // Uncaught ReferenceError: name is not defined
```

## 重复声明

- `var` 允许重复声明，后声明的会覆盖先声明的
- `let` 和 `const` 不允许重复声明

```javascript
var name = 'Ron'
var name = 'Ronen'
console.log(name) // 'Ronen' 重复声明，后声明的会覆盖先声明的
```

```javascript
let name = 'Ron'
let name = 'Ronen' // Identifier 'name' has already been declared
```

```javascript
const name = 'Ron'
const name = 'Ronen' // Identifier 'name' has already been declared
```

## 修改声明的变量

- `var` 和 `let` 可以修改声明的变量
- `const` 声明一个只读的常量。一旦声明，常量的值就不能改变

```javascript
var name = 'Ron'
name = 'Ronen'
console.log(name) // 'Ronen'
```

```javascript
let name = 'Ron'
name = 'Ronen'
console.log(name) // 'Ronen'
```

```javascript
const name = 'Ron'
name = 'Ronen' // Uncaught TypeError: Assignment to constant variable.
```

## 如何使用？

- 能用 `const` 的情况尽量使用 `const`
- 声明的变量要被修改，用 `let`
- 不使用 `var`

> 为什么不推荐使用 `var`，而是推荐使用 `let`? 因为var创建的变量是挂载在 `window` 顶级对象上面的，如果跟之前定义的对象重复则会改变之前定义的值，变量造成污染。
