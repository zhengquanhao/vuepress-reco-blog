---
title: 原型与原型链
date: 2022-04-14
tags:
 - Interview
---

## 原型

### 什么是原型？

原型就是函数都有一个特殊的属性，叫作 `prototype`

原型对象打印出来是什么样子呢？原型对象有一个自有属性 `constructor`，这个属性指向该函数，原型对象也有自己的原型对象。

```javascript
function doSomething(){}
console.log( doSomething.prototype );
```

```javascript
{
    constructor: ƒ doSomething(),
    __proto__: {
        constructor: ƒ Object(),
        hasOwnProperty: ƒ hasOwnProperty(),
        isPrototypeOf: ƒ isPrototypeOf(),
        propertyIsEnumerable: ƒ propertyIsEnumerable(),
        toLocaleString: ƒ toLocaleString(),
        toString: ƒ toString(),
        valueOf: ƒ valueOf()
    }
}
```

`constructor` 属性指向该函数，如下图关系展示

![constructor](./images/constructor.png)

## 原型链

### 原型链是什么？

- 原型链 `__proto__`  => 新版chrome更名为 `[[prototype]]`
- 原型对象也可能拥有原型，并从中继承方法和属性，就这样一层一层的进行关联，这种关系组成原型链

当试图访问一个对象的属性时，会先从当前实例的属性去查找，如果找到了就返回，否则顺着原型链一层一层往上找，直到找到null（原型链的顶端）为止

代码举例：

```javascript
function Person(name) {
    this.name = name;
    this.age = 18;
    this.sayName = function() {
        console.log(this.name);
    }
}
// 创建实例
var person = new Person('Ron'); // 该实例本身永远属性name、age，以及方法sayName

// 在原型上添加属性和方法
Person.prototype.sex = 'man';
Person.prototype.sayHello = () => { console.log('hello') }

// 访问自有属性
console.log(person.name); // Ron
console.log(person.age); // 18
person.sayName(); // Ron

// 访问原型上的属性
console.log(person.sex); // man
person.sayHello(); // hello

// 访问原型链上都不存在的属性
console.log(person.height); // undefined
```

归纳`构造函数`、`原型对象`、`实例对象`三者关系如图

![relation](./images/relation.png)
