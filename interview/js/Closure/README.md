---
title: 举出闭包实际场景运用的例子
date: 2022-01-14
tags:
 - Interview
---

## 闭包的定义

闭包是指内部函数总是可以访问其所在的外部函数中声明的变量和参数，即使在其外部函数被返回（寿命终结）了之后。

### 举例

现在 `innerFunc()` 在其词法作用域之外执行，但是 `innerFunc()` 仍然可以从其词法作用域访问 `outerVar`，即使是在词法作用域之外执行。也就是说 `innerFunc()` 从其词法作用域捕获(又称记忆）变量 `outerVar`。

换句话说，`innerFunc()` 是一个闭包，因为它在词法作用域内捕获了变量 `outerVar`。

正常来说，当 `outerFunc` 函数执行完毕之后，其作用域是会被销毁的，然后垃圾回收器会释放那段内存空间。而闭包却很神奇的将 `outerFunc` 的作用域存活了下来，`innerFunc` 依然持有该作用域的引用，这个引用就是闭包。

![闭包](./images/closure.png)

## 闭包实际场景运用的例子

### 1. 常见的防抖节流

```javascript
// 防抖
function debounce (fun, delay = 1000) {
    let timer = null;
    return function() {
        let self = this;
        const args = [...arguments];
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            fun.call(self, args);
        }, delay);
    }
}

// window.addEventListener("resize", () => {console.log("resize")});
window.addEventListener("resize", debounce(() => {console.log("resize")}));

//节流
function throttle(fun, delay = 1000){
    let pre = new Date().getTime();
    return function() {
        const args = [...arguments];
        const cur = new Date().getTime();
        if(cur - pre >= delay){
            pre = cur;
            fun.apply(this, args);
        }
    }
}

window.onresize = throttle(() => console.log("resize"), 2000);
```

### 2. 使用闭包可以在 JavaScript 中模拟块级作用域

```javascript
function outputNumbers(count) {
  (function () {
    for (var i = 0; i < count; i++) {
      alert(i);
    }
  })();
  alert(i); //导致一个错误！
}
```

### 3. 闭包可以用于在对象中创建私有变量

JavaScript没有像Java中的private来定义私有变量，但可以通过闭包来实现。

```javascript
//用闭包定义能访问私有函数和私有变量的公有函数。
var counter = (function(){
    var privateCounter = 0; //私有变量
    function change(val){
        privateCounter += val;
    }
    return {
        increment:function(){   //三个闭包共享一个词法环境
            change(1);
        },
        decrement:function(){
            change(-1);
        },
        value:function(){
            return privateCounter;
        }
    };
})();

console.log(counter.value());//0
counter.increment();
counter.increment();//2
//共享的环境创建在一个匿名函数体内，立即执行。
//环境中有一个局部变量一个局部函数，通过匿名函数返回的对象的三个公共函数访问。
```
