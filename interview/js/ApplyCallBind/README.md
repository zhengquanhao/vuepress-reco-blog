---
title: apply call bind 区别
date: 2022-01-13
tags:
 - Interview
---

1. 三者都可以改变函数的 `this` 对象指向。

2. 三者第一个参数都是 `this` 要指向的对象，如果如果没有这个参数或参数为 undefined 或 null，则默认指向全局 window。

3. 三者都可以传参，但是 `apply 是数组`，而 `call 是参数列表`，且 apply 和 call 是一次性传入参数，而 `bind 可以分为多次传入`。

4. bind 是返回绑定 `this 之后的函数`，便于稍后调用；apply 、call 则是`立即执行`。

5. bind()会返回一个新的函数，如果这个返回的新的函数作为`构造函数`创建一个新的对象，那么此时 this `不再指向`传入给 bind 的第一个参数，而是指向用 new 创建的实例。

> 注意！很多同学可能会忽略 bind 绑定的函数作为构造函数进行 new 实例化的情况。
