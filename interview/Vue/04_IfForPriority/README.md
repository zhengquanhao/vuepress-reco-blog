---
title: v-for和v-if的优先级
date: 2021-05-23
tags:
 - Interview
---

v-for的优先级比v-if高，在源码中进行提现的`function genElement`。

![priority](./images/priority.png)

注意：v-for和v-if不能一起使用，编译器里就会报错，一般我们在使用时先v-if判断有没有再进行v-for循环，如果反过来的话会造成无用循环浪费性能。
