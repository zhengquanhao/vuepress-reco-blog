---
title: v-if和v-show的区别
date: 2021-05-22
tags:
 - Interview
---

## 展示形式不同

v-if是把dom节点创建/删除
v-show是展示/隐藏 => display:none/block

## 使用场景不同

初次加载v-if要比v-show好，页面不会创建dom元素，用到的时候再去创建
频繁切换v-show要比v-if好，创建删除的开销要比展示隐藏大
