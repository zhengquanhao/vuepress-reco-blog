---
title: scoped原理
date: 2021-05-25
tags:
 - Interview
---

## 作用

让样式只在本组件中生效，不影响其他组件

## 原理

给节点新增加自定义属性，然后css根据属性选择器添加样式。

没加scoped:

![no-scoped](./images/no-scoped.png)

添加scoped:

![scoped](./images/scoped.png)
