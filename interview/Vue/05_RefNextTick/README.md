---
title: ref是什么？$nextTick是什么？
date: 2021-05-24
tags:
 - Interview
---

## ref是什么？

ref是来获取dom的

```html
<template>
  <button ref="btn">1111</button>
</template>

<script>
export default {
    mounted () {
      console.log(this.$refs.btn.innerHTML); // 1111
    }
}
</script>
```

## $nextTick是什么？

获取更新后的dom

使用场景：

```html
<template>
  <button ref="btn" @click="btnClick">{{msg}}</button>
</template>

<script>
export default {
    data () {
        return {
            msg: "Hello"
        }
    },
    methods: {
        btnClick () {
            this.msg = "world";
            console.log(this.$refs.btn.innerHTML); // hello

            this.$nextTick(() => {
                console.log(this.$refs.btn.innerHTML); // world
            });
        }
    }
}
</script>
```
