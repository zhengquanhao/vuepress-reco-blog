---
title: Vue2.x的生命周期
date: 2021-05-20
tags:
 - Interview
---

## 一、有哪些生命周期

系统自带八个：beforeCreate、created、beforeMount、mounted、beforeUpdate、updated、beforeDestroy、destroyed

## 如果加入了keep-alive会多俩生命周期

activated(进入组件)、deactivated（销毁组件）

## 一旦进入组件会执行哪些生命周期

会执行前四个：beforeCreate、created、beforeMount、mounted

## 如果加入了keep-alive，第一次进入组件会执行哪些生命周期

beforeCreate、created、beforeMount、mounted、activated

## 如果加入了keep-alive，第二次/第N次进入组件会执行哪些生命周期

只会执行一个生命周期 activated

## 在哪个阶段有$el，哪个阶段有$data

$el: 实例提供挂载元素，组件的根节点
$data: 数据，我们常写的 `data () {}`

```javascript
console.log("生命周期", this.$el, this.$data)
```

结果：

```shell
beforeCreate undefined undefined
created undefined Object
beforeMount undefined Object
mounted <div class="home">...</div> Object
```

## Vue生命周期的简单原理

```html
<script src="./vue.js"></script>
<script>
    new Vue({
        beforeCreate () {
            console.log("beforeCreate");
        },
        created () {
            console.log("created");
        },
        beforeMount () {
            console.log("beforeMount");
        },
        mounted () {
            console.log("mounted");
        }
    })
</script>
```

```javascript
class Vue {
    // 构造方法，new 的时候constructor方法会自动执行
    constructor(options) {
        options.beforeCreate();
        options.created();
        options.beforeMount();
        options.mounted();
    }
}
```

**那this的指向和何时存在$el,$data又是如何控制的呢？**

```html
<div id="app"></div>
<script src="./vue.js"></script>
<script>
    new Vue({
        el: "#app",
        data: {
            msg: "hello"
        },
        beforeCreate () {
            console.log("beforeCreate", this.$data, this.$el);
        },
        created () {
            console.log("created", this.$data, this.$el);
        },
        beforeMount () {
            console.log("beforeMount", this.$data, this.$el);
        },
        mounted () {
            console.log("mounted", this.$data, this.$el);
        }
    })
</script>
```

```javascript
// 通过bind改变this的指向，改变$el,$data在生命周期中的属性达到效果
class Vue {
    // 构造方法，new 的时候constructor方法会自动执行
    constructor(options) {
        options.beforeCreate.bind(this)();

        this.$data = options.data;

        options.created.bind(this)();
        options.beforeMount.bind(this)();

        this.$el = document.querySelector(options.el);

        options.mounted.bind(this)();
    }
}
```

输出结果：

```shell
beforeCreate undefined undefined
created {msg: 'hello'} undefined
beforeMount {msg: 'hello'} undefined
mounted {msg: 'hello'} <div id=​"app">​</div>​
```
