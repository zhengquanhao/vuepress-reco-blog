---
title: 你对SPA单页面的理解，它的优缺点分别是什么？如何实现SPA应用呢
date: 2022-08-16
tags:
 - Interview
---

## 一、什么是SPA

单页面应用其实是一种网站的模型，它的特点是通过动态重写当前页面来实现与用户交互，通过路由的方式让所有代码都通过单个页面加载和渲染，这种方式就避免了多页面应用的问题（页面之间切换打断用户体验）。

举个例子：一个杯子，早上装的牛奶，中午装的是开水，晚上装的是茶，我们发现，变的始终是杯子里的内容，而杯子始终是那个杯子；多页面则早中晚三个杯子各不相同。

## 二、SPA的优缺点

优点：

1. 用户体验好，内容的改变不需要重新加载整个页面
2. 前后端分离，分工更明确
3. 减轻服务器压力，服务器只用出数据就可以，不用管展示逻辑和页面合成

缺点：

1. 首次渲染速度相对较慢
2. 不利于搜索引擎的抓取

## 三、SPA/路由切换原理

要做的两件事：

1. 改变浏览器地址
2. 内容区切换内容

### 哈希模式

哈希模式有一个特点就是改变url中的哈希时不会刷新页面，核心通过`hashchange`事件来监听地址栏中hash变化驱动界面变化

改变浏览器地址 => `window.location.hash`; 内容区切换内容 => `hashchange`监听到变化后通过Dom操作改变界面内容

```html
<body>
    <ul>
        <li><a data-url="/aa">aaa</a></li>
        <li><a data-url="/bb">bbb</a></li>
        <li><a data-url="/cc">ccc</a></li>
    </ul>
    <div id="content">内容区域</div>
</body>
<script>
    // 路由数据
    let _routeData = [
        {
            path: "/aa",
            pathContent: "a页面的内容"
        },
        {
            path: "/bb",
            pathContent: "b页面的内容"
        },
        {
            path: "/cc",
            pathContent: "c页面的内容"
        }
    ]
    // 获取按钮
    let _aBtn = document.getElementsByTagName("a");
    // 获取内容元素
    let _divContent = document.getElementById("content");
    // 当前路由
    let _path;

    for(let i = 0; i <_aBtn.length; i++) {
        _aBtn[i].addEventListener("click", function() {
            _path = this.getAttribute("data-url");
            window.location.hash = _path;
        })
    }

    window.addEventListener("hashchange", () => {
        _routeData.map(item => {
            if(item.path === _path) {
                _divContent.innerHTML = item.pathContent;
            }
        })
    })
</script>
</html>
```

### history模式

核心是使用了`pushState()`方法和`popstate`事件

- history.pushState：可以改变url地址而不刷新页面，浏览器历史纪录添加记录
- history.popstate：当`history`发生变化时触发（比如在点击浏览器前进、后退按钮时）

pushState(state, title, url)有三个参数：

- 第一个state会在popstate事件触发时（就是用户点击前进后退按钮时）以回调的方式传入一些数据
- 第二个title，新页面的标题，，但浏览器都忽略了，确实不好使，一般传null
- 第三个为新网址，在浏览地址栏显示，本案例主要核心就是这个参数

```html
<body>
    <ul>
        <li><a data-url="#/aa">aaa</a></li>
        <li><a data-url="#/bb">bbb</a></li>
        <li><a data-url="#/cc">ccc</a></li>
    </ul>
    <div id="content">内容区域</div>
</body>
<script>
    // 路由数据
    let _routeData = [
        {
            path: "#/aa",
            pathContent: "a页面的内容"
        },
        {
            path: "#/bb",
            pathContent: "b页面的内容"
        },
        {
            path: "#/cc",
            pathContent: "c页面的内容"
        }
    ]
    // 获取按钮
    let _aBtn = document.getElementsByTagName("a");
    // 获取内容元素
    let _divContent = document.getElementById("content");
    // 当前路由
    let _path;

    // 更新视图
    function refreshViews(_path) {
        _routeData.map(item => {
            if(item.path === _path) {
                _divContent.innerHTML = item.pathContent;
            }
        })
    }

    for(let i = 0; i <_aBtn.length; i++) {
        _aBtn[i].addEventListener("click", function() {
            _path = this.getAttribute("data-url");
            window.history.pushState({}, null, _path);
            refreshViews(_path);
        })
    }

    window.addEventListener("popstate", () => {
        refreshViews(window.location.hash);
    })
</script>
```
