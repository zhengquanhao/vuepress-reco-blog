---
title: 如何封装一个axios
date: 2021-05-25
tags:
 - Interview
---

封装前需要先和后端协商好一些约定，请求头、状态码、请求超时时间...

```javascript
const instance = axios.create({
    baseURL: "/",
    timeout: 3000, // 请求超时时间，如果超时就会走catch的逻辑
    headers: { // 请求头
        "Content-Type": header || "application/x-www-form-urlencoded",
        "X-API-Timestamp": new Date().getTime(),
        "X-Requested-With": "XMLHttpRequest"
    }
})

// 状态码：比如未拿到登录态的状态码要用来重定向
```

设置接口请求的前缀：根据开发、测试、生成环境的不同，前缀需要加以区分 (baseURL)

