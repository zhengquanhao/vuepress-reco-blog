---
title: 使用Hover.css快速开发动画效果
date: 2021-12-08
tags:
 - CSS
categories:
 - CSS
---

## 一、介绍

在我们日常做项目的时候动画特效总是不可避免，`Hover.css` 默认提供了许多动画效果，我们可以点击[这里](http://ianlunn.github.io/Hover/?spm=a2c6h.12873639.0.0.2c966215JsfRJz)查看所有的特效，如果遇到匹配的效果可以直接找到对应的代码进行复制和使用。

## 二、使用

使用方式也很简单，以 `Wobble Horizontal` 特效为例，点击 [GitHub](https://github.com/IanLunn/Hover)，打开`css/hover.css`文件（如果你采用了预处理器也可打开对应文件）全局搜索 `Wobble Horizontal` 便能找到动画的代码：

```css
/* Wobble Horizontal */
@-webkit-keyframes hvr-wobble-horizontal {
  16.65% {
    -webkit-transform: translateX(8px);
    transform: translateX(8px);
  }
  33.3% {
    -webkit-transform: translateX(-6px);
    transform: translateX(-6px);
  }
  49.95% {
    -webkit-transform: translateX(4px);
    transform: translateX(4px);
  }
  66.6% {
    -webkit-transform: translateX(-2px);
    transform: translateX(-2px);
  }
  83.25% {
    -webkit-transform: translateX(1px);
    transform: translateX(1px);
  }
  100% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}
```

将代码复制到项目中使用即可。