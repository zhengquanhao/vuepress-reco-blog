class Vue {
    constructor(options) {
        this.$el = options.el;
        this.$data = options.data;
        // 目标：编译模板并渲染到界面上
        // 如果这个元素存在则编译模板
        if(this.$el) {
            // 数据劫持，把数据全部转化成Object.defineProperty来定义
            new IntersectionObserver(this.$data);

            // 将$data下的数据 编译到$el的内容下
            new Compile(this.$el, this);
        }
    }
}

// 实现数据劫持
class IntersectionObserver {
    constructor(data) {
        this.observer(data);
    }

    observer(data) {
        // 如果是对象才观察
        if(data && typeof data === "object") {
            // 如果是对象
            for(let key in data) {
                this.defineReactive(data, key, data[key]);
            }
        }
    }

    defineReactive(obj, key, value) {
        this.observer(value); // 递归使对象的每层都做到数据劫持
        Object.defineProperty(obj, key, {
            get() {
                return value;
            },
            set: (newValue) => {
                if(newValue !== value) {
                    this.observer(newValue); // 新增的数据也要做数据劫持
                    value = newValue;
                }
            }
        })
    }
}

CompileUtil = {
    // 根据表达式取到对应的数据
    getVal(vm, expr) { // expr => school.name
        return expr.split(".").reduce((data, current) => {
            return data[current];
        }, vm.$data);
    },
    model(node, expr, vm) { // node是节点 expr是表达式 vm是当前实例
        let fn = this.updater["modelUpdater"];
        let value = this.getVal(vm, expr);
        fn(node, value);
    },
    html() {
        // node.innerHTML = xxx
    },
    text(node, expr, vm) {
        let fn = this.updater["textUpdater"];
        let content = expr.replace(/\{\{(.+?)\}\}/g, (...args) => {
            return this.getVal(vm, args[1]);
        })
        fn(node, content);
    },
    updater: {
        // 把数据插入到节点中
        modelUpdater(node, value) {
            node.value = value;
        },
        // 处理文本节点
        textUpdater(node,value) {
            node.textContent = value;
        }
    }
}

// 编译类
class Compile {
    constructor(el, vm) {
        // 判断el是不是元素，如果不是元素就通过DOM获取
        this.el = this.isElementNode(el) ? el : document.querySelector(el);
        console.log(this.el); // <div id="app"></div>

        this.vm = vm; // 主要使用vm中的data数据

        // 把当前节点下的全部元素获取到放到内存中
        let fragment = this.node2fragment(this.el);

        // 把节点中的内容进行替换

        // 编译模板
        this.compile(fragment);

        // 把内容塞到页面中
        this.el.appendChild(fragment);
    }

    // 判断是否是元素节点
    isElementNode(node) {
        return node.nodeType === 1;
    }

    // 把节点移动到内存中
    node2fragment(node) {
        // 创建一个文档碎片
        let fragment = document.createDocumentFragment();
        let firstChild;
        while(firstChild = node.firstChild) {
            // appendChild具有移动性，所以每次的node.firstChild都不同，当全部移动到文档碎片中循环结束
            fragment.appendChild(firstChild);
        }
        return fragment;
    }

    // 判断元素属性是否是指令
    isDirective(attrName) {
        return attrName.startsWith("v-");
    }

    // 编译元素的
    compileElement(node) {
        let attributes = node.attributes; // 类数组
        [...attributes].forEach(attr => {
            let { name, value:expr } = attr;
            if(this.isDirective(name)) {
                console.log(node);
                let [, directive] = name.split("-");
                // 需要调用不同指令来处理
                CompileUtil[directive](node, expr, this.vm);
            }
        })
    }

    // 编译文本的
    compileText(node) {
        let content = node.textContent;
        if(/\{\{(.+?)\}\}/.test(content)) {
            CompileUtil['text'](node, content, this.vm); // {{a}} {{b}}
        }
    }

    // 核心的编译方法
    compile(node) { // 用来编译内存中的dom节点，解析v-model、{{ }}
        let childNodes = node.childNodes; // 拿到的是类数组
        console.log(childNodes);
        // 判断元素中是否有v-model，文本中是否有{{ }}
        [...childNodes].forEach(child => {
            if(this.isElementNode(child)) {
                this.compileElement(child);
                this.compile(child); // 如果是元素节点，继续遍历里面的子元素
            } else {
                this.compileText(child);
            }
        })
    }
}