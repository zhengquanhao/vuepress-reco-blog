---
title: Vue2文档查缺补漏
date: 2022-02-08
tags:
 - Vue
categories:
 - Vue
---

## 插槽

### 作用域插槽

让插槽内容(父组件)能够访问子组件中才有的数据。当一个组件被用来渲染一个项目数组时，这是一个常见的情况，我们希望能够自定义每个项目的渲染方式。

父组件：

```html
<todo-list>
    <template v-slot:default="slotProps">
        <span class="green">{{ slotProps.item }}</span>
    </template>
    <template v-slot:other="otherSlotProps">
        {{otherSlotProps.msg}}
    </template>
</todo-list>
```

TodoList子组件：

```html
<template>
    <ul>
        <li v-for="( item, index ) in items" :key="index">
            <slot :item="item"></slot>
        </li>
        <slot name="other" :msg="'hello world'"></slot>
        </ul>
</template>

<script>
export default {
    data () {
        return {
            items: ["Feed a cat", "Buy milk"]
        };
    }
};
</script>
```

在这个例子中，我们选择将包含所有插槽 `prop` 的对象命名为 `slotProps` 和 `otherSlotProps`，但你也可以使用任意你喜欢的名字。

或使用解构：

```html
<todo-list>
    <template v-slot="{ item }">
        <span class="green">{{ item }}</span>
    </template>
</todo-list>
```

> 注意，除了上述情况，`v-slot` 只能添加在 `<template>` 上。

### 具名插槽的缩写

跟 v-on 和 v-bind 一样，v-slot 也有缩写，即把参数之前的所有内容 (v-slot:) 替换为字符 #。例如 v-slot:header 可以被重写为 #header：

```html
<base-layout>
  <template #header>
    <h1>Here might be a page title</h1>
  </template>

  <template #default>
    <p>A paragraph for the main content.</p>
    <p>And another one.</p>
  </template>

  <template #footer>
    <p>Here's some contact info</p>
  </template>
</base-layout>
```

然而，和其它指令一样，该缩写只在其有参数的时候才可用。这意味着以下语法是无效的：

```html
<!-- 这将触发一个警告 -->

<todo-list #="{ item }">
  <i class="fas fa-check"></i>
  <span class="green">{{ item }}</span>
</todo-list>
```

如果希望使用缩写的话，你必须始终以明确的插槽名取而代之：

```html
<todo-list #default="{ item }">
  <i class="fas fa-check"></i>
  <span class="green">{{ item }}</span>
</todo-list>
```

上例改写：

```html
<todo-list>
    <template #default="{ item }">
        <span class="green">{{ item }}</span>
    </template>
    <template #other="{ msg }">
        {{ msg }}
    </template>
</todo-list>
```
