---
title: ElementUI组件库的一些坑
date: 2021-12-07
tags:
 - Vue
categories:
 - Vue
---

## 一、`el-popover`文字换行问题

首先需要明确知道，`el-popover` 通过条件触发后会在最外层（#app同级）创建出`.el-popover`元素，该元素默认样式文本换行样式为`word-break: break-all;`。

所以想要换行要自定义组件class名称并加入样式：

```vue
<el-popover
    placement="top"
    popper-class="data-popper"
    width="300"
    trigger="hover"
    :content="***">
    <span slot="reference" class="tips-icon"></span>
</el-popover>
...
.data-popper{
    word-break: break-word !important;
}
```

当我认为完成达成效果的时候，居然还是摆了我一道，竟呈现出了以下难以想象的展示效果：

![el-popover错误展示](./images/popover-tips.png)

每个单词居然间隔这么长，而且产品提供的文案也没问题呀，最后找来找去原来是这行样式在作怪：

```css
text-align: justify;
```

查了一下`text-align: justify;`表示文字向两侧对齐，对最后一行无效。

重新修改对齐方式问题得以解决：

```css
.data-popper{
    word-break: break-word !important;
    text-align: left !important;
}
```
