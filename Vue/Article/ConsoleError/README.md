---
title: Vue项目控制台报错归纳
date: 2021-12-03
tags:
 - Vue
categories:
 - Vue
---

## 一、 `v-for`问题

控制台出现以下错误提示是由于`v-for`循环里，`key`值可能重复了，所以会报这个错，使用item的唯一id作为key。

```shell
Duplicate keys detected: '***'. This may cause an update error
```