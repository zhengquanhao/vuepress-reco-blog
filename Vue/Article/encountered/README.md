---
title: Vue开发中遇到的问题整理
date: 2021-12-03
tags:
 - Vue
categories:
 - Vue
---

Vue开发中遇到的问题整理
---

## 一、如何在data/computed里调用methods中的方法

以data为例：

```vue
data() {
    let self = this  // 加上这一句就OK了
    return {
        list: [
            {
                label: "text",
                num: 10,
                cb () {
                    self.goTo();
                }
            }
        ]
    };
}
```

## 二、vue项目使`echarts`图标宽自适应

```vue
mounted() {
    this.getBizRealTimeTargets();
    this.initChart();
    // echars图标宽度自适应
    window.addEventListener("resize", () => {
        let myChart = echarts.init(this.$refs.effectChart)
        myChart.resize()
    }, false);
}
```