---
title: Vite项目装完包执行npm run serve后浏览器报Cannot GET /
date: 2022-04-25
tags:
 - Vite
categories:
 - Vite
---

在使用 Vite 时，想像以前执行 `npm run serve` 一样，查看生产环境的应用。

在执行后并未发现异常，但在浏览器访问时，显示 Cannot GET /，说是找不到根目录。

查看了 `package.json` 文件中的 scripts，运行 `npm run serve` 执行的是 `vite preview`。

![script脚本](./images/script.png)

> 所以说应该运行`npm run dev`的，我运行错命令了，尴尬。。。

但为什么运行serve命令就操作了呢？或者说 `vite preview` 到底是干什么用的？

vite preview 用作预览本地构建，而不应直接作为生产服务器。

所以需要先运行 `npm run build` 后，再运行 `npm run serve` 可以正常访问啦！
