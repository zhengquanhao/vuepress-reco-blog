---
title: vite-ts项目初始化
date: 2022-06-07
tags:
 - Vite
categories:
 - Vite
---

## 通过vite创建vue项目

```shell
npm init vite@latest
```

## 引入ts

### 安装

```shell
npm i typescript -D
```

### 初始化tsconfig.json

```shell
npx tsc --init
```

### 添加ts配置文件

`/src/main.js`改成`main.ts`，且在`index.html`引入中改成ts; `.vue`文件的`script`改成`<script lang="ts">`。

此时`main.ts`等文件中会报错不识别`.vue`文件。在根目录添加`shim.d.ts`配置文件,添加以下内容：

```typescript
declare module "*.vue" {
    import { Component } from "vue";
    const component: Component;
    export default component;
}
```

## 初始化vite配置文件

```typescript
import type { UserConfig } from "vite";
import vue from "@vitejs/plugin-vue";

const viteConfig:UserConfig = {
    plugins:[vue()],
    server: {
        host: "127.0.0.1", // 默认是 localhost
        port: 8080, // 默认是 3000 端口
        cors: true, // 默认启用并允许任何源
        open: true, // 浏览器自动打开
        //反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
        // proxy: {
        //     "/api": {
        //         target: "http://目标地址",   //代理接口
        //         changeOrigin: true,
        //         rewrite: (path) => path.replace(/^\/api/, "")
        //   }
        // }
    }
}

export default viteConfig;
```
