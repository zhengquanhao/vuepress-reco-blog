const nav = require("./nav");
const sidebar = require("./sidebar");
const plugins = require("./plugins");

module.exports = {
    title: "Ron's Blog",
    description: "A Personal Blog",
    base: process.env.NODE_ENV === "production" ? "./" : "/",
    port: "8000",
    dest: "./dist",
    head: [
        ["meta", { name: "viewport", content: "width=device-width,initial-scale=1,user-scalable=no" }],
        ["link", { rel: "icon", href: "/favicon.ico" }],
        ["link", { rel: "stylesheet", href: "/css/style.css" }],
        ["script", { type: "text/javascript", src: "/js/main.js" }],
    ],
    markdown: {
        lineNumbers: true
    },
    theme: "reco",
    themeConfig: {
        nav,
        sidebar,
        logo: "/logo.png",
        search: true,
        searchMaxSuggestions: 10,
        lastUpdated: "Last Updated",
        author: "Ron",
        authorAvatar: "/avatar.png",
        startYear: "2021",
        type: "blog",
        blogConfig: {
            // "category": {
            //     "location": 2,
            //     "text": "Category"
            // },
            tag: {
                location: 6,
                text: "标签"
            }
        },
        friendLink: [
            {
                title: "午后南杂",
                desc: "Enjoy when you can, and endure when you must.",
                logo: "/friend/luan.png",
                link: "https://www.recoluan.com"
            },
            {
                title: "vuepress-theme-reco",
                desc: "A simple and beautiful vuepress Blog & Doc theme.",
                logo: "/friend/reco.png",
                link: "https://vuepress-theme-reco.recoluan.com"
            },
            {
                title: "Lehi J.",
                desc: "E4111第一美男子姜同学的博客",
                logo: "/friend/jiang.png",
                link: "https://lehichiang.github.io/"
            },
            {
                title: "Yao's Blog",
                desc: "E4111最二次元的男人的博客",
                logo: "/friend/yao.png",
                link: "https://jdhsgs.github.io/yaoyin-blog-dist"
            }
        ]
    },
    plugins
}