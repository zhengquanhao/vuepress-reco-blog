module.exports = [
    {
        text: "首页",
        link: "/",
        icon: "reco-home"
    },
    {
        text: "技术汇总",
        icon: "reco-category",
        items: [ // 子标题
            {
                text: "入门前端",
                items: [
                    {
                        text: "前端入门路线",
                        link: "/Routine/"
                    },
                    {
                        text: "必备工具介绍",
                        link: "/Utils/"
                    },
                    {
                        text: "Http",
                        link: "/Http/"
                    },
                    {
                        text: "JavaScript基础",
                        link: "/JS/"
                    }
                ]
            },
            {
                text: "JS高级知识",
                items: [
                    {
                        text: "ES6",
                        link: "/ES6/"
                    },
                    {
                        text: "Node",
                        link: "/Node/"
                    }
                ]
            },
            {
                text: "三大框架",
                items: [ // 子标题
                    { text: "React", link: "https://zh-hans.reactjs.org/"}, // 外部链接
                    { text: "Vue", link: 'https://cn.vuejs.org/' },
                    { text: "Angular", link: "https://angular.cn/" },
                    { text: "Vue3.0", link: "/Vue3/"}
                ]
            },
            {
                text: "构建工具",
                items: [
                    {
                        text: "Webpack",
                        link: "/Webpack/"
                    },
                    {
                        text: "Vite",
                        link: "/Vite/"
                    }
                ]
            },
            {
                text: "永无止境",
                items: [
                    {
                        text: "TypeScript",
                        link: "/TypeScript/"
                    },
                    {
                        text: "React Native",
                        link: "/RN/"
                    }
                ]
            },
        ]
    },
    {
        text: "工具搭建",
        link: "/Tools/", // 内部链接 以docs为根目录
        icon: "reco-suggestion"
    },
    {
        text: "面试题目",
        link: "/Interview/", // 内部链接 以docs为根目录
        icon: "reco-document"
    },
    {
        text: "时间线",
        link: "/timeline/",
        icon: "reco-date"
    },
    {
        text: "留言",
        link: "/Comment/",
        icon: "reco-message"
    },
    {
        text: "GitLab",
        link: "https://gitlab.com/zhengquanhao/",
        icon: "reco-gitlab"
    }
]