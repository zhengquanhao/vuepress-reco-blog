module.exports = {
    "/Tools/": [
        {
            title: "NPM",
            path: "/Tools/npm/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "手写一个 NPM 包",
                    path: "/Tools/npm/01_WriteAnNpmPackage/"
                },
                {
                    title: "发布自己的 NPM 包",
                    path: "/Tools/npm/02_PublishNpmPackage/"
                }
            ]
        },
        {
            title: "Vite",
            path: "/Tools/vite/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "Vite项目搭建",
                    path: "/Tools/vite/01_ViteProjectConstruction/"
                }
            ]
        },
        {
            title: "Vue",
            path: "/Tools/vue/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "Vue项目配置在局域网下访问",
                    path: "/Tools/vue/01_AccessUnderLAN/"
                }
            ]
        },
    ],
    "/Utils/": [
        {
            title: "Chrome浏览器",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "Chrome插件推荐",
                    path: "/Utils/Browser/extension/"
                },
                {
                    title: "Chrome快捷键",
                    path: "/Utils/Browser/hotKey/"
                },
                {
                    title: "如何导出Chrome插件",
                    path: "/Utils/Browser/downExtension/"
                },
                {
                    title: "关闭Chrome同源策略",
                    path: "/Utils/Browser/sameOrigin/"
                },
                {
                    title: "解决Chrome94CORS跨域问题",
                    path: "/Utils/Browser/chrome94/"
                }
            ]
        },
        {
            title: "VsCode编译器",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "VsCode插件推荐",
                    path: "/Utils/VsCode/extension/"
                },
                {
                    title: "VsCode快捷键",
                    path: "/Utils/VsCode/hotKey/"
                },
                {
                    title: "VsCode中的一些实用操作",
                    path: "/Utils/VsCode/operation/"
                },
                {
                    title: "Setting.json文件配置",
                    path: "/Utils/VsCode/config/"
                }
            ]
        },
        {
            title: "Git",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "GitHub/GitLab 部署发布项目",
                    path: "/Utils/Git/publish/"
                }
            ]
        },
        {
            title: "NPM",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "node版本控制",
                    path: "/Utils/NPM/NodeUpgrade/"
                },
                {
                    title: "NRM工具的基本使用",
                    path: "/Utils/NPM/nrm/"
                },
                {
                    title: "开发中可能用到的Npm包",
                    path: "/Utils/NPM/NpmPackages/"
                }
            ]
        }
    ],
    "/Http/": [
        {
            title: "破冰篇",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "01 | 时势与英雄：HTTP的前世今生",
                    path: "/Http/01_Introduce/"
                },
                {
                    title: "02 | HTTP是什么？HTTP又不是什么？",
                    path: "/Http/02_WhatIsHttp/"
                },
                {
                    title: "03 | HTTP世界全览（上）：与HTTP相关的各种概念",
                    path: "/Http/03_HttpRelatedConcepts/"
                },
                {
                    title: "04 | HTTP世界全览（下）：与HTTP相关的各种协议",
                    path: "/Http/04_HttpRelatedProtocols/"
                },
                {
                    title: "05 | 常说的“四层”和“七层”到底是什么？“五层”“六层”哪去了？",
                    path: "/Http/05_Layer/"
                },
                {
                    title: "06 | 域名里有哪些门道？",
                    path: "/Http/06_Domain/"
                },
                {
                    title: "07 | 自己动手，搭建HTTP实验环境",
                    path: "/Http/07_ExperimentalEnvironment/"
                }
            ]
        },
        {
            title: "基础篇",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "08 | 键入网址再按下回车，后面究竟发生了什么？",
                    path: "/Http/08_Enter/"
                },
                {
                    title: "09 | HTTP报文是什么样子的？",
                    path: "/Http/09_HttpMessage/"
                },
                {
                    title: "10 | 应该如何理解请求方法？",
                    path: "/Http/10_RequestMethod/"
                },
                {
                    title: "11 | 你能写出正确的网址吗？",
                    path: "/Http/11_RightPath/"
                },
                {
                    title: "13 | HTTP有哪些特点？",
                    path: "/Http/13_Features/"
                },
                {
                    title: "14 | HTTP有哪些优点？又有哪些缺点？",
                    path: "/Http/14_AdvantagesDisadvantages/"
                }
            ]
        },
        {
            title: "进阶篇",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "15 | 海纳百川：HTTP的实体数据",
                    path: "/Http/15_EntityData/"
                },
                {
                    title: "16 | 把大象装进冰箱：HTTP传输大文件的方法",
                    path: "/Http/16_BigFile/"
                },
                {
                    title: "17 | 排队也要讲效率：HTTP的连接管理",
                    path: "/Http/17_ConnectionManagement/"
                },
                {
                    title: "18 | 四通八达：HTTP的重定向和跳转",
                    path: "/Http/18_HttpRedirect/"
                },
                {
                    title: "19 | 让我知道你是谁：HTTP的Cookie机制",
                    path: "/Http/19_HttpCookie/"
                },
                {
                    title: "20 | 生鲜速递：HTTP的缓存控制",
                    path: "/Http/20_CacheControl/"
                },
                {
                    title: "21 | 良心中间商：HTTP的代理服务",
                    path: "/Http/21_Proxy/"
                },
                {
                    title: "22 | 冷链周转：HTTP的缓存代理",
                    path: "/Http/22_CachingProxy/"
                }
            ]
        },
        {
            title: "安全篇",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "23 | HTTPS是什么？SSL/TLS又是什么？",
                    path: "/Http/23_Https/"
                },
                {
                    title: "24 | 固若金汤的根本（上）：对称加密与非对称加密",
                    path: "/Http/24_Encryption/"
                },
                {
                    title: "25 | 固若金汤的根本（下）：数字签名与证书",
                    path: "/Http/25_SignaturesCertificates/"
                }
            ]
        },
        {
            title: "飞翔篇",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "26 | 时代之风（上）：HTTP/2特性概览",
                    path: "/Http/26_Http2/"
                },
                {
                    title: "27 | 时代之风（下）：HTTP/2内核剖析",
                    path: "/Http/27_Http2Kernel/"
                },
                {
                    title: "28 | 未来之路：HTTP/3展望",
                    path: "/Http/28_Http3/"
                }
            ]
        },
        {
            title: "探索篇",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "29 | Nginx：高性能的Web服务器",
                    path: "/Http/29_Nginx/"
                },
                {
                    title: "30 | CDN：加速我们的网络服务",
                    path: "/Http/30_CDN/"
                },
                {
                    title: "31 | WebSocket：沙盒里的TCP",
                    path: "/Http/31_WebSocket/"
                }
            ]
        },
        {
            title: "总结篇",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "32 | HTTP性能优化面面观（上）",
                    path: "/Http/32_Summarize1/"
                },
                {
                    title: "33 | HTTP性能优化面面观（下）",
                    path: "/Http/33_Summarize2/"
                }
            ]
        }
    ],
    "/JS/": [
        {
            title: "简介",
            path: "/JS/01_Introduction/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "JavaScript 简介",
                    path: "/JS/01_Introduction/01_IntroductionToJavaScript/"
                },
                {
                    title: "代码编辑器",
                    path: "/JS/01_Introduction/02_CodeEditor/"
                },
                {
                    title: "开发者控制台",
                    path: "/JS/01_Introduction/03_DeveloperConsole/"
                }
            ]
        },
        {
            title: "JavaScript 基础知识",
            path: "/JS/02_BasicKnowledge/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "Hello, World!",
                    path: "/JS/02_BasicKnowledge/01_HelloWorld/"
                },
                {
                    title: "代码结构",
                    path: "/JS/02_BasicKnowledge/02_CodeStructure/"
                }
            ]
        },
        {
            title: "代码质量",
            path: "/JS/03_CodeQuality/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "在 Chrome 中调试",
                    path: "/JS/03_CodeQuality/01_Debugging/"
                }
            ]
        }
    ],
    "/Vue3/": [
        {
            title: "DevTools的使用",
            path: "/Vue3/01_Devtools/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "安装",
            path: "/Vue3/02_Install/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "介绍与基本使用",
            path: "/Vue3/03_BasicGrammar/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "组合式 API",
            path: "/Vue3/04_OptionsApi/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Vue3路由",
            path: "/Vue3/05_Router/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Vue3路由",
            path: "/Vue3/05_Router/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Vue3.2新特性",
            path: "/Vue3/06_SetupSyntacticSugar/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Vue3 VS Vue2",
            path: "/Vue3/07_Vue3VSvue2/",
            collapsable: false,
            sidebarDepth: 1
        }
    ],
    "/React/": [
        {
            title: "编写第一个React应用程序",
            path: "/React/03_FirstReactProject/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "JSX原理",
            path: "/React/04_JSX/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "React类组件",
            path: "/React/05_ClassComponent/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "函数式组件",
            path: "/React/06_FunctionComponent/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "组件的嵌套",
            path: "/React/07_ComponentNested/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "组件样式",
            path: "/React/08_ComponentStyle/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "React事件绑定",
            path: "/React/09_EventBinding/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Ref的应用",
            path: "/React/10_Ref/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Ref的应用",
            path: "/React/10_Ref/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "状态初识",
            path: "/React/11_StateExperience/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "循环渲染",
            path: "/React/12_For/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "TodoList案例",
            path: "/React/13_TodoList/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "条件渲染",
            path: "/React/14_If/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "dangerouslySetInnerHTML",
            path: "/React/15_dangerouslySetInnerHTML/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "状态再体验",
            path: "/React/16_State2/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "属性Props",
            path: "/React/17_Props/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "状态VS属性",
            path: "/React/18_StateVSProps/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "表单的受控与非受控",
            path: "/React/19_ControlledUncontrolled/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "父子组件通信",
            path: "/React/20_ComponentCommunication/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "案例：表单域组件",
            path: "/React/21_FormField/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "非父子组件传值",
            path: "/React/22_NoPCCommunication/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "插槽",
            path: "/React/23_Slot/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "生命周期",
            path: "/React/24_Life/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "hooks",
            path: "/React/25_hooks/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "React路由",
            path: "/React/26_ReactRouter/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "React反向代理",
            path: "/React/27_Proxy/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Css Module",
            path: "/React/28_CSSModule/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Flux与Redux介绍",
            path: "/React/29_FluxRedux/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Redux中间件",
            path: "/React/30_ReduxMiddleWare/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "immutable.js",
            path: "/React/31_Immutable/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "Mobx",
            path: "/React/32_Mobx/",
            collapsable: false,
            sidebarDepth: 1
        },
        {
            title: "ts",
            path: "/React/33_TS/",
            collapsable: false,
            sidebarDepth: 1
        },
    ]
}