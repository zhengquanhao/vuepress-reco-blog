module.exports = [
    //一键复制代码插件
    ['vuepress-plugin-code-copy', true],
    // 阅读进度条
    ["reading-progress", true],
    // 看板娘插件
    // [
    //   "@vuepress-reco/vuepress-plugin-kan-ban-niang",{
    //     theme: [
    //         "z16", "miku", "haru1", "haru2", "haruto", "koharu", "shizuku",
    //     ]
    //   }
    // ],
    // 音乐播放器 https://www.npmjs.com/package/@vuepress-reco/vuepress-plugin-bgm-player
    // [
    //     "@vuepress-reco/vuepress-plugin-bgm-player",{
    //       audios: [
    //         // 本地文件示例
    //         // {
    //         //   name: '夏の喚く',
    //         //   artist: '小小君',
    //         //   url: '/bgm/bgm.mp3',
    //         //   cover: '/bgm/cover.png'
    //         // },
    //         // 网络文件示例
    //         {
    //           name: '夏の喚く',
    //           artist: '小小君',
    //           url: 'https://zhengquanhao.gitlab.io/vuepress-reco-blog/assets/bgm/bgm.mp3',
    //           cover: 'https://zhengquanhao.gitlab.io/vuepress-reco-blog/assets/bgm/cover.png'
    //         }
    //       ],
    //       autoShrink: true,
    //       shrinkMode: "mini"
    //     }
    // ],
    // 鼠标点击特效
    [
        "cursor-effects", {
            size: 1,                    // size of the particle, default: 2
            // shape: ['circle'],  // shape of the particle, default: 'star'
            zIndex: 999999999           // z-index property of the canvas, default: 999999999
          },
    ],
    // 丝带
    ["ribbon-animation", {
        size: 90,   // 默认数据
        opacity: 0.3,  //  透明度
        zIndex: -1,   //  层级
        opt: {
            // 色带HSL饱和度
            colorSaturation: "80%",
            // 色带HSL亮度量
            colorBrightness: "60%",
            // 带状颜色不透明度
            colorAlpha: 0.65,
            // 在HSL颜色空间中循环显示颜色的速度有多快
            colorCycleSpeed: 6,
            // 从哪一侧开始Y轴 (top|min, middle|center, bottom|max, random)
            verticalPosition: "center",
            // 到达屏幕另一侧的速度有多快
            horizontalSpeed: 200,
            // 在任何给定时间，屏幕上会保留多少条带
            ribbonCount: 2,
            // 添加笔划以及色带填充颜色
            strokeSize: 0,
            // 通过页面滚动上的因子垂直移动色带
            parallaxAmount: -0.5,
            // 随着时间的推移，为每个功能区添加动画效果
            animateSections: true
        },
        ribbonShow: false, //  点击彩带  true显示  false为不显示
        ribbonAnimationShow: true  // 滑动彩带
    }],
    ["redirect", {
        redirectors: [
            // 定制化重定向
            {
              base: "/Vue3/", // 将 `/Vue3/` 自动重定向到某个子页面
              storage: true, // 保存最后一次访问的结果到 `localStorage`，供下次重定向使用
              alternative: [
                // 提供一个备选列表，如果都找不到就只能 404 Not Found 喽
                "01_Devtools", // 相当于 `/Vue3/01_Devtools/`
                "02_Install"
              ]
            },
        ],
    }]
]