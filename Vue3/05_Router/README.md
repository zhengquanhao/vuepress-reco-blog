---
title: 路由
date: 2022-01-18
tags:
 - Vue3.0
categories:
 - Vue3.0
---

## 一、安装

```shell
npm install vue-router@4
```

## 二、基本使用

### 2.1 定义路由

```javascript
import { createRouter, createWebHistory } from "vue-router";

// 定义路由组件
const Home = () => import("@/views/Home.vue");
const About = () => import("@/views/About.vue");

// 开启历史模式
// vue2中使用 mode: history 实现
// hash: createWebHashHistory()
const routerHistory = createWebHistory();

// 创建路由实例并定义路由
const router = createRouter({
    history: routerHistory,
    routes: [
        {
            path: "/",
            redirect: "/home"
        },
        {
            path: "/home",
            name: "home",
            component: Home
        },
        {
            path: "/about",
            name: "about",
            component: About
        }
    ]
});

// 导出
export default router;
```

### 2.2 挂载路由

```javascript
// main.js
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp(App)
app.use(router)
app.mount('#app')
```

### 2.3 页面跳转

```vue
<template>
    <div id="app">
        <router-link to="/home">Home页面</router-link>
        <router-link to="/about">About页面</router-link>
        <router-view />
    </div>
</template>
```

## 三、动态路由

### 3.1 动态路由说明

很多时候，我们需要将给定匹配模式的路由映射到同一个组件。例如，我们可能有一个 `User` 组件，它应该对所有用户进行渲染，但用户 ID 不同。在 Vue Router 中，我们可以在路径中使用一个动态字段来实现，我们称之为 路径参数：

```javascript
const User = () => import("@/views/Users.vue");

// 这些都会传递给 `createRouter`
const routes = [
    // 动态字段以冒号开始
    {
        path: '/users/:id',
        name: "users",
        component: User
    }
]
```

现在像 `/users/johnny` 和 `/users/jolyne` 这样的 URL 都会映射到同一个路由。

路径参数 用冒号 `:` 表示。当一个路由被匹配时，它的 params 的值将在每个组件中以 `this.$route.params` 的形式暴露出来。因此，我们可以通过更新 `User` 的模板来呈现当前的用户 ID。

### 3.2 响应路由参数的变化

使用带有参数的路由时需要注意的是，当用户从 `/users/johnny` 导航到 `/users/jolyne` 时，**相同的组件实例将被重复使用**。因为两个路由都渲染同个组件，比起销毁再创建，复用则显得更加高效。**不过，这也意味着组件的生命周期钩子不会被调用。**

使用 `beforeRouteUpdate` 导航守卫进行解决：

```javascript
beforeRouteUpdate (from, to, next) {
    console.log("from", from);
    console.log("to", to);
    console.log(to.params.id);
    next();
}
```

## 四、路由的匹配语法

### 4.1 在参数中最定义正则

```javascript
const router = createRouter({
    history: routerHistory,
    routes: [
        {
            path: "/article/:id(\\d+)",
            component: () => import("@/views/Article.vue")
        }
    ]
})
```

### 4.2 可重复参数/可选参数

```javascript
const router = createRouter({
    history: routerHistory,
    routes: [
        // ‘+’（至少一个参数） ’*‘（0到任意个）  ’？‘（0次或一次） 规则与正则一致
        {
            path: "/news/:id+",
            component: () => import("@/views/News.vue")
        }
    ]
})
```

## 五、配置错误页

```javascript
const router = createRouter({
    history: routerHistory,
    routes: [
        // 不在路由列表的路径返回错误页
        {
            path: "/:path(.*)",
            component: () => import("@/views/NotFound.vue")
        }
    ]
})
```

## 六、路由嵌套

```javascript
const router = createRouter({
    history: routerHistory,
    routes: [
        // 嵌套路由
        {
            path: "/user",
            component: () => import("@/views/user/User.vue"),
            children: [
                {
                    path: "userInfo", // 前面不需要加’/‘
                    component: () => import("@/views/user/UserInfo.vue")
                },
                {
                    path: "userLogin", // 前面不需要加’/‘
                    component: () => import("@/views/user/userLogin.vue")
                }
            ]
        }
    ]
})
```

创建`User.vue`写入`User Page`，`UserInfo.vue`文件写入`User Info Page`，`UserLogin.vue`文件写入`User Login Page`。

地址输入`/user`显示`User Page`，地址输入`/user/userInfo`显示`User Page User Info Page`，地址输入`/user/userLogin`显示`User Page User Login Page`。

## 七、编程式导航

### 7.1 导航到不同的位置

想要导航到不同的 URL，可以使用 `router.push` 方法。这个方法会向 history 栈添加一个新的记录，所以，当用户点击浏览器后退按钮时，会回到之前的 URL。

当你点击 `<router-link>` 时，内部会调用这个方法，所以点击 `<router-link :to="...">` 相当于调用 `router.push(...)`。

| 声明式 | 编程式 |
| --- | --- |
| `<router-link :to="...">` | `router.push(...)` |

该方法的参数可以是一个字符串路径，或者一个描述地址的对象。例如：

```javascript
// 字符串路径
router.push('/users/eduardo')

// 带有路径的对象
router.push({ path: '/users/eduardo' })

// 命名的路由，并加上参数，让路由建立 url
router.push({ name: 'user', params: { username: 'eduardo' } })

// 带查询参数，结果是 /register?plan=private
router.push({ path: '/register', query: { plan: 'private' } })

// 带 hash，结果是 /about#team
router.push({ path: '/about', hash: '#team' })
```

> **注意：** 如果提供了 `path`，`params` 会被忽略

### 7.2 替换当前位置

| 声明式 | 编程式 |
| --- | --- |
| `<router-link :to="..." replace>` | `router.replace(...)` |

也可以直接在传递给 router.push 的 routeLocation 中增加一个属性 replace: true ：

```javascript
router.push({ path: '/home', replace: true })
// 相当于
router.replace({ path: '/home' })
```

### 7.3 横跨历史

该方法采用一个整数作为参数，表示在历史堆栈中前进或后退多少步，类似于 `window.history.go(n)`。

```javascript
// 向前移动一条记录，与 router.forward() 相同
router.go(1)

// 返回一条记录，与router.back() 相同
router.go(-1)

// 前进 3 条记录
router.go(3)

// 如果没有那么多记录，静默失败
router.go(-100)
router.go(100)
```

### 7.4 简单使用的举例

```html
<template>
    <div class="about">
        <p>About</p>
        <button @click="toHome">toHome</button>
    </div>
</template>
<script>
import { useRouter } from "vue-router";
export default {
    setup () {
        const router = useRouter();
        const toHome = () => {
            router.push({
                name: "home"
            });
        };
        return {
            toHome
        };
    }
};
</script>
```

### 八、命名路由

除了 `path` 之外，你还可以为任何路由提供 `name`。这有以下优点：

- 没有硬编码的 `URL`
- `params` 的自动编码/解码。
- 防止你在 `url` 中出现打字错误。
- 绕过路径排序（如显示一个）

```javascript
const routes = [
  {
    path: '/user/:username',
    name: 'user',
    component: User
  }
]
```

要链接到一个命名的路由，可以向 `router-link` 组件的 `to` 属性传递一个对象：

```html
<router-link :to="{ name: 'user', params: { username: 'ron' }}">
  User
</router-link>
```

这跟代码调用 `router.push()` 是一回事：

```javascript
router.push({ name: 'user', params: { username: 'ron' } })
```

在这两种情况下，路由将导航到路径 /user/ron。

