---
title: 安装
date: 2022-01-12
tags:
 - Vue3.0
categories:
 - Vue3.0
---

## CDN方式

通过CDN方式创建vue3应用的实例

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>cdn方式</title>
    <head>
        <script src="https://unpkg.com/vue@next"></script>
    </head>
</head>
<body>
    <div id="container">{{msg}}</div>

    <script>
        const msg = {
            data () {
                return {
                    msg: "hello vue3"
                }
            }
        }

        const app = Vue.createApp(msg).mount("#container");
    </script>
</body>
</html>
```

在浏览器打开上述页面，控制台输入`app`, 会返回一个`proxy`, 输入`app.msg="hello world"`会发现数据双向绑定界面内容从`hello vue3`显示成`hello world`。

## 使用构建工具创建

### 使用vue-cli创建

```shell
# 查看@vue/cli版本，确保@vue/cli版本在4.5.0以上方可安装vue3
vue --version

# 安装或升级你的@vue/cli
npm install -g @vue/cli

# 创建
vue create vue3-project

# 启动
cd vue3-project
npm run serve
```

### 使用vite创建

```shell
# 使用`Vite` 需要 `Node.js` 版本 `>= 12.0.0`

# 查看Node版本指令
node -v
node --version

# 创建
npm init vite@latest

# 启动
cd vite-project
npm i
npm run dev
```
