---
title: Vue3项目`DevTools`不生效问题
date: 2021-12-03
tags:
 - Vue3.0
categories:
 - Vue3.0
---

vue3项目`DevTools`不生效问题
---

聪明的小伙伴都会想到是版本的问题，于是乎打开谷歌商店，搜了一下，出现了这两个`devtools`工具。

![devtools](./images/devtools.png)

选择上边beta版安装就可以了，但需要`重启浏览器`哦。

