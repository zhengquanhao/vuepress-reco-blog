---
title: 组合式 API
date: 2022-01-14
tags:
 - Vue3.0
categories:
 - Vue3.0
---

## 为什么改用组合式 API？

使用Vue2方式书写的情况下，当我们的组件开始变得更大时，逻辑关注点的列表也会增长。尤其对于那些一开始没有编写这些组件的人来说，这会导致组件难以阅读和理解。

![options-api](./images/options-api.png)

这种碎片化使得理解和维护复杂组件变得困难。选项的分离掩盖了潜在的逻辑问题。此外，在处理单个逻辑关注点时，我们必须不断地“跳转”相关代码的选项块。

如果能够将同一个逻辑关注点相关代码收集在一起会更好。而这正是组合式 API 使我们能够做到的。

## 组合式 API 基础

既然我们知道了**为什么**，我们就可以知道**怎么做**。为了开始使用组合式 API，我们首先需要一个可以实际使用它的地方。在 Vue 组件中，我们将此位置称为 `setup`。

## setup 组件选项

新的 `setup` 选项在组件**创建之前**执行，一旦 `props` 被解析，就将作为组合式 API 的入口。

```shell
# 打印顺序
setup
beforeCreate: 初始化数据之前
data
created: 数据初始化之后
beforeMounte: 挂载渲染之前
mounted: 挂载组件之后
```

> 在 `setup` 中你应该避免使用 `this`，因为它不会找到组件实例。`setup` 的调用发生在 `data` property、`computed` property 或 `methods` 被解析之前，所以它们无法在 `setup` 中被获取。

`setup` 选项是一个接收 `props` 和 `context` 的函数，`props` 是父组件传给子组件的属性（同Vue2的props）可以作为 `setup` 的第一个参数直接获取，`context` 是父组件传给子组件的内容，我们之后具体讨论。

此外，我们将 `setup` 返回的所有内容都暴露给组件的其余部分(包含页面)供其使用。

```javascript
export default {
  props: {
    user: {
      type: String,
      required: true
    }
  },
  setup(props) {
    console.log(props) // { user: '' }

    return {} // 这里返回的任何内容都可以用于组件的其余部分
  }
  // 组件的“其余部分”
}
```

## 带 ref 的响应式变量

在 Vue 3.0 中，我们可以通过一个新的 `ref` 函数使一个变量变为任何响应式变量，如下所示：

```vue
<template>
    <div class="container">
        {{ num }}
    </div>
</template>

<script>
import { ref } from "vue"

export default {
    setup () {
        const num = ref(0);
        return {
            num
        }
    }

}
</script>
```

`ref` 接收参数并将其包裹在一个带有 `value` property 的对象中返回，然后可以使用该 property 访问或更改响应式变量的值：

如下案例：我们创建了一个响应式变量 `num`，点击按钮进行 `num + 1`。

```vue
<template>
    <div class="container">
        {{ num }}
        <button @click="changeNum">点击</button>
    </div>
</template>

<script>
import { ref } from "vue"

export default {
    setup () {
        const num = ref(0);

        const changeNum = () => {
            console.log(num); // { value: 0 }
            console.log(num.value); // 0
            num.value++;
        }

        return {
            num,
            changeNum
        }
    }

}
</script>
```

> vue3帮我们处理了在插值表达式里直接写 `num` 就可以了，而不用写 `num.value`。

## 带 reactive 的响应式对象

知道了使用 `ref` 创建一个基本类型的响应式数据，那如何创建响应式的对象数据呢？这就需要用到 `reactive`。

如下案例：创建了一个响应式对象 `user`，点击按钮将 `user.name` 从 `Ron` 修改为 `Ron!`。

```vue
<template>
    <div class="container">
        {{ user.name }}
        {{ user.age }}
        <button @click="changeUserInfo">点击</button>
    </div>
</template>

<script>
import { reactive } from "vue";

export default {
    setup () {

        const user = reactive(
            {
                name: "Ron",
                age: 18
            }
        )

        const changeUserInfo = () => {
            user.name += "!";
            console.log(user.name);
        }

        return {
            user,
            changeUserInfo
        }
    }

}
</script>
```

如果你觉得每次使用对象都要用 `user.**` 很麻烦，可以使用`toRefs`进行解构。

```vue
<template>
    <div class="container">
        {{ name }}
        {{ age }}
    </div>
</template>

<script>
import { reactive, toRefs } from "vue";

export default {
    setup () {

        const user = reactive(
            {
                name: "Ron",
                age: 18
            }
        )

        return {
            ...toRefs(user)
        }
    }

}
</script>
```

## Vue3生命周期钩子函数

为了使组合式 API 的功能和选项式 API 一样完整，我们还需要一种在 `setup` 中注册生命周期钩子的方法。这要归功于 Vue 导出的几个新函数。组合式 API 上的生命周期钩子与选项式 API 的名称相同，但前缀为 `on`：即 mounted 看起来会像 `onMounted`。

因为 `setup` 函数本就在 `created` 前执行，vue3中将`beforeCreate`，`created`函数废弃。将`beforeDestroy`，`destroyed`更名为`beforeUnmount`，`unmounted`。

```vue
<template>
    <div class="container">
        {{ num }}
        <button @click="changeNum">点击</button>
    </div>
</template>

<script>
import { onBeforeMount, onBeforeUnmount, onBeforeUpdate, onMounted, onUnmounted, onUpdated, ref} from "vue";

export default {
    setup () {
        const num = ref(0);

        const changeNum = () => {
            num.value++;
        }


        onBeforeMount(() => {
            console.log("onBeforeMount");
        })

        onMounted(()=> {
            console.log("onMounted");
        })

        onBeforeUpdate(() => {
            console.log("onBeforeUpdate");
        })

        onUpdated(() => {
            console.log("onUpdated");
        })

        onBeforeUnmount(() => {
            console.log("onBeforeUnmount");
        })

        onUnmounted(() => {
            console.log("onUnmounted");
        })

        return {
            num,
            changeNum
        }
    }

}
</script>
```

## 计算属性

```vue
<template>
    <div class="container">
        {{ twiceTheNum }}
    </div>
</template>

<script>
import { ref, computed } from "vue";

export default {
    setup () {
        const num = ref(5);

        const twiceTheNum = computed(() => num.value * 2); // 箭头函数省略了return 10

        return {
            num,
            twiceTheNum
        }
    }

}
</script>
```

## watch监听

### 单个监听

```vue
<template>
    <div class="container">
        {{ num }}
        <button @click="changeNum">点击</button>
    </div>
</template>

<script>
import { ref, watch } from "vue";

export default {
    setup () {
        const num = ref(5);

        const changeNum = () => {
            num.value++;
        }

        watch(num, (newVal, oldVal) => {
            console.log("newVal", newVal);
            console.log("oldVal", oldVal);
            console.log("num数值改变了");
        })

        return {
            num,
            changeNum
        }
    }
}
</script>
```

### 多个监听

```vue
<template>
    <div class="container">
        {{ num }}
        {{ name }}
        {{ age }}
        <button @click="changeInfo">点击</button>
    </div>
</template>

<script>
import { reactive, ref, toRefs, watch } from "vue";

export default {
    setup () {
        const num = ref(5);
        const user = reactive({
            name: "Ron",
            age: 18
        })

        const changeInfo = () => {
            num.value++;
            user.name = "Ron!";
        }

        watch([num, user], (newVal, oldVal) => {
            console.log("newVal", newVal); // 这里newVal的值为一个数组
            console.log("oldVal", oldVal); // 这里oldVal的值为一个数组
            console.log("监听到了");
        })

        return {
            num,
            changeInfo,
            ...toRefs(user)
        }
    }
}
</script>
```

### 自动监听

```vue
<template>
    <div class="container">
        {{ num }}
        {{ name }}
        {{ age }}
        <button @click="changeInfo">点击</button>
    </div>
</template>

<script>
import { reactive, ref, toRefs, watchEffect } from "vue";

export default {
    setup () {
        const num = ref(5);
        const user = reactive({
            name: "Ron",
            age: 18
        })

        const changeInfo = () => {
            num.value++;
            user.name += "!";
        }

        watchEffect(() => {
            console.log("num", num.value); // 自动监听num值是否有所改变，监听基本数据类型需要.value
            console.log("userInfo", user.name); // 自动监听user.name值是否有所改变
            console.log("监听到了");
        })

        return {
            num,
            changeInfo,
            ...toRefs(user)
        }
    }
}
</script>
```

## watch 与 watchEffect 的区别

### watch函数

- 与watch配置功能一致
- 监视指定的一个或多个响应式数据, 一旦数据变化, 就自动执行监视回调
- 默认初始时不执行回调, 但可以通过配置immediate为true, 来指定初始时立即执行第一次
- 通过配置deep为true, 来指定深度监视

### watchEffect函数

- 不用直接指定要监视的数据, 回调函数中使用的哪些响应式数据就监视哪些响应式数据
- 默认初始时就会执行第一次, 从而可以收集需要监视的数据
- 监视数据发生变化时回调

## 组件

父组件：

```vue
<template>
    <div class="container">
        <User :name="name" :age="age"></User>
    </div>
</template>

<script>
import User from "./components/User.vue"
import { reactive, toRefs } from "vue";

export default {
    components: {
        User
    },
    setup () {
        const user = reactive({
            name: "Ron",
            age: 18
        })

        return {
            ...toRefs(user)
        }
    }
}
</script>
```

子组件：

```vue
<template>
    <div class="user">
        name: {{ name }}
        age: {{ age }}
    </div>
</template>

<script>
export default {
    setup(props, context) {
        console.log("props", props);
        console.log("context", context);
    },
    props: {
        name: {
            type: String,
            default: ""
        },
        age: {
            type: Number
        }
    }
}
</script>
```

### Props 参数

`setup` 函数中的第一个参数是 `props`。正如在一个标准组件中所期望的那样，`setup` 函数中的 `props` 是响应式的，当传入新的 `prop` 时，它将被更新。

```javascript
setup(props, context) {
    console.log("props", props); // { name: "Ron", age: 18}
}
```

### Context 参数

传递给 `setup` 函数的第二个参数是 `context`。`context` 是一个普通 JavaScript 对象，暴露了其它可能在 `setup` 中有用的值：

```javascript
setup(props, context) {
    // Attribute (非响应式对象，等同于 $attrs)
    console.log(context.attrs)

    // 插槽 (非响应式对象，等同于 $slots)
    console.log(context.slots)

    // 触发事件 (方法，等同于 $emit)
    console.log(context.emit)

    // 暴露公共 property (函数)
    console.log(context.expose)
}
```

## Provide / Inject

### 设想场景

假设我们要重写以下代码，其中包含一个 `MyMap` 组件，该组件使用组合式 API 为 `MyMarker` 组件提供用户的位置。

vue2 写法:

```vue
<!-- src/components/MyMap.vue -->
<template>
  <MyMarker />
</template>

<script>
import MyMarker from './MyMarker.vue'

export default {
  components: {
    MyMarker
  },
  provide: {
    location: 'North Pole',
    geolocation: {
      longitude: 90,
      latitude: 135
    }
  }
}
</script>
```

```vue
<!-- src/components/MyMarker.vue -->
<template>
    <div class="container">
        {{location}}---{{geolocation}}
    </div>
</template>
<script>
export default {
  inject: ['location', 'geolocation']
}
</script>
```

展示效果：

```shell
North Pole---{ "longitude": 90, "latitude": 135 }
```

### 组合式 API 使用 Provide

在 `setup()` 中使用 `provide` 时，我们首先从 vue 显式导入 `provide` 方法。这使我们能够调用 `provide` 来定义每个 `property`。

`provide` 函数允许你通过两个参数定义 `property`：

1. name (`<String>` 类型)
2. value

使用 `MyMap` 组件后，provide 的值可以按如下方式重构：

```vue
<!-- src/components/MyMap.vue -->
<template>
  <MyMarker />
</template>

<script>
import { provide } from 'vue'
import MyMarker from './MyMarker.vue'

export default {
  components: {
    MyMarker
  },
  setup() {
    provide('location', 'North Pole')
    provide('geolocation', {
      longitude: 90,
      latitude: 135
    })
  }
}
</script>
```

### 组合式 API 使用 inject

在 `setup()` 中使用 `inject` 时，也需要从 `vue` 显式导入。导入以后，我们就可以调用它来定义暴露给我们的组件方式。

`inject` 函数有两个参数：

1. 要 `inject` 的 property 的 `name`
2. 默认值 (可选)

```vue
<!-- src/components/MyMarker.vue -->
<template>
  <div class="container">
    {{userLocation}}---{{userGeolocation}}
  </div>
</template>
<script>
import { inject } from "vue";
export default {
  setup() {
    // 需要定义变量接收并暴露
    const userLocation = inject("location", "The Universe");
    const userGeolocation = inject("geolocation");

    return {
      userLocation,
      userGeolocation,
    };
  },
};
</script>
```

### 添加响应性

```vue
<!-- src/components/MyMap.vue -->
<template>
    <div class="container">
        <MyMarker />
        <button @click="changeLocation">点击</button>
    </div>
</template>

<script>
import { provide, ref, reactive } from "vue";
import MyMarker from "./MyMarker.vue";

export default {
  components: {
    MyMarker
  },
  setup () {
        const location = ref("North Pole");
        const geolocation = reactive({
            longitude: 90,
            latitude: 135
        });
        provide("location", location);
        provide("geolocation", geolocation);
        const changeLocation = () => {
            location.value = "South Pole";
        }
        return {
            location,
            geolocation,
            changeLocation
        }
  }
}
</script>
```

最后，如果要确保通过 `provide` 传递的数据不会被 `inject` 的组件更改，我们建议对提供者的 property 使用 `readonly`。

```vue
<!-- src/components/MyMap.vue -->
<template>
  <MyMarker />
</template>

<script>
import { provide, reactive, readonly, ref } from 'vue'
import MyMarker from './MyMarker.vue'

export default {
  components: {
    MyMarker
  },
  setup() {
    const location = ref('North Pole')
    const geolocation = reactive({
      longitude: 90,
      latitude: 135
    })

    const updateLocation = () => {
      location.value = 'South Pole'
    }

    provide('location', readonly(location))
    provide('geolocation', readonly(geolocation))
    provide('updateLocation', updateLocation)
  }
}
</script>
```

