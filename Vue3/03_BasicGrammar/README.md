---
title: 介绍与基本使用
date: 2022-01-13
tags:
 - Vue3.0
categories:
 - Vue3.0
---

## 声明式渲染

在我们没有学习Vue之前更改界面的内容都是通过DOM`命令式`来实现，而 Vue.js 的核心是一个允许采用简洁的模板语法来`声明式`地将数据渲染进 DOM 的系统。并且现在数据和 DOM 已经被建立了关联，所有东西都是响应式的。这也是Vue 在背后做的工作。

```html
<template>
    <h1>{{msg}}</h1>
</template>

<script>
// 命令式
// document.querySelector("h1").innerText = "hello vue3";

// 声明式
export default {
    name: "App",
    data () {
        return {
            msg: "hello vue3"
        }
    }
}
</script>
```

## 模板语法中的常用指令

### v-once指令: 使得内容只渲染一次

如下方案例，点击按钮，上方 msg 改变为`hello world`，下方不变。

```vue
<template>
    <div class="container">
        <h1>{{ msg }}</h1>
        <h1 v-once>{{ msg }}</h1>
        <button @click="changeMsg">更改msg</button>
    </div>
</template>

<script>
export default {
    data () {
        return {
            msg: "hello vue3"
        }
    },
    methods: {
        changeMsg () {
            this.msg = "hello world";
        }
    }
}
</script>
```

### v-html指令: 使得内容插入html代码

如下方案例，界面内容渲染为 `h1` 标签格式的`hello vue3`。

```vue
<template>
    <div v-html="content"></div>
</template>

<script>
export default {
    data () {
        return {
            content: "<h1>hello vue3</h1>"
        }
    }
}
</script>
```

### v-bind指令: 绑定属性的内容

如下方案例，点击按钮颜色由粉色切换为天空色。

```vue
<template>
    <div class="container">
        <div :id="id"></div>
        <button @click="toggle">点击</button>
    </div>
</template>

<script>
export default {
    data () {
        return {
            id: "id1"
        }
    },
    methods: {
        toggle () {
            this.id = "id2";
        }
    }
}
</script>

<style>
#id1 {
    width: 100px;
    height: 100px;
    background-color: pink;
}
#id2 {
    width: 100px;
    height: 100px;
    background-color: skyblue;
}
</style>
```

## 动态指令

刚刚了解到v-bind绑定的属性值可以是一个变量，同样属性也可以为一个变量。

如下方案例，初始状态盒子粉色点击时触发弹窗。点击切换按钮后，盒子变为蓝色，鼠标经过出弹窗。

```vue


<template>
    <div class="container">
        <div :[attributeName]="attributeValue"></div>
        <button @[eventName]="showDialog">展示弹窗</button>
        <button @click="toggle">切换</button>
    </div>
</template>

<script>
export default {
    data () {
        return {
            attributeName: "id",
            attributeValue: "box",
            eventName: "click"
        }
    },
    methods: {
        showDialog() {
            alert("hello vue3");
        },
        toggle () {
            this.attributeName = "class",
            this.eventName = "mouseover";
        }
    }
}
</script>

<style>
#box {
    width: 100px;
    height: 100px;
    background-color: pink;
}
.box {
    width: 100px;
    height: 100px;
    background-color: skyblue;
}
</style>
```

## 模板语法中使用JavaScript表达式

```vue
<template>
    <div class="container">
        <!-- JavaScript表达式可以在模板语法中使用 -->
        <p>{{ msg.split("") }}</p>
        <p>{{ msg.split("").reverse() }}</p>
        <p>{{ msg.split("").reverse().join("") }}</p>
        <p>{{ toggle ? "是" : "否" }}</p>
    </div>
</template>

<script>
export default {
    data () {
        return {
            msg: "hello vue3",
            toggle: false
        }
    }
}
</script>
```

展示结果：

```shell
[ "h", "e", "l", "l", "o", " ", "v", "u", "e", "3" ]
[ "3", "e", "u", "v", " ", "o", "l", "l", "e", "h" ]
3euv olleh
否
```

## 计算属性

模板内的表达式非常便利，但是设计它们的初衷是用于简单运算的。在模板中放入太多的逻辑会让模板过重且难以维护。所以，对于任何包含响应式数据的复杂逻辑，我们应该使用计算属性。

上述案例可用计算属性优化为：

```vue
<template>
    <p>{{ reverseMsg }}</p>
</template>

<script>
export default {
    data () {
        return {
            msg: "hello vue3"
        }
    },
    computed: {
        reverseMsg () {
            return this.msg.split("").reverse().join("");
        }
    }
}
</script>
```

## 监听

案例：监听msg的内容长度变化，长度超出10时打印文案。

```vue
<template>
    <input type="text" v-model="msg">
</template>

<script>
export default {
    data () {
        return {
            msg: ""
        }
    },
    watch: {
        msg(newVal, oldVal) {
            console.log(newVal); // 最新的值
            console.log(oldVal); // 之前的值
            if (newVal.length > 10) {
                console.log("长度超出10");
            }
        }
    }
}
</script>
```

## 绑定class

```vue
<template>
    <div class="container">
        <!-- 类 class -->
        <!-- 第一种写法，放置字符串 -->
        <p class="static">hello1</p>
        <!-- 第二种写法，放置对象 -->
        <p :class="{ active: isActive }">hello2</p>
        <!-- 第三种写法，放置数组 -->
        <p :class="['static', 'active']">hello3</p>
        <!-- 第四种写法，放置数组和对象结合 -->
        <p :class="['static', { active: true }]">hello4</p>
    </div>
</template>

<script>
export default {
    data () {
        return {
            isActive: true
        }
    }
}
</script>

<style>
.static {
    font-size: 20px;
}
.active {
    color: pink;
}
</style>
```

效果展示：

![bind-class](./images/bind-class.png)

## 绑定style

```vue
<template>
    <div class="container">
        <!-- style -->
        <!-- 第一种写法，放置字符串 -->
        <p style="font-size: 20px">hello1</p>
        <p :style="fontSize20">hello2</p>
        <!-- 第二种写法，放置对象 -->
        <p :style="{ background: 'pink' }">hello3</p>
        <p :style="{ backgroundColor: 'pink', fontSize: '20px' }">hello4</p>
        <p :style="styleObj">hello5</p>
        <!-- 第三种写法，放置数组，数组中包含多个对象 -->
        <p :style="[styleObj, { width: '200px' }]">hello6</p>
    </div>
</template>

<script>
export default {
    data () {
        return {
            fontSize20: "font-size: 20px",
            styleObj: {
                backgroundColor: 'pink',
                fontSize: '20px'
            }
        }
    }
}
</script>
```

效果展示：

![bind-style](./images/bind-style.png)

> 注意：使用style绑定时，CSS的属性名要使用字符串形式或驼峰命名。

## 条件渲染

如下案例，页面显示Vue3，不显示show。

```vue
<template>
    <div class="container">
        <!-- v-if 条件渲染 -->
        <p v-if="version === 1">Vue1</p>
        <p v-else-if="version === 2">Vue2</p>
        <p v-else-if="version === 3">Vue3</p>
        <p v-else>!Vue</p>
        <!-- v-show 条件渲染 -->
        <p v-show="toggle">show</p>
    </div>
</template>

<script>
export default {
    data () {
        return {
            version: 3,
            toggle: false
        }
    }
}
</script>
```

## 循环渲染

### 循环数组：

```vue
<template>
    <ul>
        <li v-for="(item, i) in newsList" :key="i">{{ item }}</li>
    </ul>
</template>

<script>
export default {
    data () {
        return {
            newsList: [
                "十九届六中全会决议中的10个明确",
                "延误救治 西安2家医院停业3个月",
                "31省份新增本土确诊124例 天津41例",
                "河南新增76例本土确诊 安阳44例"
            ]
        }
    }
}
</script>
```

### 循环对象数组：

```vue
<template>
    <ul>
        <li v-for="(item, i) in newsList" :key="i">{{ item.title }}--{{ item.date }}</li>
    </ul>
</template>

<script>
export default {
    data () {
        return {
            newsList: [
                {
                    title: "十九届六中全会决议中的10个明确",
                    date: "2022-01-13"
                },
                {
                    title: "延误救治 西安2家医院停业3个月",
                    date: "2022-01-12"
                },
                {
                    title: "31省份新增本土确诊124例 天津41例",
                    date: "2022-01-12"
                },
                {
                    title: "河南新增76例本土确诊 安阳44例",
                    date: "2022-01-13"
                }
            ]
        }
    }
}
</script>
```

### 循环对象

```vue
<template>
    <ul>
        <!-- item: 属性值， key: 属性名， i: 索引 -->
        <li v-for="(item, key, i) in newsObj" :key="i">{{ item }}--{{ key }}---{{i}}</li>
    </ul>
</template>

<script>
export default {
    data () {
        return {
            newsObj: {
                title: "十九届六中全会决议中的10个明确",
                date: "2022-01-13"
            }
        }
    }
}
</script>
```

效果展示：

![for-obj](./images/for-obj.png)

## 事件处理

```vue
<template>
    <div class="container">
        <!-- 绑定事件，不需要参数 -->
        <p @click="addEvent">{{ num }}</p>
        <!-- 绑定事件，直接处理表达式 -->
        <p @click="num += 2">{{ num }}</p>
        <!-- 绑定事件，传递参数 -->
        <p @click="addNum(10)">{{ num }}</p>
        <!-- 绑定事件传递事件对象和参数 -->
        <p @click="addNum1(20, $event)">{{ num }}</p>
        <!-- 绑定事件多个处理函数 -->
        <p @click="addNum(10), showDialog()">{{ num }}</p>
    </div>
</template>

<script>
export default {
    data () {
        return {
            num: 0
        }
    },
    methods: {
        addEvent(e) {
            // 获取事件对象
            console.log(e);
            this.num += 2;
        },
        addNum(val) {
            this.num += val;
        },
        addNum1(val, e) {
            // 获取事件对象
            console.log(e);
            this.num += val;
        },
        showDialog() {
            alert("hello");
        }
    }
}
</script>
```

### 事件修饰符

https://juejin.cn/post/6844903854035730439

### 按键修饰符

Vue 为最常用的键提供了别名：

- `.enter`
- `.tab`
- `.delete (捕获“删除”和“退格”键`)`
- `.esc`
- `.space`
- `.up`
- `.down`
- `.left`
- `.right`

### 系统修饰符

可以用如下修饰符来实现仅在按下相应按键时才触发鼠标或键盘事件的监听器。

- `.ctrl`
- `.alt`
- `.shift`
- `.meta`

## 表单输入绑定

### 基础用法
#### 文本 (Text)

```html
<input v-model="message" placeholder="edit me" />
<p>Message is: {{ message }}</p>
```

#### 多行文本 (Textarea)

```html
<textarea v-model="message" placeholder="add multiple lines"></textarea>
<p>Message is: {{ message }}</p>
```

#### 复选框 (Checkbox)

单个复选框，绑定到布尔值：

```vue
<template>
    <div class="container">
        <input type="checkbox" id="checkbox" v-model="checked" />
        <label for="checkbox">{{ checked }}</label>
    </div>
</template>

<script>
export default {
    data () {
        return {
            checked: false
        }
    }
}
</script>
```

多个复选框，绑定到同一个数组：

```vue
<template>
    <div id="v-model-multiple-checkboxes">
        <input type="checkbox" id="jack" value="Jack" v-model="checkedNames" />
        <label for="jack">Jack</label>
        <input type="checkbox" id="john" value="John" v-model="checkedNames" />
        <label for="john">John</label>
        <input type="checkbox" id="mike" value="Mike" v-model="checkedNames" />
        <label for="mike">Mike</label>
        <br />
        <span>Checked names: {{ checkedNames }}</span>
    </div>
</template>

<script>
export default {
    data () {
        return {
            checkedNames: []
        }
    }
}
</script>
```

#### 单选框 (Radio)

```vue
<template>
    <div id="v-model-radiobutton">
        <input type="radio" id="one" value="1" v-model="picked" />
        <label for="one">One</label>
        <br />
        <input type="radio" id="two" value="2" v-model="picked" />
        <label for="two">Two</label>
        <br />
        <span>Picked: {{ picked }}</span>
    </div>
</template>

<script>
export default {
    data () {
        return {
            picked: ""
        }
    }
}
</script>
```

#### 选择框 (Select)

单选时：

```vue
<template>
    <div id="v-model-select" class="demo">
        <select v-model="selected">
            <option disabled value="">Please select one</option>
            <option>A</option>
            <option>B</option>
            <option>C</option>
        </select>
        <span>Selected: {{ selected }}</span>
    </div>
</template>

<script>
export default {
    data () {
        return {
            selected: ""
        }
    }
}
</script>
```

多选时 (绑定到一个数组)：

```vue
<template>
    <div id="v-model-select" class="demo">
        <select v-model="selected" multiple>
            <option>A</option>
            <option>B</option>
            <option>C</option>
        </select>
        <br />
        <span>Selected: {{ selected }}</span>
    </div>
</template>

<script>
export default {
    data () {
        return {
            selected: ""
        }
    }
}
</script>
```

### 值绑定

对于单选按钮，复选框及选择框的选项，v-model 绑定的值通常是静态字符串 (对于复选框也可以是布尔值)：

```html
<!-- 当选中时，`picked` 为字符串 "a" -->
<input type="radio" v-model="picked" value="a" />

<!-- `toggle` 为 true 或 false -->
<input type="checkbox" v-model="toggle" />

<!-- 当选中第一个选项时，`selected` 为字符串 "abc" -->
<select v-model="selected">
  <option value="abc">ABC</option>
</select>
```

但是有时我们可能想把值绑定到当前活动实例的一个动态 property 上，这时可以用 v-bind 实现，此外，使用 v-bind 可以将输入值绑定到非字符串。

#### 复选框 (Checkbox)

```html
<input type="checkbox" v-model="toggle" true-value="yes" false-value="no" />
```

```javascript
// 当选中时：
vm.toggle === 'yes'
// 当未选中时：
vm.toggle === 'no'
```

#### 单选框 (Radio)

```html
<input type="radio" v-model="pick" v-bind:value="a" />
```

```javascript
// 当选中时
vm.pick === vm.a
```

### 选择框选项 (Select Options)

```html
<select v-model="selected">
  <!-- 内联对象字面量 -->
  <option :value="{ number: 123 }">123</option>
</select>
```

```javascript
// 当选中时
typeof vm.selected // => 'object'
vm.selected.number // => 123
```

### 修饰符

#### .lazy

在默认情况下，`v-model` 在每次 `input` 事件触发后将输入框的值与数据进行同步 (除了上述输入法组织文字时)。你可以添加 `lazy` 修饰符，从而转为在 `change` 事件之后进行同步：

```html
<!-- 在“change”时而非“input”时更新 -->
<input v-model.lazy="msg" />
```

#### .number

如果想自动将用户的输入值转为数值类型，可以给 `v-model` 添加 `number` 修饰符：

```html
<input v-model.number="age" type="text" />
```

当输入类型为 `text` 时这通常很有用。如果输入类型是 `number`，Vue 能够自动将原始字符串转换为数字，无需为 `v-model` 添加 `.number` 修饰符。如果这个值无法被 `parseFloat()` 解析，则返回原始的值。

#### .trim

如果要自动过滤用户输入的首尾空白字符，可以给 `v-model` 添加 `trim` 修饰符：

```html
<input v-model.trim="msg" />
```

## 组件

父组件

```vue
<template>
    <div class="container">
        <!-- 组件基本使用 -->
        <Main></Main>
        <!-- 组件通过props传值 -->
        <Main :msg="msg"></Main>
        <!-- 子组件的值传递给父组件 -->
        <Main :msg="msg" @change="ChangeMsg"></Main>
    </div>
</template>

<script>
// 引入组件
import Main from "./components/Main.vue"

export default {
    // 组件挂载
    components: {
        Main
    },
    data () {
        return {
            msg: ""
        }
    },
    methods: {
        ChangeMsg(val) {
            // val等参数为子组件中的传参
            console.log(val);
            this.msg = val;
        }
    }
}
</script>
```

`Main`子组件

```vue
<template>
    <div class="mian">
        Main
        <button @click="changeMsg">改变msg</button>
    </div>
</template>

<script>
export default {
    props: {
        msg: {
            type: String,
            default: ""
        }
    },
    methods: {
        changeMsg() {
            // 第一个参数作为事件名称（自定义），与父组件一致
            // 剩余的参数作为事件(change)的参数
            this.$emit("change", "hello vue3");
        }
    }
}
</script>
```

## 生命周期

![lifecycle](./images/lifecycle.png)
