---
title: 如何发布自己的NPM包
date: 2021-12-12
tags:
 - npm
categories:
 - Tools
---

如何发布自己的NPM包
---

在发布自己的npm包之前需要提前准备好要发布的项目。

## 1.注册NPM 账号

注册地址：[npm官网](www.npmjs.com/)

## 2.登录npm

运行指令进行npm登录。

```shell
npm login
```

输入命令后会让输入`Username`, `Password` , `Email`三项信息。

在这里可能遇到输入完对应信息后一直没有反应的情况，这是因为你采用的 `taobao/cnpm` 镜像造成的，需要切换回`npm`的源，问题得以解决。

>出现 `Logged in as 用户名 on https://registry.npmjs.org/.` 表示登录成功

```shell
nrm ls
nrm use npm
```

- nrm ls查看当前采用的镜像
- nrm use npm 切换采用npm镜像

## 3.发布自己的npm包

发布包前要保证自己的包名是唯一的，随后运行指令：

```shell
npm publish
或
npm publish --access public
```

当你以为即将成功的时候，坑又来了。

### 3.1 你可能遇到：
```shell
npm ERR! code E402
npm ERR! 402 Payment Required - PUT https://registry.npmjs.org/项目名 - You must sign up for private packages
```

那是因为 `@npm/package-name` 这种形式的包名，是有作用域的包名形式，执行 `npm publish` 的时候默认是发布私有的包。
因此，第一种方式是花钱买私有包的服务，另外一种方式就是指定参数，表示公开：

```shell
npm publish --access public
```

### 3.2 可能遇到

```shell
npm ERR! This package has been marked as private
npm ERR! Remove the 'private' field from the package.json to publish it.
```

因为这是一个私有项目，所以要更改`package.json` 配置，修改 `private` 属性值为 `false` 即可。

```json
"private": false
```

### 3.3 可能遇到

```shell
npm ERR! code E403
npm ERR! 403 403 Forbidden - PUT https://registry.npmjs.org/*** - Forbidden
npm ERR! 403 In most cases, you or one of your dependencies are requesting
npm ERR! 403 a package version that is forbidden by your security policy.
```

因为镜像没有用npm镜像/没有验证邮箱。注册后，npm 网站会出现一个需要验证邮箱的提示信息，如果没有验证邮箱地址，将无法发布 `npm` 包。
![邮箱验证提示](./images/email-verify.png)


经过万重艰难险阻，当你终于看到以下提示，便发布成功了。

```shell
+ 包名@版本号
```