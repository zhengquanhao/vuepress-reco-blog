---
title: 手写一个Vue组件发布到NPM
date: 2021-12-12
tags:
 - npm
categories:
 - Tools
---


手写一个Vue组件发布到npm
---

## 一、前言

我们为什么要写个组件上传到 `npm` 镜像上呢，我们肯定遇到过这样一个场景，项目中有很多地方与某个功能相似，你想到的肯定是把该功能封装成 `Component` 组件，后续方便我们调用。但是过了一段时间，你的Leader让你去开发另一个项目，结果你在哪个项目中又看见了类似的功能，你这时会怎么做?   你也可以使用 `Ctrl + c + v` 大法，拿过来上一个项目封装好的代码，但是如果需求有些变动，你得维护两套项目的代码，甚至以后更多的项目....，这时你就可以封装一个功能上传到你们公司内网的 `npm` 上(或者自己的账号上)，这样每次遇到类似的功能直接 `npm install` 安装并`import` 导入进来使用就可以，需求有变动时完全可以改动一处代码。


## 二、搭建项目

本文主要流程在于手写 `npm` 包，故项目使用 `vue-cli` 搭建基本结构，如需其他依赖还请自行安装。

```shell
vue create ron-npm-template
```

> 在这里我们选择vue2默认模板。

## 三、书写npm包

### 3.1 目标

我们将手写一个头部组件并抽离为 `npm` 包发布供任何项目安装使用。
### 3.2 代码书写

根目录下新建`packages`文件夹，此文件夹将存放包主要代码。`packages`下新建`header`文件夹和`index.js`文件。`header`文件夹下为头部vue文件`main.vue`。

1. `index.js`代码：

```javascript
import Header from './header/main.vue'

Header.install = function (Vue) {
  Vue.component(Header.name, Header)
}

export default Header
```

如果想要同时注入多个组件 `index.js` 书写如下：

```javascript
import Header from './header/main'

const components = [
    Header
]

const install = function (Vue) {
    if (install.installed) return
    install.installed = true
    components.map(component => {
        Vue.component(component.name, component)
    })
}

if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}

export default {
    Header
}
```

每个npm包都需要有 `install` 方法，当执行 `npm i` 命令是将会执行`install`方法，需要注意的是这里使用了`Header.name`，所以在`main.vue`中需要写上`name`属性。

2. `main.vue`中写入代码：

```vue
<template>
    <div id="header">Header</div>
</template>

<script>
export default {
    name: "ronNpmTemplate"
}
</script>

<style>
* {
    margin: 0;
    padding: 0;
}
#header {
    line-height: 60px;
    background: pink;
}
</style>
```

3. 自此npm包就写好了，可是其他项目安装我们的npm依赖时是怎样找到对应的目录和install方法的呢？所以我们需要修改`package.json`文件指定入口。

新增一条脚本指令:

```json
"scripts": {
    "lib": "vue-cli-service build --target lib --name ron-npm-template --dest lib packages/index.js"
}
```

这句话的意思是将`packages/index.js`下的组件打包到`lib`文件夹下，并且生成的文件名为`ron-npm-template`。

运行`npm run lib`进行npm打包操作。

```shell
npm run lib
```

出现以下提示证明打包成功，并且可以清晰看到打包生成的文件名称和大小，我们真正挂载在npm上的代码也是这些。

```shell
  File                           Size                                         Gzipped

  lib/ron-npm-template.umd.min.js    9.00 KiB                                     3.32 KiB
  lib/ron-npm-template.umd.js        29.73 KiB                                    7.43 KiB
  lib/ron-npm-template.common.js     29.25 KiB                                    7.30 KiB
  lib/ron-npm-template.css           0.04 KiB                                     0.06 KiB
```

以为这就结束了可以快乐的发布了？其实还没完，需要在 `package.json` 使用`main`属性指定真正的包代码。

```json
"main": "lib/ron-npm-template.umd.min.js",
```

## 四、测试组件

根目录新建`example`文件夹，下面创建`app.vue`，`main.js`并加入代码。

1. `main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'
import Header from '../packages/index'

Vue.use(Header)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
```

2. `app.vue`

```vue
<template>
  <div id="app">
    <ron-npm-template />
  </div>
</template>

<script>
export default {
  name: 'app'
}
</script>
```

> 因为我们在`package/index.js`中使用`Vue.component(Header.name, Header)`全局增加该组件，所以可以直接这样使用`<ron-npm-template />`。

3. 并在配置文件 `vue.config.js` 中更改项目入口：

```javascript
module.exports = {
  pages: {
    index: {
      entry: 'examples/main.js',
      template: 'public/index.html',
      filename: 'index.html'
    }
  },
  productionSourceMap: false
}
```

运行`npm run serve`便可以看到组件效果。


## 五、结语

[项目代码GitLab地址](https://gitlab.com/zhengquanhao/npm-publish-test)