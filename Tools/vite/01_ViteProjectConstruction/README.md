---
title: 搭建第一个 Vite 项目
date: 2021-12-04
tags:
 - Vite
categories:
 - Tools
sticky: 1
---

搭建第一个 Vite 项目
---

## 一、前提环境

使用`Vite` 需要 `Node.js` 版本 `>= 12.0.0`

查看Node版本指令

```shell
node -v
或
node --version
```

## 二、搭建Vite项目

使用 NPM:

```shell
npm init vite@latest
```

使用 Yarn:

```shell
yarn create vite
```

使用 PNPM:

```shell
pnpm create vite
```

然后按照提示操作即可，我们在这里选择 `vue` 框架。

随后运行一下命令进入并启动项目

```shell
cd vite-project
npm i
npm run dev
```

> 细心的小伙伴可能发现搭建`vite`项目时间特别短，因为`webpack`搭建项目时帮我们字段安装了依赖而`vite`需要手动安装。

## 三、基本配置

在`vite.config.js`进行基本配置，包含本地服务器配置，别名配置，反向代理。

```javascript
import { defineConfig } from "vite"
import { resolve } from "path"
import vue from "@vitejs/plugin-vue"

function pathResolve(dir) {
    return resolve(__dirname, ".", dir);
}


export default defineConfig({
    plugins:[vue()], // 配置需要使用的插件列表，这里将vue添加进去
    resolve: {
        alias: {
          "/@": pathResolve("src"), // 这里是将src目录配置别名为 /@ 方便在项目中导入src目录下的文件
        }
    },
    // 本地运行配置，及反向代理配置
    server: {
        host: "127.0.0.1", // 默认是 localhost
        port: "8000", // 默认是 3000 端口
        cors: true, // 默认启用并允许任何源
        open: true, // 浏览器自动打开
        //反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
        proxy: {
            "/api": {
                target: "http://目标地址",   //代理接口
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, "")
          }
        }
    }
})
```

## 四、使用路由

### 4.1 安装vue-router依赖

```shell
npm install vue-router@next -S
```

### 4.2 路由文件配置

新建`router`文件夹`index.js`文件并写入路由配置。

```javascript
import { createRouter, createWebHistory } from "vue-router"

// 开启历史模式
// vue2中使用 mode: history 实现
const routerHistory = createWebHistory();

const router = createRouter({
    history: routerHistory,
    routes: [
        {
            path: "/",
            redirect: "/home"
        },
        {
            path: "/home",
            name: "home",
            component: () => import("@/views/Home.vue")
        },
        {
            path: "/about",
            name: "about",
            component: () => import("@/views/About.vue")
        }
    ]
})

export default router;
```

> 此处省略搭建`Home.vue`，`About.vue`文件。

### 4.3 路由引用

在入口文件 `main.js` 中引用路由：

```javascript
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp(App)
app.use(router)
app.mount('#app')
```

### 4.4 路由容器

在`App.vue`中加入路由容器：

```vue
<router-view></router-view>
```

### 4.5 路由跳转

在 `About.vue` 文件尝试下路由跳转：

```vue
<template>
    <div class="about">
        <p>About</p>
        <button @click="toHome">toHome</button>
    </div>
</template>
<script>
import { useRouter } from 'vue-router'
export default {
  setup () {
    const router = useRouter()
    const toHome = (() => {
      router.push({
        name: 'home'
      })
    })
    return {
      toHome
    }
  },
}
</script>
<style  scoped>
</style>
```

### 4.6 遇到的坑

当使用`npm install vue-router`方式安装，且使用 `createRouter` 函数式可能遇到一下报错，将vue-router卸载，并使用`npm install vue-router@next -S`安装问题解决。

```shell
The requested module '/node_modules/.vite/vue-router.js?v=8b9a6bbd' does not provide an export named 'createRouter'
```

## 五、引入全局状态管理Vuex

### 5.1 安装vuex依赖

```shell
# 安装vuex@4
npm install vuex@next --save
```

### 5.2 vuex文件配置

src下新建`store`文件夹，及`index.js`文件加入配置：

```javascript
import { createStore } from 'vuex';

const store = createStore({
  state: {},
  mutations: {},
  actions: {},
  getters: {}
});

export default store;
```

目录划分：

```javascript
import { createStore } from "vuex";
import state from "./state";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

// 创建一个新的 store 实例
const store = createStore({
    state,
    mutations,
    actions,
    getters
});

export default store;
```

state.js:

```javascript
export default {
    isLoading: false,
    loadingNum: 0
};
```

mutations.js:

```javascript
export default {
    setLoadingState (state, payload) {
        console.log(payload);
        if (payload) {
            state.loadingNum++;
            if (state.loadingNum > 0) {
                state.isLoading = true;
            }
        }
        if (!payload) {
            state.loadingNum--;
            if (state.loadingNum === 0) {
                state.isLoading = false;
            }
        }
    }
};
```

getters.js:

```javascript
export default {
    getLoadingState (state) {
        return state.isLoading;
    }
};
```

actions.js:

```javascript
export default {
    setLoadingState (context, payload) {
        context.commit("setLoadingState", payload);
    }
};
```

### 5.3 vuex的引用

在入口文件 `main.js` 中引用路由：

```javascript
// main.js
...
import store from "./store";

app.use(store);
```

### 5.4 vuex的基本使用

```html
<p>{{ $store.state.isLoading }}</p>
```

## 六、使用less预处理器

### 6.1 安装less依赖

```shell
npm i less --save-dev
```

### 6.2 配置less

```javascript
// vite.config.js:
...
export default defineConfig({
    // 支持 less 样式
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true
            }
        }
    }
})
```

### 6.3 验证less是否配置成功

```html
<div class="box">
    <div class="h1">h1</div>
    <div class="h2">h2</div>
</div>

<style scoped lang="less">
.box {
  width: 100px;
  height: 100px;
  border: 1px solid #42b983;
  .h1 {
    color: red;
  }
  .h2 {
    color: blue;
  }
}
</style>
```

## 七、接口请求 fetch

### 7.1 安装axios依赖

```shell
npm i axios --save
```

### 7.2 定义全局fetch方法

```javascript
// src/common/fetch/index.js
import axios from "axios";
import store from "@/store";

// 添加一个响应拦截器
axios.interceptors.response.use(function (res) {
    return res;
}, function (error) {
    const data = error.response.data;
    if (data.code === 20001) {
        const currentUrl = window.location.href;
        window.location = data.data.loginUrl + "?returnUrl=" + currentUrl;
    } else {
        return Promise.reject(error);
    }
});

export default function fetch (options, header) {
    store.dispatch("setLoadingState", true);
    return new Promise((resolve, reject) => {
        const arrHeader = { "Content-Type": header || "application/json;charset=utf-8" };
        // 创建一个axios实例
        const instance = axios.create({
            // 设置默认根地址
            baseURL: process.env.VUE_APP_BASE_API,
            // 设置请求超时设置
            timeout: 30000,
            // 设置请求时的header
            headers: arrHeader,
            // 允许跨域携带cookie
            withCredentials: true
        });

        // 请求处理
        instance(options)
            .then((res) => {
                // 请求成功时,根据业务判断状态
                if (res.data) {
                    resolve(res.data);
                    store.dispatch("setLoadingState", false);
                    return false;
                } else {
                    resolve(res.data);
                    store.dispatch("setLoadingState", false);
                    return false;
                }
            })
            .catch((error) => {
                // 请求失败时,根据业务判断状态
                if (error) {
                    store.dispatch("setLoadingState", false);
                    reject(error.data);
                }
            });
    });
}
```

### 7.3 全局挂载$fetch方法

```javascript
// src/api/index.js
import * as apiTest from "./test";

const apiObj = {
    apiTest
};

const install = function (Vue) {
    if (install.installed) return;
    install.installed = true;

    Object.defineProperties(Vue.config.globalProperties, {
        $fetch: {
            get () {
                return apiObj;
            }
        }
    });
};

export default install;
```

> vue2中注册全局变量使用 `Vue.prototype`，但在vue3中无法通过 `Vue.prototype` 来注册全局组件/方法，我们需要使用 `Vue.config.globalProperties`。

### 7.4 全局注入

main.js:

```javascript
import api from "./api/index";
app.use(api);
```

### 7.5 组件中使用fetch

如果有一个模块叫`Test`，我们将词模块的所有接口统一维护在`api/test.js`下:

```javascript
import fetch from "../common/fetch";

// const baseURL = "https://test.com"; // mock
const baseURL = process.env.NODE_ENV === "production" ? "接口域名" : ""; // 代理

// get请求
export function getList (params) {
    return fetch({
        url: baseURL + "/about/list",
        method: "get",
        params
    });
};

// post请求
export function addList (params) {
    return fetch({
        url: "接口地址",
        method: "post",
        data : params
    });
};

// post-application形式
// export function addList (params) {
//     return fetch({
//         url: "接口地址",
//         method: "post",
//         data : params
//     }, "application/json");
// };

// restful风格接口参数拼接
export function deleteList (id, params) {
    return fetch({
        url: `接口地址/${id}`,
        method: "delete",
        params
    });
};

// 上传-文件格式
export function upload (params) {
    return fetch({
        url: baseURL + "/upload",
        method: "post",
        data: params
    }, "multipart/form-data");
}
```

在组件中使用fetch:

```javascript
import { onMounted, getCurrentInstance } from "vue";

export default {
    setup () {
        const internalInstance = getCurrentInstance();
        const $fetch = internalInstance.appContext.config.globalProperties.$fetch;
        const getList = () => {
            $fetch.apiTest.getList().then(res => {
                if (res.code === 200) {
                    console.log(res);
                }
            }).catch(() => {
                console.log("error");
            });
        };

        onMounted(() => {
            getList();
        });

        return {
            getList
        };
    }
};
```

> 注意：Vue3在组件中使用全局变量/方法需要`getCurrentInstance`。

### 7.6 处理代理

```javascript
"/api": {
    target: "http://目标地址", // 代理接口
    ws: false,
    secure: false,
    changeOrigin: true,
    rewrite: (path) => path.replace(/^\/api/, "")
}
```

> 注意：如果是从webpack迁移的项目，webpack中重写路径的属性名叫`pathRewrite`，在vite中叫`rewrite`。

## 八、Element Plus 组件库

### 8.1 安装Element Plus 依赖

```shell
npm install element-plus --save
```

### 8.2 完整引入

`main.js`:

```javascript
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
app.use(ElementPlus);
```

### 8.3 按需导入（推荐）

#### 8.3.1 安装依赖

```shell
npm install -D unplugin-vue-components unplugin-auto-import
```

> 按需导入需要插件`unplugin-vue-components`，如果需要自动导入可以安装`unplugin-auto-import`插件。

#### 8.3.2 配置vite

```javascript
// vite.config.ts
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

export default {
  plugins: [
    // ...
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
}
```

> 使用 `unplugin-vue-components` 后，可以实现按需引入，而且也不用在`main.js`中再使用import引入需要的组件，直接使用`<el-button>`等组件就可以。

### 8.4 验证Element Plus 使用成功

```html
<el-button color="#626aef" style="color: white">Custom</el-button>
<el-button color="#626aef" plain>Custom</el-button>
```

## 九、多语言国际化 i18n

### 9.1 安装i18n依赖

```shell
# 兼容vue3需安装9版本
npm i vue-i18n@9
# or
npm i vue-i18n@next
```

### 9.2 使用方法

1. 引入i18n
2. 创建实例
3. 准备好需要的翻译
4. 选择性使用element-plus
5. 导出i18n

```javascript
// src/i18n/index.js
import { createI18n } from "vue-i18n";
import cookie from "@/utils/cookie";
import * as id from "./in_ID";
import * as en from "./en_US";
import * as cn from "./zh_CN";
import ElementPlus from "element-plus";
import elementZh from "element-plus/es/locale/lang/zh-cn";
import elementEn from "element-plus/es/locale/lang/en";
import elementId from "element-plus/es/locale/lang/id";

const elementMap = {
    in_ID: elementId,
    en_US: elementEn,
    zh_CN: elementZh
};

const install = (app) => {
    const i18n = createI18n({
        locale: cookie.getCookie("language") || "in_ID",
        messages: {
            in_ID:  {
                ...id,
                ...elementId
            },
            en_US: {
                ...en,
                ...elementEn
            },
            zh_CN: {
                ...cn,
                ...elementZh
            }
        }
    });
    app.use(ElementPlus, {
        locale: elementMap[cookie.getCookie("language") || "in_ID"]
    });
    app.use(i18n);
};
export default install;
```

en_US.json:

```json
{
    "userName": "UserName"
}
```

zh_CN.json

```json
{
    "userName": "用户名"
}
```

in_ID.json

```json
{
    "userName": "UserName ID"
}
```

```javascript
// src/utils/cookie.js
export default {
    getCookie (name) {
        const reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
        const arr = document.cookie.match(reg);
        if (arr) {
            return unescape(arr[2]);
        } else {
            return null;
        }
    }
};
```

### 9.3 把 i18n 挂载到 Vue 实例上

```javascript
// main.js
import i18n from "./i18n/index";
app.use(i18n);
```

### 9.4 在 HTML 模板中使用

```html
<template>
    <!-- 验证json是否引入成功 -->
    <h1>{{t("userName")}}</h1>
    <!-- 验证element plus是否引入成功 -->
    <el-pagination
        :page-size="20"
        :pager-count="11"
        layout="prev, pager, next, jumper"
        :total="1000"
    >
    </el-pagination>
</template>
<script>
import { useI18n } from "vue-i18n";
export default {
    setup() {
        const { t } = useI18n();
        return { t }
    }
}
</script>
```

## 十、Eslint

### 10.1 安装eslint依赖

package.json:

```json
"devDependencies": {
    ...
    "eslint": "^7.26.0",
    "eslint-config-standard": "^16.0.2",
    "eslint-friendly-formatter": "^4.0.1",
    "eslint-loader": "^4.0.2",
    "eslint-plugin-import": "^2.23.2",
    "eslint-plugin-node": "^11.1.0",
    "eslint-plugin-promise": "^5.1.0",
    "eslint-plugin-vue": "^7.9.0"
  }
```

```shell
npm i
```

### 10.2 配置eslint

根目录下新增`.eslintrc.js`:

```javascript
module.exports = {
    root: true,
    env: {
        browser: true,
        es6: true
    },
    parserOptions: {
        sourceType: "module"
    },
    extends: [
        "plugin:vue/essential",
        "standard"
    ],
    plugins: [
        "vue"
    ],
    rules: {
        // 箭头函数规则需要加小括号
        "arrow-parens": 0,
        // 构造函数前后空格
        "generator-star-spacing": 0,
        // 禁止使用debug
        "no-debugger": 0,
        // 分号结尾
        semi: [2, "always"],
        // 强制双引号
        quotes: [2, "double"],
        // 变量、对象等前后空格
        "key-spacing": [0, { beforeColon: true, afterColon: true }],
        // 缩进
        indent: [2, 4],
        // 取消换行结束
        "eol-last": 0,
        "no-labels": ["error", { allowLoop: true }]
    }
};
```

## 十一、配置postcss

Vite自身已经集成PostCSS，无需再次安装。另外也无需单独创建PostCSS配置文件，已集成到vite.config.js的css选项中。可直接配置css.postcss选项即可。
### 11.1 安装autoprefixer依赖

```shell
npm i autoprefixer@8 --save-dev
```

> 默认安装了最新版autoprefixer没有生效所以安装了8版本。

版本号：
```json
"autoprefixer": "^8.6.5",
"vite": "^2.6.4"
```

### 11.2 添加配置

根目录下新建 `vite.config.js` 文件并写入代码：

```javascript
import autoprefixer from 'autoprefixer'
...
export default defineConfig({
    ...
    css:{
        postcss:{
          plugins:[
            autoprefixer()
          ]
        }
    }
})
```

### 11.3 增加浏览器配置文件 `.browserslistrc`

```shell
> 1%
last 2 versions
not dead
```
### 11.4 效果展示
当看到如下效果，表示 `autoprefixer` 生效：
```css
.footer[data-v-fae5bece] {
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
}
```

## 十二、图片懒加载

### 12.1 安装依赖

```shell
npm i vue3-lazyload -S
```

### 12.2 挂载到Vue实例

```javascript
// main.js
import VueLazyLoad from "vue3-lazyload";
import defaultImage from "./assets/images/default.jpeg";
app.use(VueLazyLoad, {
    loading: defaultImage, // 图片正在加载时显示的 loading 图片
    error: defaultImage // 图片加载异常时显示的 error 图片
});
```

### 12.3 使用图片懒加载

```html
<!-- Home.vue -->
<template>
  <img v-for="(img, i) in ImageList" :key="i" v-lazy="{src: img}" />
</template>
<script>
import { onMounted, getCurrentInstance, ref } from "vue";
export default {
    setup () {
        const internalInstance = getCurrentInstance();
        const $fetch = internalInstance.appContext.config.globalProperties.$fetch;
        const ImageList = ref([]);

        const getImageList = () => {
            $fetch.apiTest.getImageList({ id: "123456" }).then(res => {
                if (res.code === 200) {
                    ImageList.value = res.data;
                }
            }).catch(() => {
                console.log("error");
            });
        };

        onMounted(() => {
            getImageList();
        });

        return {
            ImageList
        }

    }
}
</script>
```

## 十三、全局过滤器

vue3删除了filter就好比：员工filter会干的活，员工computed和员工methods也会干，而且比员工filter干得多，干的好。这样的话，老板vue就把filter开除了，filter就被fired了。毕竟多一个员工，多一些用工成本（员工filter哇的一声哭了出来）

### 13.1 配置全局过滤器

```javascript
const NumFormat = num => {
    if (!num) return "0.00";

    if (num) {
        //   num = num.toString().split('.')[0];
        // eslint-disable-next-line no-useless-escape
        num = parseInt(num).toString().replace(/\$|\,/g, "");
        if (num === "" || isNaN(num)) {
            return "";
        }
        const sign = num.indexOf("-") > -1 ? "-" : "";
        if (num.indexOf("-") > -1) {
            num = num.substring(1);
        }
        let cents = num.indexOf(".") > 0 ? num.substr(num.indexOf(".")) : "";
        cents = cents.length > 1 ? cents : "";
        num = num.indexOf(".") > 0 ? num.substring(0, num.indexOf(".")) : num;
        if (cents === "") {
            if (num.length > 1 && num.substr(0, 1) === "0") {
                return "";
            }
        } else {
            if (num.length > 1 && num.substr(0, 1) === "0") {
                return "";
            }
        }
        for (let i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
            num = num.substring(0, num.length - (4 * i + 3)) + "," + num.substring(num.length - (4 * i + 3));
        }
        return sign + num + cents;
    } else {
        return "0";
    }
};
export { NumFormat };
```

```javascript
// main.js
import * as filters from "./filters/index";
app.config.globalProperties.$filters = {
    ...filters
};
```

### 13.2 使用全局过滤器

```html
<!-- Home.vue -->
<h1>{{ $filters.NumFormat(12345.00) }}</h1>
```

## 十四、全局指令

```javascript
// src/directive/clstag.js
// 基础埋点前缀
const CLSTAG_BASE_PREFIX = "pageclick|keycount|";
export default {
    name: "clstag",
    inserted: function (el, binding) {
        const serviceValue = binding.value.value;
        const serviceKey = binding.value.key;
        if (!serviceValue) {
            return;
        }
        const clsTag = serviceKey ? (CLSTAG_BASE_PREFIX + serviceKey + "|" + serviceValue) : (CLSTAG_BASE_PREFIX + serviceValue);
        el.setAttribute("clstag", clsTag);
    }
};
```

```javascript
// src/directive/index.js
import clsTag from "./clstag";

const vueDirective = {};
vueDirective.install = function install (Vue) {
    // 埋点
    Vue.directive("clstag", clsTag);
};

export default vueDirective;
```

## 十五、开启gzip压缩

使用 `vite-plugin-compression` 插件包帮助我们完成gzip压缩工作。

### 15.1 依赖安装

```shell
npm i vite-plugin-compression -D
```

### 15.2 使用依赖

在 `vite.config.js` 中加入配置。
```javascript
...
import viteCompression from 'vite-plugin-compression';
export default defineConfig({
    plugins:[
        ...
        viteCompression({
            threshold: 1024
        })
    ]
    ...
})
```

打包成功后会在打包文件夹中多出`.gz`文件。

[点击这里查看更多`vite-plugin-compression` 配置说明](https://www.npmjs.com/package/vite-plugin-compression)

## 十六、sourceMap

```javascript
// vite.config.js
// 打包配置
build: {
    // 构建后是否生成 source map 文件
    sourcemap: process.env.NODE_ENV === 'development'
}
```

## 十七、添加环境变量
根目录下新建`.env`, `.env.pre`, `.env.production`, `.emv.development`，写入不同环境下的环境变量信息，这里我们配置一下几条，可自行添加，指令可在`package.json`下使用`--mode`自行配置。

文件说明:

- `.env`：全局默认配置文件，无论什么环境都会加载合并。
- `.env.development`：开发环境的配置文件
- `.env.production`：生产环境的配置文件

```shell
NODE_ENV = 'production'
VUE_APP_BASE_API = '/api'
outputDir = dist
```

环境变量使用方式举例： `process.env.NODE_ENV`

## 十八、引入normalize

- `src/style`下放入 `normalize.css` 文件。

- main.js入口引入。

```javascript
import "@/style/normalize.css";
```

## 关闭生产环境devtools

```javascript
// 分环境处理
if (process.env.NODE_ENV === 'development') {
  if ('__VUE_DEVTOOLS_GLOBAL_HOOK__' in window) {
  // 这里__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue赋值一个createApp实例
    window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = app
  }
  app.config.devtools = true
}
```

## 十九、使公共资源不进行打包，通过cdn获取

### 19.1 安装依赖

```shell
npm install vite-plugin-cdn-import --save-dev
```

### 19.2 配置

```javascript
// vite.config.js
import importToCDN from 'vite-plugin-cdn-import'

export default {
    plugins: [
        importToCDN({
            modules: [
                // {
                //     name: "vue",
                //     var: "Vue",
                //     path: "//xxx.com/vue.min.js"
                // },
                {
                    name: "axios",
                    var: "axios",
                    path: "//lib.jd.id/common/axios.min.js"
                }
            ]
        })
    ]
}
```

[参考资料](https://www.npmjs.com/package/vite-plugin-cdn-import)

## 更多参考

项目GitLab地址[vue3-vite-template](https://gitlab.com/zhengquanhao/vue3-vite-template)

更多学习请参见[vite官方文档](https://cn.vitejs.dev/)