---
title: Vue项目配置在局域网下访问
date: 2021-12-04
tags:
 - Vue
categories:
 - Tools
---

Vue项目配置在局域网下访问
---

### 一、vue-cli2.0项目

`vue-cli2.0`项目需要把 `devServer` 的 `host`改为 `0.0.0.0`。

配置如下：
![LAN-access](./images/LAN-access.png)

### 二、vue-cli3.0项目

`vue-cli3.0`项目会默认帮我们期待局域网访问配置，不需要配置即可访问。

> 遇到cookie问题，可配置`host`解决。
> 例：192.168.137.193 inside-a.com
