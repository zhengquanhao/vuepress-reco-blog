---
title: ES6这些技巧你真的知道吗？
date: 2021-12-23
tags:
 - ES6
---

## 1. 互换两个对象的值

利用ES6数组解构快速实现值的交换。

```javascript
let a = "hello";
let b = "world";
[a, b] = [b, a];
console.log("a", a); // world
console.log("b", b); // hello
```

## 2. 对象的展开

接口大多数传递的参数都是key:value形式的对象,现在我们刚好有一个对象，接口里面的参数也正好相等，我们可以下面这种写法没问题，但是我们利用数组结构有更加简单的用法。

之前的写法：

```javascript
let formData = {
    username: "Ron",
    age: 23
}
let params = {
    username: formData.username,
    age: formData.age
}
console.log("params", params); // {username: 'Ron', age: 23}
```

优化后的写法：

```javascript
let params = {...formData};
console.log("params", params); // {username: 'Ron', age: 23}
```

## 3. 数组拼接

以前我们都是通过 `concat` 来进行数据拼接的，这种方法假如我们有很多种数组就要拼接很多次,要多写很多次代码,极大的减少了我们摸鱼的时间，现在可以通过扩展运算符来进行数组合并拼接。

```javascript
let a = ["a", "b", "c"];
let b = ["d", "e"];
let c = ["f"];
console.log([...a, ...b, ...c]); // ['a', 'b', 'c', 'd', 'e', 'f']
```

## 4. 数组去重

我们可以通过ES6的 `Set` 数组方法去重。

```javascript
let arr = [1, 2, 3, 4, 5, 6, 3, 3, 4, 5];
console.log([...new Set(arr)]); // [1, 2, 3, 4, 5, 6]
```

## 5. 对象取值

假设有个对象是下面这种结构要求算出a+b,用ES6数组解构可以快速实现。

```javascript
const obj = {
    a: 1,
    b: 2,
    c: 3,
    d: 4
}
const {a, b, c, d} = obj;
console.log(a + b); // 3
console.log(c + d); // 7
```

## 6. 可选链操作符

假设我们要获取到deepObj这个对象中的蓝莓: 我们需要进行判断 否则可能会出现错误。

```javascript
// 使用 && 操作符
let blueberry = deepObj.fruit && deepObj.fruit.onTheGround && deepObj.fruit.onTheGround.blueberry || '';
// 使用可选链操作符
let blueberry = deepObj.fruit?.onTheGround?.blueberry || '';
```
