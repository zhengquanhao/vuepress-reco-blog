---
title: JavaScript 简介
date: 2021-12-02
tags:
 - JavaScript
---

JavaScript 简介
---

让我们来看看 JavaScript 有什么特别之处，我们可以用它实现什么，以及哪些其他技术可以与其搭配产生奇妙的效果。

## 什么是 JavaScript？

JavaScript 最初被创建的目的是“使网页更生动”。

这种编程语言写出来的程序被称为 `脚本`。它们可以被直接写在网页的 `HTML` 中，在页面加载的时候自动执行。

脚本被以纯文本的形式提供和执行。它们不需要特殊的准备或编译即可运行。


> **为什么叫 JavaScript？**
> `JavaScript` 在刚诞生的时候，它的名字叫 “LiveScript”。但是因为当时 `Java` 很流行，所以决定将一种新语言定位为 Java 的“弟弟”会有助于它的流行。
> 随着 `JavaScript` 的发展，它已经成为了一门完全独立的语言，并且也拥有了自己的语言规范 [ECMAScript](https://en.wikipedia.org/wiki/ECMAScript)。

如今，JavaScript 不仅可以在浏览器中执行，也可以在服务端执行，甚至可以在任意搭载了 JavaScript 引擎 的设备中执行。

浏览器中嵌入了 `JavaScript` 引擎，有时也称作“JavaScript 虚拟机”。

不同的引擎有不同的“代号”，例如：

- [V8](https://en.wikipedia.org/wiki/V8_(JavaScript_engine)) —— Chrome 和 Opera 中的 JavaScript 引擎。
- [SpiderMonkey](https://en.wikipedia.org/wiki/SpiderMonkey) —— Firefox 中的 JavaScript 引擎。
- ……还有其他一些代号，像 “Chakra” 用于 IE，“ChakraCore” 用于 Microsoft Edge，“Nitro” 和 “SquirrelFish” 用于 Safari，等等。

上面这些术语很容易记住，因为它们经常出现在开发者的文章中。我们也会用到这些术语。例如，如果“V8 支持某个功能”，那么我们可以认为这个功能大概能在 Chrome 和 Opera 中正常运行。

> **引擎是如何工作的？**  
> 引擎很复杂，但是基本原理很简单。  
>
> 1. 引擎（如果是浏览器，则引擎被嵌入在其中）读取（“解析”）脚本。  
> 2. 然后，引擎将脚本转化（“编译”）为机器语言。  
> 3. 然后，机器代码快速地执行。  
>
> 引擎会对流程中的每个阶段都进行优化。它甚至可以在编译的脚本运行时监视它，分析流经该脚本的数据，并根据获得的信息进一步优化机器代码。

## 浏览器中的 JavaScript 能做什么？
现代的 JavaScript 是一种“安全的”编程语言。它不提供对内存或 CPU 的底层访问，因为它最初是为浏览器创建的，不需要这些功能。

JavaScript 的能力很大程度上取决于它运行的环境。例如，Node.js 支持允许 JavaScript 读取/写入任意文件，执行网络请求等的函数。

浏览器中的 JavaScript 可以做与网页操作、用户交互和 Web 服务器相关的所有事情。

例如，浏览器中的 JavaScript 可以做下面这些事：

- 在网页中添加新的 HTML，修改网页已有内容和网页的样式。
- 响应用户的行为，响应鼠标的点击，指针的移动，按键的按动。
- 向远程服务器发送网络请求，下载和上传文件（所谓的 AJAX 和 COMET 技术）。
- 获取或设置 cookie，向访问者提出问题或发送消息。
- 记住客户端的数据（“本地存储”）。

## 是什么使得 JavaScript 与众不同？

至少有 3 件事值得一提：

- 与 HTML/CSS 完全集成。
- 简单的事，简单地完成。
- 被所有的主流浏览器支持，并且默认开启。

JavaScript 是将这三件事结合在一起的唯一的浏览器技术。

这就是为什么 JavaScript 与众不同。这也是为什么它是用于创建浏览器界面的使用最广泛的工具。

此外，JavaScript 还可用于创建服务器和移动端应用程序等。


## 总结
JavaScript 最开始是专门为浏览器设计的一门语言，但是现在也被用于很多其他的环境。
如今，JavaScript 已经成为了与 HTML/CSS 完全集成的，使用最广泛的浏览器语言。