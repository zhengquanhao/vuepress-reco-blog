简介
---

介绍 JavaScript 语言及其开发环境。

- [JavaScript 简介](/JS/01_Introduction/01_IntroductionToJavaScript/)

- [代码编辑器](/JS/01_Introduction/02_CodeEditor/)

- [开发者控制台](/JS/01_Introduction/03_DeveloperConsole/)