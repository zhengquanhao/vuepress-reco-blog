---
title: 在 Chrome 中调试
date: 2021-12-02
tags:
 - JavaScript
---

`调试` 是指在一个脚本中找出并修复错误的过程。所有的现代浏览器和大多数其他环境都支持调试工具 —— 开发者工具中的一个令调试更加容易的特殊用户界面。它也可以让我们一步步地跟踪代码以查看当前实际运行情况。

在这里我们将会使用 Chrome（谷歌浏览器），因为它拥有足够多的功能，其他大部分浏览器的功能也与之类似。

## “资源（Sources）”面板

你的 `Chrome` 版本可能看起来有一点不同，但是它应该还是处于很明显的位置。

在 `Chrome` 中打开 [示例页面](https://zhengquanhao.gitlab.io/vuepress-reco-blog/JS/03_CodeQuality/01_Debugging/example/debug.html)。

使用快捷键 `F12`（Mac：`Cmd+Opt+I`）打开开发者工具。
选择 `Sources`（资源）面板。
