---
title: 代码结构
date: 2021-12-02
tags:
 - JavaScript
---

我们将要学习的第一个内容就是构建代码块。

## 语句

语句是执行行为（action）的语法结构和命令。

我们已经见过了 `alert('Hello, world!')` 这样可以用来显示消息的语句。

我们可以在代码中编写任意数量的语句。语句之间可以使用分号进行分割。

例如，我们将 “Hello World” 这条信息一分为二：

```javascript
alert('Hello'); alert('World');
```

通常，每条语句独占一行，以提高代码的可读性：

```javascript
alert('Hello');
alert('World');
```

## 分号

当存在换行符（line break）时，在大多数情况下可以省略分号。

下面的代码也是可以运行的：

```javascript
alert('Hello')
alert('World')
```

在这，JavaScript 将换行符理解成“隐式”的分号。这也被称为 自动分号插入。

**在大多数情况下，换行意味着一个分号。但是“大多数情况”并不意味着“总是”！**

有很多换行并不是分号的例子，例如：

```javascript
alert(3 +
1
+ 2);
```

代码输出 6，因为 JavaScript 并没有在这里插入分号。显而易见的是，如果一行以加号 "+" 结尾，那么这是一个“不完整的表达式”，不需要分号。所以，这个例子得到了预期的结果。

**但存在 JavaScript 无法确定是否真的需要自动插入分号的情况。**

这种情况下发生的错误是很难被找到和解决的，故不推荐此写法。

## 注释

随着时间推移，程序变得越来越复杂。为代码添加 注释 来描述它做了什么和为什么要这样做，变得非常有必要了。

你可以在脚本的任何地方添加注释，它们并不会影响代码的执行，因为引擎会直接忽略它们。

单行注释以两个正斜杠字符 `//` 开始。

这一行的剩余部分是注释。它可能独占一行或者跟随在一条语句的后面。

就像这样：

```javascript
// 这行注释独占一行
alert('Hello');

alert('World'); // 这行注释跟随在语句后面
```

多行注释以一个正斜杠和星号开始 `/*` 并以一个星号和正斜杠结束 `*/`。

就像这样:

```javascript
/* 两个消息的例子。
这是一个多行注释。
*/
alert('Hello');
alert('World');
```

注释的内容被忽略了，所以如果我们在 `/* … */` 中放入代码，并不会执行。

有时候，可以很方便地临时禁用代码：

```javascript
/* 注释代码
alert('Hello');
*/
alert('World');
```

> 使用热键: 在大多数的编辑器中，一行代码可以使用 `Ctrl+/` 热键进行单行注释，诸如 `Ctrl+Shift+/` 的热键可以进行多行注释（选择代码，然后按下热键）。对于 `Mac` 电脑，应使用 `Cmd` 而不是 `Ctrl`，使用 `Option` 而不是 Shift。

### 注释使用的注意点

不支持注释嵌套！ 不要在 `/*...*/` 内嵌套另一个 `/*...*/`。

下面这段代码报错而无法执行：

```javascript
/*
  /* 嵌套注释 ?!? */
*/
alert( 'World' );
```

注释会增加代码总量，但这一点也不是什么问题。有很多工具可以帮你在把代码部署到服务器之前缩减代码。这些工具会移除注释，这样注释就不会出现在发布的脚本中。所以，注释对我们的生产没有任何负面影响。
