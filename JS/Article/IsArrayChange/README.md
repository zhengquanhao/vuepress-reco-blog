---
title: JS 改变原数组和不改变原数组的方法整理
date: 2021-12-22
tags:
 - JavaScript
categories:
 - JavaScript
---

## 一、改变原数组的

### 1. shift

将第一个元素删除并且返回删除元素，空即为undefined。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.shift();
console.log(a); // a
console.log(arr); // ['b', 'c', 'd']
```

### 2. unshift

向数组开头添加元素，并返回新的长度。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.unshift(0);
console.log(a); // 5 返回数组长度
console.log(arr); // [0, 'a', 'b', 'c', 'd']
```

### 3. pop

删除最后一个并返回删除的元素。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.pop();
console.log(a); // d
console.log(arr); // ['a', 'b', 'c']
```

### 4. push

向数组末尾添加元素，并返回新的长度。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.push('f');
console.log(a); // 5 返回数组长度
console.log(arr); // ['a', 'b', 'c', 'd', 'f']
```

### 5. reverse

颠倒数组顺序。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.reverse();
console.log(a); // ["d", "c", "b", "a"]
console.log(arr); // ["d", "c", "b", "a"]
```

### 6. sort

对数组排序。

```javascript
let arr = ['c', 'a', 'd', 'b'];
let a = arr.sort();
console.log(a); // ['a', 'b', 'c', 'd']
console.log(arr); // ['a', 'b', 'c', 'd']
```

### 7. splice

`splice(start,length,item)` 删，增，替换数组元素，返回被删除数组，无删除则不返回。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.splice(1, 2, 'f');
console.log(a); // 返回被删除的元素数组['b', 'c']
console.log(arr); // 在添加的地方添加元素后的数组["a", "f", "d"]
```

### 8. copyWithin

方法浅复制数组的一部分到同一数组中的另一个位置，并返回它，不会改变原数组的长度。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.copyWithin(1, 2, 3); // 复制数组的第三项到第二个位置
console.log(a); //返回被复制的元素数组 ['a', 'c', 'c', 'd']
console.log(arr); //原元素数组已经改变 ['a', 'c', 'c', 'd']
```

### 9. fill

用于将一个固定值替换数组的元素。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.fill('e', 2, 4);
console.log(a); // 返回它会改变调用它的 `this` 对象本身, 然后返回它['a', 'b', 'e', 'e']
console.log(arr); // ['a', 'b', 'e', 'e']
```

## 二、不改变原数组的

### 1. concat

连接多个数组，返回新的数组。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.concat(['e', 'f']);
console.log(a); // 新数组 ["a", "b", "c", "d", "e", "f"]
console.log(arr); // ["a", "b", "c", "d"] 不变
```

### 2. join

将数组中所有元素以参数作为分隔符放入一个字符。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.join('-');
console.log(a); // 字符串 a-b-c-d
console.log(arr); // ["a", "b", "c", "d"] 不变
```

### 3. slice

删除元素，返回选定元素。

```javascript
let arr = ['a', 'b', 'c', 'd'];
let a = arr.slice(1);
console.log(a); // ["b", "c", "d"]
console.log(arr); // ["a", "b", "c", "d"] 不变
```

### 4. map,filter,forEach,some,every,reduce

map,filter,forEach,some,every,reduce等不改变原数组。


## 三、知识点补充

### 1. copyWithin知识点补充

语法:

```javascript
array.copyWithin(target, start, end);
```

参数说明:

- target 必需，复制到指定目标索引位置。
- start	可选，元素复制的起始位置。
- end 可选，停止复制的索引位置 (默认为 array.length)。如果为负值，表示倒数。


### 2. fill知识点补充

语法:

```javascript
array.fill(value, start, end);
```

参数说明:

- value 必需，填充的值。
- start 可选，开始填充位置。
- end 可选，停止填充位置 (默认为 array.length)。
