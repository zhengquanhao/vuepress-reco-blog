#!/usr/bin/env sh

# 当发生错误时中止脚本
set -e

# 克隆发布项目
git clone https://gitlab.com/zhengquanhao/vuepress-reco-blog-dist.git

# 生成静态文件
npm run build

# 将静态资源复制到发布项目
cp -r ./dist/. ./vuepress-reco-blog-dist

# 进入发布项目的文件夹
cd ./vuepress-reco-blog-dist

# 执行git操作
git add -A

git commit -m 'deploy'

git push

# 退出发布项目的文件夹
cd ..

# 删除文件夹
rm -rf ./vuepress-reco-blog-dist
rm -rf ./dist
