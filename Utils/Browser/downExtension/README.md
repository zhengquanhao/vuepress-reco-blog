---
title: 如何将Chrome浏览器将扩展程序下载到本地？
date: 2021-12-05
tags:
 - Utils
categories:
 - Utils
---

## 前言

上篇文件为大家介绍了常用的Chrome插件，那怎样把我们浏览器中使用的插件保存到本地了，今天就让我们一起来看一下。

本文将介绍谷歌浏览器扩展程序下载教程，下载谷歌扩展文件到电脑上的步骤，帮助玩家享受多功能化的谷歌浏览器。

## 两种下载方式

### 一、通过ID下载

① 查找插件ID

首先进入谷歌浏览器的设置界面，找到想下载插件的对应ID。

![6999e1d53a41343fcc5ea6da7f6f7926](./images/5E979D3E-C9E2-4953-B8D5-E8EBEDDBB3A4.png)

![9ee095c1ebd29bd72acd06fa1c4e3b59](./images/5EB6A8FA-3002-472F-8E3D-B2C7D835A9B3.png)

② 下载扩展

将ID粘贴到网址http://chrome-extension-downloader.com/，点击download，即可将浏览器中的插件下载到本地啦。

![86cb5b46d34adda7b8fc0374b29bb676](./images/41E6897A-700D-4FB9-99BF-DB02ADC46659.png)

### 二、通过GET CRX工具下载

① 安装`Get CRX`插件

打开chrome网上商店，搜索`Get CRX`，进行扩展的下载操作。

![3a16eb62b4d587cac0da4db5802b1afb](./images/6180F558-2997-4464-90CB-8528F209808B.png)

> `Get CRX`插件安装地址: https://chrome.google.com/webstore/detail/get-crx/dijpllakibenlejkbajahncialkbdkjc?hl=zh-CN

② 下载.crx文件

安装完成后，在扩展安装页点击右上角的插件图标，并选择 `Get CRX of this extension` 就可以下载啦。

> 注意要在`扩展安装页面`插件才可以生效哦（如下图所示）。

![b318f3ebe10d9ee102bb8d67d4e370c4](./images/4F93DDF3-2614-4F48-B9A7-C641130B00F8.png)
