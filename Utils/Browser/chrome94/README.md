---
title: 解决谷歌浏览器最新chrome94版本CORS跨域问题
date: 2021-12-27
tags:
 - Utils
categories:
 - Utils
---

## CORS跨域问题

升级谷歌浏览器最新chrome94版本后，提示：

```shell
Access to XMLHttpRequest at 'http://localhost:xxxx/api' from origin 'http://xxx.xxx.com:xxxx' has been blocked by CORS policy: The request client is not a secure context and the resource is in more-private address space `local`.
```

## 解决办法

打开浏览器，进入 `chrome://flags/` 页面

![chrome://flags/](./images/flags.png)

搜索 `Block insecure private network requests`

![Block insecure private network requests](./images/block-insecure-network.png)

设置为`Disabled`，`Relaunch`就好了。
