---
title: 程序员都在用的Chrome浏览器插件
date: 2021-12-05
sticky: 2
tags:
 - Utils
categories:
 - Utils
---


## 前言

有句话，事半功倍，其必然是借助了某些思想和工具。以下几款Chrome插件，你值得拥有。

![1fc0ecc243f7a0eb2c18bd7c32a9a704](./images/58F44EDA-1F34-47BB-BCB0-C4202E362BA7.png)

> 需要能去谷歌商城，如不能，先安装`iGG谷歌访问助手`，也可联系我要下载包，然后一切安好。

## 一、功能类

### iGG谷歌访问助手

![72f602729f5f0c5541fd1af99606c7aa](./images/83BF271C-7CB9-48B7-B3C4-74BF1082F185.png)

基础免费，没任何强制要求，还等什么? 当然访问youtube等，那就是另外一回事。

在Chrome浏览器上亲测过，非常好用。

> [iGG谷歌访问助手插件安装地址](https://chrome.google.com/webstore/detail/igg%E8%B0%B7%E6%AD%8C%E8%AE%BF%E9%97%AE%E5%8A%A9%E6%89%8B/ncldcbhpeplkfijdhnoepdgdnmjkckij?utm_source=chrome-ntp-icon)

### 绿色搜索

超五星推荐，去掉百度搜索广告或者弱化显示，真的是良心作品，良心作品，良心作品。
同样搜索"前端"二字，看效果，广告不见了，广告不见了，广告不见了，赶紧安装吧。

启用"绿色搜索"前：
![b2d69519ac5b9c00b86963d1018e4dde](./images/48BF6556-D6D0-4C53-858D-164DC745CDED.png)

启用"绿色搜索"后：
![1496a2ff90a9334c25093e4b4faf1a7c](./images/1197E4CA-3501-42F9-BFC7-63953931CE44.png)

> [绿色搜索插件安装地址](https://chrome.google.com/webstore/detail/%E7%BB%BF%E8%89%B2%E6%90%9C%E7%B4%A2/aeajloomjeoncelkceelhhpkgbcgafek)

### Infinity 新标签页

![b4c9b740ab9fc2e10ec547ea33c296b2](./images/5263255F-863E-471C-AEAC-D220DCCE6542.png)

百万用户选择的新标签页，自由添加网站图标，云端高清壁纸，快速访问书签、天气、笔记、待办事项、扩展管理与历史记录。

> [Infinity 新标签页插件安装地址](https://chrome.google.com/webstore/detail/infinity-new-tab/dbfmnekepjoapopniengjbcpnbljalfg)

### 扩展管理器（Extension Manager）

![691683226fd812e72d743b519dcd8221](./images/76378E3B-79CE-4173-B99C-D8B4AF31F355.png)

chrome插件管理器，插件太多放不下，激活和停用不方便，这款，真的很优秀！

> [扩展管理器（Extension Manager）插件安装地址](https://chrome.google.com/webstore/detail/extension-manager/gjldcdngmdknpinoemndlidpcabkggco)

### OneTab

![ea127251bbf49c8c56a3fc4de7d0b9d5](./images/DBD4F591-AFE8-44E7-9207-F0ED6B55E061.png)

节省高达95％的内存，并减轻标签页混乱现象。

> [OneTab插件安装地址](https://chrome.google.com/webstore/detail/onetab/chphlpgkkbolifaimnlloiipkdnihall)


## 二、开发类

### FeHelper(前端助手)

程序员必备插件，内置了json美化，json比对，图片转base, 二维码/解码, markdown工具，简易postMan, JS正则， 页面取色，编码转换，网页性能，游猴脚本，excel转json等等

![0c67d7aa157f172bdce142d62015cb67](./images/80B8F82E-37CA-45AD-BE4F-DEFF3D152791.png)

> [FeHelper(前端助手)插件安装地址](https://chrome.google.com/webstore/detail/fehelper%E5%89%8D%E7%AB%AF%E5%8A%A9%E6%89%8B/pkgccpejnmalmdinmhkkfafefagiiiad?hl=zh-CN)

### Octotree-GitHub code tree

github网站浏览利器，右边生产菜单，一览无余， 简单高效！

![f61c52035d8fab49e07f69112c00d40b](./images/24285785-3B00-4EE5-BF19-CD914D0DBD34.png)

> [Octotree-GitHub code tree插件安装地址](https://chrome.google.com/webstore/detail/octotree-github-code-tree/bkhaagjahfmjljalopjnoealnfndnagc?hl=zh-CN)


### GitHub加速

![122aa4f4f8a71c2ecc45d5977f5231ff](./images/D2E423DD-7F72-403C-B5A4-BF538F645CEB.png)

国内Github下载很慢，用上了这个插件后，下载速度嗖嗖嗖的~

> [GitHub加速插件安装地址](https://chrome.google.com/webstore/detail/github%E5%8A%A0%E9%80%9F/mfnkflidjnladnkldfonnaicljppahpg)


### ColorZilla

吸管工具，颜色选择器.

![dab574d1143a1aa36b42378e0c4129f1](./images/FDA818CF-10C8-4188-AF1A-67115DC07176.png)

> [ColorZilla插件安装地址](https://chrome.google.com/webstore/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp)

### Page Ruler Redux

开发人员设计器标尺，用于获得完美的像素尺寸和定位，以测量任何网页上的元素。

![d925cf7f8efe6b3619a7172bd9a6fe02](./images/397FE25F-330D-4CF4-85A6-41EBA7DCBB01.png)

> [Page Ruler Redux插件安装地址](https://chrome.google.com/webstore/detail/page-ruler-redux/giejhjebcalaheckengmchjekofhhmal)