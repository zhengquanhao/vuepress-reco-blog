---
title: 常用工具类
date: 2021-04-14
categories:
 - Tools
---

## 金额格式化

```javascript
let NumFormat = value => {
    let flag = false;
    if (!value) {
        if (value !== 0) {
            return;
        }
    }
    // 处理负数
    if (value < 0) {
        value = Math.abs(value).toFixed(2);
        flag = true;
    };
    let arr = value.toString().split(".");
    let l = arr[0];
    let r = "";
    // 判断是否存在小数部分，如果存在才获取
    if (arr.length === 2) {
        r = arr[1];
    }
    // 将整数部分胡成字符串形式并进行反转
    l = l.split("").reverse().join("");
    // 利用正则进行三位添加一逗号的操作
    l = l.replace(/(\d{3})/g, function (m, m1) {
        return m1 + ",";
    });
    l = l.split("").reverse().join("");
    // 将字符串反转回去 有可能第一位就是一个逗号 若出现这种现象 舍去首位的逗号
    // window.console.log(l[0]);
    if (l[0] === ",") {
        l = l.slice(1);
    }
    // 处理后的正数取反
    if (flag) {
        l = "-" + l;
    };
    // 将处理后的整数部分加上小数部分返回
    if (r) {
        return l + "." + r;
    } else {
        return l;
    }
};
export { NumFormat };
```

## 指定时区时间转换

```javascript
// 使用：{{$dateFormat(beginTime, 'dd-MM-yyyy hh:mm:ss')}}
export default (date, fmt) => {
    // 如果是时间戳的话那么转换成Date类型
    if (!date) {
        return;
    }
    // 如果不是时间戳，是字符串，如 2020-07-16
    if (isNaN(date)) {
        date = (new Date(date)).getTime();
    }
    var timezone = 7; // 目标时区时间，东七区
    var offsetGMT = new Date().getTimezoneOffset(); // 本地时间和格林威治的时间差，单位为分钟
    if (typeof date === "number") {
        date = new Date(date + offsetGMT * 60 * 1000 + timezone * 60 * 60 * 1000);
        // date = new Date(date);
    } else if (typeof date === "string") {
        // date = new Date(parseInt(date));
        date = new Date(parseInt(date + offsetGMT * 60 * 1000 + timezone * 60 * 60 * 1000));
    }
    let o = {
        // 月份
        "M+": date.getMonth() + 1,
        // 日
        "d+": date.getDate(),
        // 小时
        "h+": date.getHours(),
        // 分
        "m+": date.getMinutes(),
        // 秒
        "s+": date.getSeconds(),
        // 季度
        "q+": Math.floor((date.getMonth() + 3) / 3),
        // 毫秒
        "S": date.getMilliseconds()
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (let k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
};
```
