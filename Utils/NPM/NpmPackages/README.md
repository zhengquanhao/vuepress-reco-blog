---
title: 开发中可能用到的Npm包
date: 2022-04-25
tags:
 - npm
categories:
 - Tools
---

## 一、sortablejs

### 安装依赖

```shell
npm install sortablejs --save
```

### 使用

```javascript
import Sortable from "sortablejs";

// 拖拽表格
const rowDrop = () => {
    const tbody = document.querySelector(
        ".el-table__body-wrapper tbody"
    );
    Sortable.create(tbody, {
        onEnd ({ newIndex, oldIndex }) {
            // 移动元素的新旧索引
            console.log("newIndex", newIndex);
            console.log("oldIndex", oldIndex);
            const tData = JSON.parse(JSON.stringify(tableData))._value;
            console.log("tData", tData);
            const currRow = tData.splice(oldIndex, 1)[0];
            console.log("currRow", currRow);
            tData.splice(newIndex, 0, currRow);
            console.log("tData", tData);
            // _this.tableData = [];
            const updateList = [];
            tData.forEach((item, index) => {
                // if (index >= newIndex && index <= oldIndex) {
                const param = {
                    id: item.id,
                    position: index
                };
                updateList.push(param);
                // }
            });
            console.log("updateList", updateList);
            // 如果顺序未改变就不与后端交互
            // if (updateList.length > 1) {}
            // _this.$fetch.api_tab.SortTab(updateList);
            // setTimeout(() => {
            //     _this.tableData = tData;
            // }, 5);
        }
    });
};

onMounted(() => {
    rowDrop();
});
```

[sortablejs的npm地址链接](https://www.npmjs.com/package/sortablejs)
