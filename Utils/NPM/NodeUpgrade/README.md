---
title: node版本控制
date: 2021-12-14
tags:
 - npm
categories:
 - Tools
---


## 一、查看当前版本

```shell
node -v

npm -v
```

## 二、更新至最新版本

### 2.1 更新npm

```shell
npm install -g npm
```

### 2.2 更新node版本

先清除npm缓存：

```shell
npm cache clean -f
```

然后安装n模块：

```shell
npm install -g n
```

升级node.js到最新稳定版：

```shell
n stable
```

## 三、版本降级/升级

```shell
n 版本号
```

> 如果是mac系统 升级出现错误 在命令前面加`sudo`