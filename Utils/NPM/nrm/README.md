---
title: NPM源管理器NRM使用教程
date: 2021-12-07
tags:
 - npm
categories:
 - Tools
---

## 一、介绍

`npm` 包有很多的镜像源，有的源有的时候访问失败，有的源可能没有最新的包等等，所以有时需要切换 `npm` 的源，`nrm` 包就是解决快速切换问题的。

`nrm` 可以帮助您在不同的npm源地址之间轻松快速地切换。

`nrm` 内置了许多源，部分经测试访问失败，主要源包括如下：

|源	| URL | 主页 |
| -- | -- | -- |
| npm | https://registry.npmjs.org/ | https://www.npmjs.com/ |
| cnpm | http://r.cnpmjs.org/ | https://cnpmjs.org/ |
| taobao | https://registry.npm.taobao.org/	| https://npm.taobao.org/ |

## 二、安装

使用如下命令进行 `nrm` 的安装:

```shell
npm install -g nrm
```

是否安装成功:

```shell
nrm --version
```


## 三、使用

### 3.1 列出可选择的源

终端执行运行命令列出可选择的源：

```shell
nrm ls
```

`*` 标记的为当前源，展示效果：

```shell
~ nrm ls

  npm ---- https://registry.npmjs.org/
  cnpm --- http://r.cnpmjs.org/
* taobao - https://registry.npm.taobao.org/
  nj ----- https://registry.nodejitsu.com/
  rednpm - http://registry.mirror.cqupt.edu.cn/
  npmMirror  https://skimdb.npmjs.com/registry/
  edunpm - http://registry.enpmjs.org/
```

### 3.2 切换使用的源

终端执行运行命令 `nrm use **` 切换使用的源：

```shell
nrm use npm
```

表示切换源为 `npm`。

### 3.3 添加一个源

如果你想添加一个源，终端执行命令 `nrm add <registry> <url> [home]`，`reigstry` 为源名，`url` 为源的路径， `home` 为源的主页(可不写)。

```shell
nrm add company http://npm.company.com/
```

### 3.4 删除一个源

想要删除一个源，终端执行命令 `nrm del <registry>`，reigstry为源名。

```shell
nrm del company
```

### 3.5 测试源速度

测试一个源的响应时间，终端执行命令 `nrm test ***`。

```shell
nrm test npm
```

展示效果：

```shell
~ nrm test npm

* npm ---- 833ms
```

测试所有源的速度：
```shell
nrm test
```

展示效果：
```shell
~ nrm test

* npm ---- 807ms
  cnpm --- 374ms
  taobao - 209ms
  nj ----- Fetch Error
  rednpm - Fetch Error
  npmMirror  1056ms
  edunpm - Fetch Error
```

### 3.6 访问源的主页

如果你想访问源的主页，可在终端输入下面命令 `nrm home ***`：

```shell
nrm home taobao
```

此命令会在浏览器中打开淘宝源的主页：[https://npm.taobao.org/](https://npm.taobao.org/)

注：如果要查看自己添加的源的主页，那么在添加源的时候就要把主页带上：

```shell
~ nrm add company http://npm.company.com/ http://npm.company.com/
```

如果添加源的时候没有写home信息，那么 `nrm home` 命令不会有效果。

