---
title: 整理八类30+ vscode插件  让你的开发效率飞速提升
date: 2021-12-05
sticky: 3
tags:
 - Utils
categories:
 - Utils
---

## 前言

微软的 VS Code 是一个免费的开源代码编辑器，最近越来越受欢迎。它非常轻巧、灵活，同时也提供了很多强大的功能，但也许最酷的是 VS Code 提供了规模超大的扩展插件，下面将给你介绍一些适合前端的顶级 VS Code 扩展插件。

## 一、主题图标

### 1.1 Material Theme

`VS Code`最悠久的主题, 想要自定义编辑器主题的小伙伴可以安排起来啦！

![04f8af581db06e9d4ce91158bd8af3da](./images/7574C11A-FC35-4F55-AC76-921433078A41.png)

[Material Theme插件地址](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme)

### 1.2 vscode-icons


![78f38f553957e7a9085e8db8172e958e](./images/147CE4EE-CBD2-4257-88AA-22B605E36D17.png)

**插件效果：**

![ebdbdbcbb2a81813b90fa5c3b36bdd22](./images/09AAE1DA-EAFA-43D1-908C-2CDD40A90FF7.gif)

[vscode-icons插件地址](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)


## 二、浏览器相关

### 2.1 open in brower

通过该插件用户可以在编辑器中快速在浏览器中打开 `HTML` 文件，该浏览器可以是计算机上安装的任何浏览器。

![12865821fef13d89b1019096717d5f54](./images/F9BE0EB5-CED5-40D5-9B40-962C26D6B0D7.png)

**插件效果：**

Window: 使用快捷键 `Alt+B` 查看效果。
Mac: 使用快捷键 `option+B` 查看效果。

[open in brower插件地址](https://marketplace.visualstudio.com/items?itemName=techer.open-in-browser)

### 2.2 Browser Preview

可在编辑器中打开一个浏览器进行实时的预览，甚至调试。

![f42a9d2f18e60ef69e8d29d0b76a599a](./images/036C4037-2BCB-49F0-859A-C777FB32FD45.png)


**插件效果：**

![18b2785764500a3def207c564e8356b4](./images/CC57EE3F-A508-434C-9B35-1FB2867092C0.gif)


[Browser Preview插件地址](https://marketplace.visualstudio.com/items?itemName=auchenberg.vscode-browser-preview)

## 三、汉化插件

### Chinese (Simplified) Language Pack for Visual Studio Code

安装此插件并重新加载vscode后，你会惊奇的发现编辑器的语言从默认的英文变成中文啦，英文不好的同学不用再挠头啦！

![f7b82a27e91ed33cbac5d3f1961fd987](./images/7371921A-EBD4-4ED6-98BF-D6B6BF16722F.png)

[汉化插件地址](https://marketplace.visualstudio.com/items?itemName=MS-CEINTL.vscode-language-pack-zh-hans)

## 四、开发效率提升

### 4.1 Html Language Features

当你在书写html时，`Html Language Features`会提示完整的标签名称(此插件推荐了，书写html时会乱补充符号，比如打出=会补充"")。

![fe220938cfea10ea974b134d323c64c3](./images/18384BBE-D082-4514-BDAE-24D0FE45F08D.png)

**插件效果：**

![11cb3dba882d92700b6f1123932561da](./images/72AA8039-5940-416E-AA58-9046DF27C985.png)

[Html Language Features插件地址](https://marketplace.visualstudio.com/items?itemName=SimonSiefke.html-language-features)

### 4.2 Auto Close Tag

自动闭合 `HTML` 标签。

![dd8406d0a3612a2ff0269e64c55cd393](./images/A7E594D5-455D-43B6-83F9-265F4A795E2E.png)

**插件效果：**

![11b188a33610037a92de75e936d4837a](./images/A729CF85-4E84-4AD2-A972-2CA85FC5F805.gif)


[Auto Close Tag插件地址](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)

### 4.3 Auto Rename Tag

自动重命名配对的 `HTML` 标签。

![2310f40f639dc1d70a240d13fbcc8514](./images/6EE0BAE1-4FAA-4A20-A770-1B176E1F8C13.png)

**插件效果：**

![304d2802620974eeb369a6302f50cfc0](./images/D50E3D60-713B-4F8A-81AF-409BE39A6CF7.gif)

[Auto Rename Tag插件地址](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)

### 4.4 CSS Peek

根据类名快速定位到CSS代码。

![31bd9bcdd6c78b3393bd6b494043271c](./images/559C9300-90B0-4B3F-8677-9F8D7ADD98B5.png)

**插件效果：**

![247271ae745aaffee6497624e335b7b9](./images/DEFFAAA7-69D3-48C6-9653-A098483003F3.gif)

[CSS Peek插件地址](https://marketplace.visualstudio.com/items?itemName=pranaygp.vscode-css-peek)

### 4.5 IntelliSense for CSS class names in HTML

帮助开发者提供 CSS 类名补全。

![6def0fd7541ee247e992a999520143e3](./images/54BB3077-63D3-4521-A71B-C1764169987E.png)

**插件效果：**

![7fe8c998a0b0623cf8b807d5f6ce66a2](./images/13CB312F-4EFA-4BFE-9CD2-BE18C47F2108.gif)

[IntelliSense for CSS class names in HTML插件地址](https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion)


### 4.6 lit-html

在JavaScript/TypeScript的文件中，如果有使用到HTML标记，lit-html提供语法高亮和相应的补全支持。

![d99c2ed3645237772e07e232f0315e8f](./images/0CC3DA72-3062-440F-9AD6-81013183FA43.png)

**插件效果：**

![7dd08964273d60ab3e52c01b1661c7f8](./images/3629813A-DFBF-452E-B02A-EDBDF04BFDBB.gif)

[lit-html插件地址](https://marketplace.visualstudio.com/items?itemName=bierner.lit-html)


### 4.7 Todo Tree

最实用的vscode插件，在我们的代码书写过程中经常会有需要待办的代码，比如这段代码后续需要优化，在这时可以利用todo tree做一个标记，做过的标记都会出现在插件的树视图中。等到我们达到可做后续开发的条件是，便可直接通过树视图快速定位到之前标记的代码处继续开发。

![a205930a82189ef81fcc05266e297e38.png](evernotecid://5022495C-83AD-469C-B16C-EC19A1C06A17/appyinxiangcom/32353337/ENResource/p99)

**插件效果：**

![4ce0bc0b32dcb38125ad15881b8b57a2](./images/C8D2291E-7C35-4F01-90C0-B8A6259EEAAE.png)

[Todo Tree插件地址](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)

### 4.8 Bookmarks

使用书签标记/取消标记位置，今后便可快速定位到打标位置的代码。

![7448a7ea16785c3a0c7e7d01e85e716e](./images/3C4808F4-74E4-47A0-B349-0D4E810CA460.png)

**插件效果：**

![00d684fedf87910e32be65bdacee8bb6](./images/FE32F10B-099B-400D-9D0A-ED2BCCAF464E.png)

[Bookmarks插件地址](https://marketplace.visualstudio.com/items?itemName=alefragnani.Bookmarks)


### 4.9 Quokka.js

实时执行JavaScript代码

![922cd71053b92fb4f021feec199b5a7f](./images/D47F28C1-6AE5-47EC-92E1-3D492E346B61.png)

**插件效果：**

![7643656038c0aa39c756b6525aa1b590](./images/3482DDB0-8B41-460A-8A48-992FF9B4954F.gif)

[Quokka.js插件地址](https://marketplace.visualstudio.com/items?itemName=WallabyJs.quokka-vscode)


## 五、Git相关

### 5.1 Git Graph

查看存储库的 Git 图表，并从图表轻松执行 Git 操作。

![088fdd42ae957ec956d6479a89e94432](./images/F38A5C9E-E2C1-4857-B000-2793AC1EC678.png)

**插件效果：**

![77899c6e422bb6491d9c6b2889a52c03](./images/0B9145B5-76AC-427C-A07F-D289BF339590.gif)

[Git Graph插件地址](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)


### 5.2 GitLens

GitLens帮助您更好地理解代码。快速了解更改行或代码块的人、原因和时间。回顾历史，以进一步了解代码如何以及为何演变。轻松探索代码库的历史和演变。

![75cc86b4eb918384063c948a8974be4b.png](evernotecid://5022495C-83AD-469C-B16C-EC19A1C06A17/appyinxiangcom/32353337/ENResource/p532)

**插件效果：**

![8113cf05975e0b0f33b9e3b41f46ad71](./images/9B082CED-9D76-4DE9-9008-54F5FAFE9EEC.png)

[GitLens插件地址](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)


### 5.3 Git History

查看和搜索 git log 以及图表和详细信息，比较分支、比较提交等。

![e1e414ae3074130b82c89575f2221997](./images/A40C985C-9985-4BBB-9147-84A2E45D5231.png)

**插件效果：**

![7635e8c56abeb0028a1d0345324f664e](./images/37C193AC-AC94-41B5-B743-7E968C2DC794.gif)
![9d077815a8cf1f944688ac50692ea5c2](./images/F9884896-6DA2-4246-9799-538FB35DCD48.gif)
![90f060af431bda13ce3135baa3089206](./images/48FE993C-5C01-4BDF-A7A7-AD996B735B6E.gif)
![224bd47f8de86eebc00d954e54737ecc](./images/68E34816-861D-42DB-9350-F9AB5582DA8B.gif)

[Git History插件地址](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)


## 六、代码显示与代码规范

### 6.1 ESlint

检测书写的代码，让开发人员代码书写更规范。

![ea9db3a2f82029cd4e1ff7c5c2f75eb8](./images/C9990B29-F457-4094-AC75-29556B1D91E9.png)

**插件效果：**

![9c761e33e3d898b118429f5c493b94ca](./images/52F5C88F-7320-4E8F-B66C-9EFDFF80FC3F.png)

[ESlint插件地址](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)


### 6.2 CSS Formatter

格式化css代码，使你的css代码变得工整。

![1d048edf62d7ac1056e734dead6e7c1b](./images/238762ED-733D-472B-B330-8EB361172B27.png)

[CSS Formatter插件地址](https://marketplace.visualstudio.com/items?itemName=aeschli.vscode-css-formatter)


### 6.3 Bracket Pair Colorizer

使你的代码中括号的层级关系更加鲜明。

![b49ffcee1591eacc7d0fef822ee7c828](./images/4ADE5223-AFC1-4F01-9B81-0BE3C9E2A9C9.png)

**插件效果：**

![31cecfd757a49852fb8ab8df94690c22](./images/8BAA838B-43C2-4E29-B71A-6458F7CD13F5.png)

[Bracket Pair Colorizer插件地址](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)


### 6.4 highlight-matching-tag

高亮匹配的标签。

![1d39589f4dc7a175ca251ae4afb2b16f](./images/469283B9-B2D3-4DE8-B938-EB2C91C2D81C.png)

**插件效果：**

![c18ca433876d39daf92ba2f9e05e55c3](./images/A684D03F-B379-484C-86B5-EDF0117ED4D5.gif)

[highlight-matching-tag插件地址](https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag)


### 6.5 Indent-Rainbow

让代码缩进更具可读性。

![970a20a78e8118d2614c5ac29ab29b4f](./images/944FF31F-0822-4DFD-A4D3-278D01C16C18.png)

**插件效果：**

![f5038e5e0259c4bc7d7060710b7b900a](./images/614FB5DF-1DA4-4C2D-ADCE-028529209A66.png)

[Indent-Rainbow插件地址](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)


### 6.6 Trailing Spaces

高亮那些冗余的空格，可以快速删掉。

![05f88388af365c5276cd9d0ee302c43f](./images/5E450FE5-DC18-4CA9-8023-E044C5F52C4B.png)

**插件效果：**

![4d865f54adc1cab5101484f467907fb4](./images/EE8724F3-C993-4051-8A6B-81EBF12B715E.png)

[Trailing Spaces插件地址](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)


### 6.7 Color Highlight

解析编写的rgba或16进制颜色，并高亮显示。

![2ad3c81075154b42fd23fc99e5d10428](./images/4260942C-177A-45F4-A57B-8A9876B44239.png)

**插件效果：**

![ed0ba7756ac417f01bcd52072fb6788a](./images/BB8141C4-602F-4527-9A54-662C137F28B4.png)

[Color Highlight插件地址](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)


### 6.8 SVG Viewer

方便开发者在编译器中查看SVG图标，而不是仅仅显示一串代码。

![fc307af46f3e7259eb1134ffa4f41207](./images/2DB8B790-DD42-49A0-9098-F98093FF8B68.png)

**插件效果：**

![e238038817a66c185f919d44412face8](./images/7CF62D01-BC4D-495D-8679-7EDEE3473F64.gif)

[SVG Viewer插件地址](https://marketplace.visualstudio.com/items?itemName=cssho.vscode-svgviewer)


### 6.9 Vetur

可以将 `.vue` 文件中的语法进行高亮显示，Vetur 不仅支持 Vue 中的 template 模板以外，还支持大多数主流的前端开发脚本和插件。

![76d4088ce6b2e7272a6d9a527d1475a9](./images/CCC690FD-0CE1-4AD1-9FE5-18D2C3DBFA13.png)

[Vetur插件地址](https://marketplace.visualstudio.com/items?itemName=octref.vetur)

## 七、服务器相关

### Live Server

为静态和动态页面启动具有实时重新加载功能的本地开发服务器。

![d8f27bf09ec5d871ff238376cb2f4155](./images/E17676CF-FB00-40D6-B408-FEE21E838D43.png)

**插件效果：**

插件使用方法：右键单击HTML资源管理器窗口中的文件，然后单击Open with Live Server。
![f0a0ed061e1c4c4d724f836ecd709d01](./images/EBBCB8C6-3C12-4436-82AA-65D1B603CDE4.gif)

[Live Server插件地址](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)


## 八、其他

### 8.1 File Utils

提供了一个更加简洁的方法来创建、复制、移动、重命名、删除文件/文件夹。

![e84f20b57d0bf93b1fed3aaceeace7e1](./images/A53E0C6F-1E57-48E4-97FC-7FDE77C3CF42.png)

**插件效果：**

![d124d6e7c194eb442e21260e705bfaa2](./images/AB955CB7-8613-43EA-8B7C-E86920C087DA.gif)

[File Utils插件地址](https://marketplace.visualstudio.com/items?itemName=sleistner.vscode-fileutils)


### 8.2 Comment Translate(英文翻译插件)

识别代码中注释部分，不干扰阅读。支持不同语言，单行、多行注释

![66a16cdd2b4c6f165e57794df21af028](./images/1CCC857B-D6A7-413E-9B5F-E38A176EC144.png)

**插件效果：**

![9ad741d05aa44d620b34fe3e24ade15a](./images/308A995C-F60A-40A4-8B62-9ED5C59C9095.gif)
支持用户字符串与变量翻译,支持驼峰拆分
![fb39c8167fd569cda4f9ae0e50fcc95a](./images/DE2E5334-B882-45BF-A797-F095C2092608.gif)
选择区域翻译 - 划词翻译
![c0e5c65f843b1c55d950106dba757a2c](./images/A11FB1AB-FDD5-45F7-911C-035C2B1DC653.gif)

[Comment Translate插件地址](https://marketplace.visualstudio.com/items?itemName=intellsmi.comment-translate)

### 8.3 前端工具箱

把面试题、常用资料导航装到VSCode里，超级方便！

![b0b6ffc02664544868381f229d357bd8](./images/D5CDDAF4-076B-4EEB-A85E-1B121E94BE4D.png)

**插件效果：**

![1a8d8ad123cc7befef04c8dbc42a20e3](./images/FD1083D4-DB10-4938-AA58-2B75768742FE.png)

[前端工具箱插件地址](https://marketplace.visualstudio.com/items?itemName=poetries.fe-tools)

## 总结

> 一同努力，共同成长，顶峰相见