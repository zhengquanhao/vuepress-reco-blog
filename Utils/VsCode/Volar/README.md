---
title: Volar - Vue3.2终极开发神器！
date: 2022-04-22
tags:
 - Utils
categories:
 - Utils
---

![尤雨溪微博](./images/you.png)

当第一次看到这条微博的时候，就在想`Volar`是什么？所以我们一起来学习一下。

随着`vscode`以及`vue`的越来越普及，`vetur`这个名字也越来越被人熟知。

`vetur`是一个`vscode`插件，用于为`.vue`单文件组件提供代码高亮以及语法支持。

可是由于众所周知的原因，`vue`以及`vetur`对于`ts`的支持，并不友好。在`@vue/composition-api`这个插件 出来之前，`vue2`的`ts`需要使用`vue-prototype-decorator`这个插件，来通过装饰器的模式进行模拟，但是由于不是从底层支持，因此`vue2`的`ts`使用体验可谓是一塌糊涂。

在2020年9月18日，`vue3 - one piece`正式发布。随之而来的，是一整套从头到尾使用ts的新底层以及全新的`composition-api`。与此同时，`volar`顺应而生。

## Volar是什么

与`vetur`相同，`volar`是一个针对`vue`的`vscode`插件，不过与`vetur`不同的是，`volar`提供了更为强大的功能，让人直呼卧槽。

安装的方式很简单，直接在`vscode`的插件市场搜索`volar`，然后点击安装就可以了。

![valor-install](./images/volar-install.png)

## Volar的功能

`Volar`作为vue的二代插件，除了集成了`vetur`的相关功能，如`高亮、语法提示`等之外，更让我关注的，是它独有的功能。

### 功能一：兼容Vue3不再需要唯一根标签

学过`vue`语法的应该都知道，在vue的`template`中，需要一个唯一的`根标签`，这是vue决定的，但是在`vue3`中，去除了这个限制，只要在template中，可以使用多个根标签，不再需要考虑因为唯一根标签所引起的问题。

![无需唯一根节点](./images/tempalte.png)

### 功能二：编辑器快捷分割

vue单文件组件，按照功能，存在`template`、`script`、`style`三个根元素。

就像常规的`html`文件，在单一文件内功能太多，容易造成文件冗余。一个数据稍微多点的`vue`页面，就可能有两三千行代码。随之带来的，就是各种不方便：找数据不方便、上下文切换不方便、开发不方便，等等等等。

为了解决这些问题，`volar`提供了一个快捷方式。

在安装好`volar`之后，进入`.vue`单文件组件，会发现右上角多了一个图标，点击一下这个图标。

![split](./images/split.png)

令人激动的事情发生了，我们的vue文件，按照功能，被拆分成了三个视窗，并且每个视窗都负责自己的功能，其他的两个根元素都被合并了。

也就是说，我们可以非常容易的进行区分开template、script、style了，把一个文件拆成三个窗口，当三个文件来用，而且全部由插件来帮你完成，我们只需要点一下即可。

### 功能三: 对setup语法有了更好的支持

之前在js里面定义的变量未使用会书写报错，现在不会了。

![setup语法糖支持](./images/setup.png)

### 功能四: style里面的class引用

![style](./images/style.png)

可以看到，在`.test`这个类名上面，出现了一个 `1 reference` 的小图标，代表着当前`class`有一次引用，我们点击一下这个`1 reference`出现了一个弹窗，里面是当前`class`的具体使用位置。

![style2](./images/style2.png)

### 功能五: class追溯

我们创建一个`template`根元素，在里面写入:

```html
<template>
    <div class="test">test</div>
</template>
```

除了`style`中的`.test`上面会出现 `1 reference` 的小图标之外，在 `class="test"` 的 `test` 下面，会出现一道横线。

![class](./images/class.png)

对于经常使用`vscode`的开发人员来说，这代表什么意义就不必多说了。

我们根据提示，按住`command`然后`点击`，会发现光标自己移动到了`style`中的`.test`之前。

### 功能六: lang语法提示

vue可以使用`lang`属性来选择使用的语言，比如`template`中的`html`和`pug`，`script`中的`ts`，`style`中的`scss`等。

但是在以前，我们都是手动输入的，编辑器，或者说`vetur`并没有给我们提供任何提示，有可能你找了很久的莫名其妙的问题，就是因为`lang`写错了。

但是这一切，在有了`volar`之后，都不同了。

![lang](./images/lang.png)

现在我们在使用`lang`进行选择预处理器的时候，`volar`为我们提供了明确的提示，告知我们可以使用哪几种语言来进行编写。

> 如果之前按照了`Vetur`使用`Vue3.2`语法糖时一直报错尝试：进入设置页 `Vetur` 把没必要的检测机制去掉。

![volar使用3.2语法糖](./images/valor-setup.png)

https://juejin.cn/post/7035909385511501860