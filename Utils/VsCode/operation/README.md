---
title: VsCode中的一些实用操作
date: 2021-12-05
tags:
 - Utils
categories:
 - Utils
---

## 分支合并

例：将`feature-myBranch-20210101` 合并到 `master`

1. 切到 `master` 本地分支，点击分支-合并分支-选择 `feature-myBranch-20210101`。
2. 处理完合并冲突后点击 `+` 号，提交代码。
3. `master` 代码变为最新。
